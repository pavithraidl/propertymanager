/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 14/02/17 17:15.
 */

"use strict";

//region Global Variables

//endregion

//region Default settings

//endregion

//region Events
$("#front-register-form").submit(function(e) {
    e.preventDefault();
});

//endregion

//region Functions
function registerClient() {
    var fName = $.trim($('#front-fname').val());
    var lName = $.trim($('#front-lname').val());
    var company = $.trim($('#front-company-name').val());
    var email = $.trim($('#front-email').val());
    var password = $.trim($('#front-password').val());
    var emailObject = $('#front-email');

    if(fName == '' || lName == '' || company == '' || email == '' || password == '') {
        $('#front-fname').trigger('blur');
        $('#front-lname').trigger('blur');
        $('#front-company-name').trigger('blur');
        $('#front-email').trigger('blur');
        $('#front-password').trigger('blur');
    }
    else {
        var el = $('#front-register-form');
        blockUI(el);
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'register-client',
            data: {fname: fName, lname:lName, company:company, email:email, password:password},
            success: function (data) {
                if(data == 1){
                    unBlockUI(el);
                    $("#front-registration-container").slideUp(300, function () {
                        $('#front-success-msg').slideDown(200);
                    });
                }
                else if(data == -1) {
                    unBlockUI(el);
                    errorExecution(emailObject, 'Sorry! This email has already used.', 'duplicate');
                }
                else {
                    //getUsersList();
                    $('#mm-btn-user-cancel').trigger('click');
                    notifyDone('User has added. Account activation link has sent.');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}
//endregion