/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 26/03/17 02:14.
 */

"use strict";

//region Global Variables
window.contactSubmitFunction = null;
window.contactResponse = null;
window.contactPhone = 1;
window.contactEmail = 1;
window.contactAddress = 0;
//endregion

//region Default settings
$('#inc-contact-address-container').hide();
//endregion

//region Events
$('#contact-avatar-overlay').on('click', function () {
    $('#contact-avatar-choose').trigger('click');
});

$('#inc-contact-add-phone').on('click', function () {
    addMorePhone();
});

$('#inc-contact-add-email').on('click', function () {
    addMoreEmail();
});

$('#inc-contact-add-address').on('click', function () {
    slideUpDownAddress();
});

$('#contact-add-contact').on('click', function () {
    addContact();
})
//endregion

//region Functions
function globalOpenContact(title) {
    $('#global-contact-modal-title').html(title)
    $('#global-open-contact').trigger('click');
}

function submitContactAvatarForm() {
    $('#upload-contact-avatar').submit();
}

$(function(){
    // function from the jquery form plugin
    $('.contact-avatar-upload').ajaxForm({
        beforeSend:function(){
            $('#contact-avatar-overlay-img').attr('src', window.domain+'assets/images/loading/white-ring-sm.svg');
            $('#contact-img-overlay-loading-rate').show();
            $("#contact-avatar-overlay").css('opacity', '1');
        },
        uploadProgress:function(event,position,total,percentComplete){
            $("#contact-img-overlay-loading-rate").text(percentComplete+'%'); //dynamicaly change the progress bar width
        },
        success:function(){
            $('#contact-img-overlay-loading-rate').fadeOut(100);

        },
        complete:function(response){

            var res = response.responseText;
            var res = JSON.parse(res);

            $('#contact-avatar-img').attr('src', window.domain+res.src);
            $("#contact-avatar-overlay").css('opacity', '0');
            $('#contact-avatar-overlay-img').attr('src', window.domain+'assets/images/icons/camera.png');
            $('#contact-avatar-id').val(res.avatarid);
            // show the image after success
        }
    });

    //set the progress bar to be hidden on loading
    $(".progress").hide();
});



function addMorePhone() {
    window.contactPhone = window.contactPhone+1;
    var phoneId = window.contactPhone;

    if(phoneId < 4) {
        var phoneAppend = $('<div id="inc-phone-field-'+phoneId+'" class="form-group margin-top-sm" style="display: none;"> ' +
            '                   <label class="control-label col-xs-12"></label> ' +
            '                   <div class="col-xs-9"> ' +
            '                       <input type="text" id="inc-contact-phone-'+phoneId+'" maxlength="14" placeholder="0222647713" class="form-control col-md-7 col-xs-12 phone-number"> ' +
            '                   </div> ' +
            '                   <div class="col-md-3 col-xs-3"> ' +
            '                       <select id="inc-contact-phone-type-'+phoneId+'" style="height: 33px;"> ' +
            '                           <option value="Mobile">Mobile</option> ' +
            '                           <option value="Home">Home</option> ' +
            '                           <option value="Work">Work</option> ' +
            '                       </select> ' +
            '                   </div> ' +
            '               </div>').hide();

        $('#inc-contact-phone-container').append(phoneAppend);
        $('#inc-phone-field-'+phoneId).fadeIn(200);

        if(phoneId == 3) {
            $('#inc-contact-add-phone').fadeOut(200);
        }
    }
}

function addMoreEmail() {
    window.contactEmail = window.contactEmail+1;
    var emailId = window.contactEmail;

    if(emailId < 3) {
        var emailAppend = $('<div id="inc-email-field-'+emailId+'" class="form-group margin-top-sm"> ' +
            '                   <label class="control-label col-xs-12"></label> ' +
            '                   <div class="col-xs-12"> ' +
            '                       <input type="text" id="inc-contact-email-'+emailId+'" maxlength="128" placeholder="someone@mail.com" class="form-control col-md-7 col-xs-12 phone-number"> ' +
            '                   </div> ' +
            '               </div>').hide();

        $('#inc-contact-email-container').append(emailAppend);
        $('#inc-email-field-'+emailId).fadeIn(200);

        if(emailId == 2) {
            $('#inc-contact-add-email').fadeOut(200);
        }
    }
}

function slideUpDownAddress() {
    if(window.contactAddress == 0) {
        window.contactAddress = 1;
        $("#inc-contact-address-container").fadeIn(200);
        $('#inc-contact-address-icon').removeClass('fa-plus-circle').addClass('fa-minus-circle')
            .attr('style', 'color: #e00165; font-size: 16px; margin-bottom: 20px;');
        $('#inc-contact-address-text').text('Remove Address');
    }
    else {
        window.contactAddress = 0;
        $("#inc-contact-address-container").fadeOut(200);
        $('#inc-contact-address-icon').removeClass('fa-minus-circle').addClass('fa-plus-circle')
            .attr('style', 'color: #00c433; font-size: 16px; margin-bottom: 20px;');
        $('#inc-contact-address-text').text('Add Address');
    }
}

function addContact() {
    //capture data from the ui
    var avatarId = $('#contact-avatar-id').val();
    var fname = $.trim($('#contact-fname').val());
    var lname = $.trim($('#contact-lname').val());
    var phone1, phone2, phone3, phoneType1, phoneType2, phoneType3, email1, email2, street, suburb, city, postalCode = null;

    if(window.contactPhone == 1) {
        phone1 = $.trim($('#inc-contact-phone-1').val());
        phoneType1 = $.trim($('#inc-contact-phone-type-1').val());
    }
    else if(window.contactPhone == 2) {
        phone2 = $.trim($('#inc-contact-phone-2').val());
        phoneType2 = $.trim($('#inc-contact-phone-type-2').val());
    }
    else if(window.contactPhone == 3) {
        phone3 = $.trim($('#inc-contact-phone-3').val());
        phoneType3 = $.trim($('#inc-contact-phone-type-3').val());
    }

    if(window.contactEmail == 1) {
        email1 = $.trim($('#inc-contact-email-1').val());
    }
    else if(window.contactEmail == 2) {
        email2 = $.trim($('#inc-contact-email-2').val());
    }

    if(window.contactAddress == 1) {
        street = $.trim($('#inc-contact-street').val());
        suburb = $.trim($('#inc-contact-suburb').val());
        city = $.trim($('#inc-contact-city').val());
        postalCode = $.trim($('#inc-contact-postal-code').val());
    }

    //validate the data
    if(fname == null || fname == '') {
        $('#contact-fname').trigger('blur');
    }
    else {
        var el = $('#contact-body');
        blockUI(el);
        //send data to the backend
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'contact/add-new-contact',
            data:{avatarid:avatarId, fname:fname, lname:lname, phone1:phone1, phone2:phone2, phone3:phone3, phonetype1:phoneType1, phonetype2:phoneType2, phonetype3:phoneType3, email1:email1, email2:email2, street:street, suburb:suburb, city:city, postalcode:postalCode},
            success: function (data) {
                window.contactResponse = data;
                window.contactSubmitFunction();
                unBlockUI(el);
                $('#contact-close-contact').trigger('click');
                clearContact();
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    }
}

function clearContact() {
    $('#contact-avatar-id').val('');
    $('#contact-fname').val('');
    $('#contact-lname').val('');
    $('#inc-contact-phone-1').val('');
    $('#inc-contact-phone-2').val('');
    $('#inc-contact-phone-3').val('');
    $('#inc-contact-email-1').val('');
    $('#inc-contact-email-2').val('');
    $('#inc-contact-street').val('');
    $('#inc-contact-suburb').val('');
    $('#inc-contact-city').val('');
    $('#inc-contact-postal-code').val('');

    $('#contact-avatar-img').attr('src', window.domain+'assets/images/icons/vector.jpg');

    $('#inc-phone-field-2').slideUp(200, function () {
        $('#inc-email-field-2').remove();
    });
    $('#inc-phone-field-3').slideUp(200, function () {
        $('#inc-email-field-3').remove();
    });
    window.contactEmail = 1;

    $('#inc-email-field-2').slideUp(200, function () {
        $('#inc-email-field-2').remove();
    });
    window.contactEmail = 1;

    if(window.contactAddress == 1) {
        window.contactAddress = 0;
        $('#inc-contact-add-address').trigger('click');
    }

}
//endregion