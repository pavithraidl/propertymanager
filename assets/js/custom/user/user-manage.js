/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 26/01/17 20:59.
 */

"use strict";

//region Global Variables
window.selectedUser = null;
window.ownUserId = null;
window.aLPaginate = 0;
//endregion

//region Default settings
getUsersList();
//endregion

//region Events
$('#user-tab-activity-log-container').scroll(function () {
    var elementHeight = $("#user-tab-activity-log-container")[0].scrollHeight;
    var scrollPosition = $("#user-tab-activity-log-container").height() + $("#user-tab-activity-log-container").scrollTop();

    if (elementHeight -100 < scrollPosition) {
        if (window.aLPaginate != 0) {
            window.aLPaginate++;
            var userId = window.selectedUser;
            getActivityLog(window.aLPaginate, userId);
        }
    }
});
//endregion

//region Functions
function getUsersList() {
    var el = $('#user-manage-widget');
    blockUI(el);

    jQuery.ajax({
        type: 'GET',
        url: window.domain+'user/get-user-list',
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else if(data == 'Unauthorized') {
                window.location.reload();
            }
            else {
                var res = JSON.parse(data);
                var htmlAppend = '';

                if(window.selectedUser == null) {
                    window.ownUserId = res[0].id;
                    window.selectedUser = res[0].id;
                }

                for(var i = 0; i < res.length; i++) {
                    htmlAppend += '  <li id="user-list-item-'+res[i].id+'" class="user-list-1 ' + (res[i].id == window.selectedUser ? 'user-list-1-selected' : '') + '" onclick="selectUser(' + res[i].id + ');" ' + (res[i].status == 3 ? 'data-toggle="tooltip" title="Account not activated yet"' : "") + (res[i].status ==2 ? 'style="opacity: 0.4;"' : '') + '>' +
                        '                   <div class="row" style="margin-bottom: -15px;"> ' +
                        '                       <div class="col-md-3 col-xs-4"> ' +
                        '                           <span class="rounded-image topbar-profile-image"> ' +
                        '                               <img id="user-avatar-'+res[i].id+'" ' + (res[i].status == 3 ? 'class="gray-scale"' : "") + ' src="'+res[i].avatar+'"> ' +
                        '                           </span> ' +
                        '                       </div> ' +
                        '                       <div class="col-md-9 col-xs-8" style="margin-left: -8px;"> ' +
                        '                           <div class="row"> ' +
                        '                               <span id="o-m-org-list-name-1" class="text-blue-3 pull-left" style="font-size: 15px;' + (res[i].status == 3 ? 'color:#A4A4A4 !important;' : "") + '"><strong>'+res[i].name+'</strong></span> ' +
                        '                               <span id="o-m-org-list-loading-container-1"></span> ' +
                        '                           </div> ' +
                        '                       <div class="row"> ' +
                        '                       <p id="o-m-org-list-roll-1" style="color: rgba(100, 100, 100, 0.31); font-size:12px;">'+res[i].jobtitle+'</p> ' +
                        '                       </div>' +
                        '                           <input type="hidden" id="o-m-active-status-1" value="Value" /> ' +
                        '                       </div> ' +
                        '                   </div> ' +
                        '                   <hr/> ' +
                        '               </li>';
                }
                getUserBasicDetails();
                $('#user-list-container').html(htmlAppend);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function getUserBasicDetails() {
    var userId = window.selectedUser;
    jQuery.ajax({
        type: 'GET',
        url: window.domain+'user/get-user-basic-details',
        data: {userid:userId},
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else if(data == 'Unauthorized') {
                window.location.reload();
            }
            else {
                var res = JSON.parse(data);
                $('.ios-switch-md').removeClass('on');
                $('.ios-switch-sm').removeClass('on');
                $('.iswitch-xs').removeClass('on');
                if(res.status == 3) {
                    $('#is-user-status').fadeOut(200).addClass('on');
                    $('#user-image').addClass('gray-scale').attr('src', res.avatar);
                    $('#user-name').attr('style', 'color:#A4A4A4 !important;');
                    $('#user-status-icon').addClass('text-gray-3').removeClass('text-green-2').removeClass('text-yellow-1');
                    $('#user-status-text').text('Pending Activation');
                    $('#user-resend-activation-email').fadeIn(300);
                }
                else if(res.status == 2) {
                    $('#is-user-status').removeClass('on');
                    $('#user-image').removeClass('gray-scale').attr('src', res.avatar);
                    $('#user-name').removeAttr('style');
                    $('#user-status-icon').addClass('text-yellow-1').removeClass('text-green-2').removeClass('text-gray-3');
                    $('#user-status-text').text('Deactivated');
                    $('#user-resend-activation-email').fadeOut(300);

                }
                else if(res.status == 1) {
                    $('#is-user-status').addClass('on');
                    $('#user-image').removeClass('gray-scale').attr('src', res.avatar);
                    $('#user-idntity').removeAttr('style');
                    $('#user-status-icon').addClass('text-green-2').removeClass('text-yellow-1').removeClass('text-gray-3');
                    $('#user-status-text').text('Active');
                    $('#user-url').attr('href', 'http://' + res.url);
                    $('#user-resend-activation-email').fadeOut(300);
                }

                //add values to settings input fields
                $('#user-settings-name').val(res.name);
                $('#user-settings-email').val(res.email);

                if(window.selectedUser == window.ownUserId) {
                    $('#is-user-status').fadeOut(200).removeClass('active');
                    $('#user-tab-access').fadeOut(200);
                    $('#user-tab-access-container').removeClass('active in');
                    $('#user-tab-activity-log').addClass('active');
                    $('#user-tab-activity-log-container').addClass('active in');
                    $('#user-delete-user').fadeOut(200);
                    $('#user-tab-settings').removeClass('active');
                    $('#user-tab-settings-container').removeClass('active in');
                }
                else {
                    $('#user-tab-access').fadeIn(200).removeClass('active');
                    $('#user-tab-access-container').removeClass('active in');
                    $('#user-tab-activity-log').addClass('active');
                    $('#user-tab-activity-log-container').addClass('active in');
                    $('#user-delete-user').fadeIn(200);
                    $('#user-tab-settings').removeClass('active');
                    $('#user-tab-settings-container').removeClass('active in');
                }

                if(res.email == null || res.email == '') {
                    $('#user-email').fadeOut(200);
                    $('#user-resend-activation-email').fadeOut(200);
                }
                else {
                    $("#user-email").fadeIn(200);
                    $('#user-email').attr('href', 'mailto:'+res.email);
                }

                $('#user-name').text(res.name);
                $('#user-job-title').text(res.jobtitle);

                changeAccess();
                var userId = window.selectedUser;
                getActivityLog(1, userId);
                var el = $('#user-manage-widget');
                unBlockUI(el);
            }


        },
        error: function (xhr, textShort, errorThrown, data) {
            noConnection(errorThrown);
            unBlockUI(el);
        }
    })
}

function selectUser(id) {
    $('.user-list-1-selected').removeClass('user-list-1-selected');
    $('#user-list-item-'+id).addClass('user-list-1-selected');
    window.selectedUser = id;
    if(window.selectedUser == window.ownUserId) {
        $('#user-delete-user').fadeOut(200);
        $('#is-user-status').fadeOut(200);
    }
    else {
        $('#user-delete-user').fadeIn(200);
        $('#is-user-status').fadeIn(200)
    }
    getUserBasicDetails();
    window.aLPaginate = 0;
    $('#user-activity-list').html('');
    triggerActivityListLoad();
}

function iSwitchOnChange(id, status) {
    var userId = window.selectedUser;
    var el = $('#user-manage-widget');
    blockUI(el);

    if (id == 'user-status') {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'user/change-user-status',
            data: {userid:userId, status:status},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 1) {
                    getUsersList();
                }
                else if(data == 0) {
                    dbError();
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection(errorThrown);
                unBlockUI(el);
            }
        });
    }
    else if(id.substring(0, 5) == 'sa-sy') {
        var systemId = id.slice(6);
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'user/change-system-access',
            data: {systemid: systemId, status:status, selecteduser:window.selectedUser},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
    else if(id.substring(0, 5) == 'sa-op') {
        var operationId = id.slice(6);
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'user/change-operation-access',
            data: {operationid: operationId, status:status, selecteduser:window.selectedUser},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}

function deleteUser() {
    var id = 'user-delete-user';
    nconfirm(id, 'Delete Permanently', 'Do you really want to remove this user account ?', 'Remove', 'danger', 30, -5);
}
window.confirmfunction = (function () {
    var userId = window.selectedUser;
    var el = $('#user-manage-widget');
    $('#user-delete-user').trigger('click');
    blockUI(el);
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'user/remove-user-account',
        data: {userid: userId},
        success: function (data) {
            if(data == 1) {
                notifyDone('You have successfully removed that user account');
                window.selectedUser = null;
                getUsersList();
                unBlockUI(el);
            }
            else if(data == 0) {
                dbError();
                unBlockUI(el);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection(errorThrown);
            unBlockUI(el);
        }
    });
});

window.notConfirmed = (function () {});
function resendActivationEmail() {
    var userId = window.selectedUser;
    var el = $('#user-manage-widget');
    blockUI(el);

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'user/resend-activation-email',
        data: {userid: userId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data != 0) {
                var res = JSON.parse(data);
                notifyDone('Activation email has resent to '+res);
            }
            else {
                dbError();
            }
            unBlockUI(el);
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function openAddUser() {
    setTimeout(function () {
        //$('#mm-user-name').focus();
    }, 300);

}
function triggerActivityListLoad() {
    if (window.aLPaginate == 0) {
        var userId = window.selectedUser;
        getActivityLog(1, userId);
    }
}

function changeAccess() {
    var userId = window.selectedUser;
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'user/get-access-list',
        data: {userid: userId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data != 0) {
                var res = JSON.parse(data);
                for(var i = 0; res.sys.length > i; i++) {
                    $("#is-sa-sy-"+res.sys[i]).addClass('on');
                }
                for(var i = 0; res.ope.length > i; i++) {
                    $("#is-sa-op-"+res.ope[i]).addClass('on');
                }
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

function getActivityLog(paginate, userId) {
    var el = $('#user-tab-activity-log-container');
    if (window.aLPaginate != -1) {
        var temPaginate = window.aLPaginate;
        window.aLPaginate = 0;
        blockUI(el);

        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'user/get-activity-list',
            data: {paginate: paginate, userid: userId},
            success: function (data) {
                if(data == -1) {
                    temPaginate = -1;
                    unBlockUI(el);
                }
                else if (data != 0) {
                    var ret = JSON.parse(data);
                    var activityList = "";
                    for (var i = 0; i < ret.length; i++) {
                        activityList += (
                            '               <li class="media">' +
                            '                   <p>' + ret[i].activity + '<br />' +
                            '                       <i class="pull-right" style="color: rgba(0, 0, 0, 0.25); font-style: italic;" data-toggle="tooltip" title="' + ret[i].createdat + '">' + ret[i].timeago + '</i>' +
                            '                   </p>' +
                            '               </li>'
                        );
                    }

                    $('#user-activity-list-loading').remove();
                    $('#user-activity-list').append(activityList);
                    window.aLPaginate = temPaginate;
                    window.aLPaginate++;
                    unBlockUI(el);
                }
                else {
                    dbError();
                    unBlockUI(el)
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection(textStatus);
            }
        });
    }
    else {
        unBlockUI(el);
    }
}

function addUser() {
    var name = $.trim($('#mm-user-name').val());
    var email = $.trim($('#mm-user-email').val());
    var jobTitle = $.trim($('#mm-user-job-title').val());
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var emailObject = $('#mm-user-email');

    if(name == '' || email == '') {
        $('#mm-user-name').trigger('blur');
        $('#mm-user-email').trigger('blur');
    }
    else if(email != '' && !re.test(email)) {
        $('#mm-user-email').trigger('blur');
    }
    else {
        var el = $('#modal-add-user-body');
        blockUI(el);
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'user/add-user',
            data: {name:name, email:email, jobtitle: jobTitle},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0){
                    $('#mm-btn-user-cancel').trigger('click');
                    dbError();
                }
                else if(data == -1) {
                    unBlockUI(el);
                    errorExecution(emailObject, 'Sorry! This email has already used.', 'duplicate');
                }
                else {
                    //getUsersList();
                    $('#mm-btn-user-cancel').trigger('click');
                    notifyDone('User has added. Account activation link has sent.');
                    getUsersList();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

}

function cancelUser() {
    $('#user-modal-close').trigger('click');
    $('#mm-user-name').val('');
    $('#mm-user-email').val('');

    $('#model-add-new-user .has-error').removeClass('has-error');
    $('#model-add-new-user .error-msg').remove();
}
//endregion