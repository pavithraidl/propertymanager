/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 21/02/17 01:31.
 */

"use strict";

//region Global Variables
window.inspectionId = $('#ins-inspection-id').val();
window.inspectionScheduleId = null;
window.loadSubSections = false;
window.initiation = 1;
//endregion

//region Default settings
window.inspectionScheduleId = $('#ins-schedule-id').val();
window.inspectionStatus = $('#ins-inspection-status').val();
//endregion

//region Events
configGlobal();

$('.edit').on('froalaEditor.blur', function (e, editor) {
    var mainId = $(this).attr('data-mainid');
    var catId = $(this).attr('data-catid');
    var secId = $(this).attr('data-secid');
    var value = $(this).find('.fr-view').html();
    var inspectionId = window.inspectionId;
    // var inspectionScheduleId = window.inspectionScheduleId;
    var el = $('#ins-r-container');
    blockUI(el);

    if(mainId != 'limitations') {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'inspection/new/save-description',
            data:{inspectionid: inspectionId, mainid:mainId, catid:catId, secid:secId, description:value},
            success: function (data) {
                if(data == 0) {
                    dbError();
                    unBlockUI(el)
                }
                else {
                    $('#ins-r-description-'+mainId+'-'+catId+'-'+secId).html(value);
                    unBlockUI(el);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
    else if(mainId == 'limitations') {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'inspection/new/save-limitations',
            data:{inspectionid: inspectionId, limitations:value},
            success: function (data) {
                if(data == 0) {
                    dbError();
                    unBlockUI(el)
                }
                else {
                    if(value == '') value =  '<span style="font-style: italic; color: #b9b9b9;">No Limitation Records</span>';
                    $('#ins-r-limitation-container').html(value);
                    unBlockUI(el);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }


});

//endregion

//region Functions
function configGlobal() {
    var oldInspectTest = $('#ins-inspection-id').val();
    if(oldInspectTest != '' && oldInspectTest != null) {
        window.loadSubSections = true;
    }
}

function saveStatus(step) {
    if(window.initiation == 1) {
        var inspectionId = $('#ins-inspection-id').val();
    }
    else {
        var inspectionId = window.inspectionId;
    }

    var limitationId = $('.view-ins-limitations');

    if(step == 2) {
        var el = $('#ins-limitation-container');
        // blockUI(el);

        var limitationData = 0;
        limitationId.text() != '' && limitationId.text() != null ? limitationData = 1 : limitationData = 0;
    }

    jQuery.ajax({
        type: 'POST',
        url: window.domain+'inspection/new/save-status',
        data:{step:step, inspectionid:inspectionId, limitationdata:limitationData},
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else {

            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection(textStatus);
        }
    });
}

function getSectorData(mainId, catId, secId) {
    var el = $('#ins-sector-body-'+mainId+'-'+catId+'-'+secId);
    var inspectionId = window.inspectionId;
    blockUI(el);

    jQuery.ajax({
        type: 'POST',
        url: window.domain+'inspection/new/get-sector',
        data:{mainid:mainId, catid:catId, secid:secId, inspectionid:inspectionId},
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                $('#ins-topic-'+mainId+'-'+catId+'-'+secId).val(res.topic);
                $(".view-description-"+mainId+'-'+catId+'-'+secId).html(res.description);
                $('#ins-points-'+mainId+'-'+catId+'-'+secId).val(res.general);

                var media = null;
                if (typeof res.media != "undefined" && res.media != null) {
                    for(var i = 0; i < res.media.length; i++) {
                        if(res.media[i].type == 1) {

                            var currentLoop = '<div id="ins-single-media-container-'+res.media[i].id+'" class="col-md-4 ins-single-media-container" style="margin-top: 10px;">' +
                                '                   <img src="'+window.domain+'assets/images/inspection/'+inspectionId+'/'+res.media[i].id+'.'+res.media[i].ext+'" style="width: 100%; height: auto; border: 1px solid rgba(122, 192, 246, 0.81);" />' +
                                '                   <i id="remove-media-'+res.media[i].id+'" class="fa fa-times-circle hover-fade" onclick="removeMedia('+res.media[i].id+');" style="color: #e30010; font-size: 30px; position: absolute; right: -3px; top: -15px;"></i> ' +
                                '               </div>';

                            if(i == 0) media = currentLoop;
                            else media += currentLoop;
                        }
                        else if(res.media[i].type == 2) {
                            var currentLoop = '<div id="ins-single-media-container-'+res.media[i].id+'" class="col-md-4 ins-single-media-container" style="margin-top: 10px;">' +
                                '                   <video style="width: 75%; height: auto; border: 1px solid rgba(122, 192, 246, 0.81);" controls>' +
                                '                       <source src="'+window.domain+'assets/images/inspection/'+inspectionId+'/'+res.media[i].id+'.'+res.media[i].ext+'" type="video/mp4">' +
                                '                       Your browser does not support video.' +
                                '                   </video>' +
                                '                   <i id="remove-media-'+res.media[i].id+'" class="fa fa-times-circle hover-fade" onclick="removeMedia('+res.media[i].id+');" style="color: #e30010; font-size: 30px; position: absolute; right: 74px; top: -15px;"></i> ' +
                                '               </div> ';

                            if(i == 0) media = currentLoop;
                            else media += currentLoop;
                        }

                    }
                }

                $('#ins-media-container-'+mainId+'-'+catId+'-'+secId).html(media);
                unBlockUI(el);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection(textStatus);
            unBlockUI(el);
        }
    });
}

function removeMedia(id) {
    var elementId = 'remove-media-'+id;
    window.toRemoveId = id;
    nconfirm(elementId, 'Remove Media', 'Do you really want to remove?', 'Remove', 'danger', -108, 50);
} window.confirmfunction = (function () {
    var mediaId = window.toRemoveId;
    var el = $('#ins-single-media-container-'+mediaId);
    $('.nconfirm-btn-cancle').trigger('click');
    blockUI(el);
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'inspection/new/remove-media',
        data: {mediaid: mediaId},
        success: function (data) {
            if(data == 1) {
                el.fadeTo(0, 100);
                unBlockUI(el);
                el.remove();
                $('#ins-r-single-media-container-'+mediaId).remove();
            }
            else if(data == 0) {
                dbError();
                unBlockUI(el);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection(errorThrown);
            unBlockUI(el);
        }
    });
}); window.notConfirmed = (function () {});
function resendActivationEmail() {

}


function saveBasicDetails(step) {
    var residential = $('input[name=residential]:checked').val();
    var age = $.trim($('#ins-age').val());
    var type = $('#ins-type').val();
    var bedrooms = $.trim($('#ins-num-bedrooms').val());
    var bathrooms = $.trim($('#ins-num-bathrooms').val());
    var whether = $.trim($('#ins-whether').val());
    var other = $.trim($('#ins-other-relevant-info').val());
    var inspectionId = window.inspectionId;
    var inspectionScheduleId = window.inspectionScheduleId;
    var present = $.trim($('#ins-present').val());

    jQuery.ajax({
        type: 'POST',
        url: window.domain+'inspection/new/save-basic',
        data:{inspectionscheduleid: inspectionScheduleId, inspectionid: inspectionId, residential: residential, age:age, type:type, bedrooms:bedrooms, bathrooms:bathrooms, whether:whether, other:other, present:present, step:step},
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else {
                var inspectionId = JSON.parse(data);
                window.inspectionId = inspectionId;
                residential = residential == 1 ? residential = 'Yes' : residential = 'No'
                $(".ins-r-f-p-residential").html(residential);
                $(".ins-r-f-p-age").html(age);
                $(".ins-r-f-p-bedrooms").html(bedrooms);
                $(".ins-r-f-p-bathrooms").html(bathrooms);
                $(".ins-r-f-p-other").html(other);
                $(".ins-r-f-p-present_people").html(present);
                $(".ins-r-f-p-whether").html(whether);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function openImageUploader(mainId, catId, secId) {
    $('#ins-upload-img-'+mainId+'-'+catId+'-'+secId).trigger('click');
}

function saveTopic(mainId, catId, secId) {
    var id = $('#ins-topic-'+mainId+'-'+catId+'-'+secId);
    var topic = $.trim(id.val());
    var inspectionId = window.inspectionId;

    var el = $('#ins-r-container');
    blockUI(el);

    if(topic != '') {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'inspection/new/save-topic',
            data:{inspectionid: inspectionId, topic:topic, mainid:mainId, catid:catId, secid:secId},
            success: function (data) {
                if(data == 0) {
                    dbError();
                    unBlockUI(el);
                }
                else {
                    $('#ins-r-topic-'+mainId+'-'+catId+'-'+secId).html(topic);
                    unBlockUI(el);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}

function saveKeyPoints(mainId, catId, secId) {
    var id = $('#ins-points-'+mainId+'-'+catId+'-'+secId);
    var points = $.trim(id.val());
    var inspectionId = window.inspectionId;

    var el = $('#ins-r-container');
    blockUI(el);

    if(points != '') {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'inspection/new/save-points',
            data:{inspectionid: inspectionId, points:points, mainid:mainId, catid:catId, secid:secId},
            success: function (data) {
                if(data == 0) {
                    dbError();
                    unBlockUI(el);
                }
                else {
                    $('#ins-r-bullet-points-'+mainId+'-'+catId+'-'+secId).html(points);
                    unBlockUI(el);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}

function showMe(mainId, catId, secId) {
    alert($('#ins-description-'+mainId+'-'+catId+'-'+secId).html());
}

function saveDescription(mainId, catId, secId) {
    var description = $('#ins-editor-'+mainId+'-'+catId+'-'+secId).html();
    console.log(description);
}

//Image Upload
function chooseImage(mainId, catId, secId) {
    var inspectionId = window.inspectionId;
    $('#img-inspection-id').val(inspectionId);
    $('#img-inspection-mainid').val(mainId);
    $('#img-inspection-catid').val(catId);
    $('#img-inspection-secid').val(secId);
    $('#image-choose').trigger('click');
}

function submitForm(id) {
    var mainId = $('#img-inspection-mainid').val();
    var catId = $('#img-inspection-catid').val();
    var secId = $('#img-inspection-secid').val();
    var progressBar = ' <div id = "image-upload-progress" class = "progress" style = "height: 8px;width: 100%;"> ' +
        '                   <div id = "image-upload-progress-bar" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;"> ' +
        '                       <span id="image-upload-sr-only" class = "sr-only">0% Complete</span> ' +
        '                   </div> ' +
        '               </div>';

    $('#btn-sector-img-upload-'+mainId+'-'+catId+'-'+secId).attr('disabled', 'disabled').append(progressBar);
    var imgId = $('#img-sector-img-upload-'+mainId+'-'+catId+'-'+secId);
    imgId.attr('src', window.domain+'/assets/images/loading/ring-sm.svg');
    $(id).submit();
}

function selectCoverImage(imageId) {
    if($('#ins-cover-image-checked').length) {
        $('#ins-cover-image-checked').fadeOut(200, function () {
            $('#ins-cover-image-checked').remove();
            $('#ins-cover-image-container-'+imageId).append(
                '<i id="ins-cover-image-checked" class="fa fa-check-circle" style="color: #1ABB9C;position: absolute;font-size: 25px;z-index: 1;right: 100px;top: 117px;"></i>'
            );
        });
    }
    else {
        $('#ins-cover-image-container-'+imageId).append(
            '<i id="ins-cover-image-checked" class="fa fa-check-circle" style="color: #1ABB9C;position: absolute;font-size: 25px;z-index: 1;right: 100px;top: 117px;"></i>'
        );
    }
    var inspectionId = window.inspectionId;
    jQuery.ajax({
        type: 'POST',
        url: window.domain+'inspection/new/add-cover-image',
        data:{inspectionid: inspectionId, imageid:imageId},
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else {
                $('#ins-r-cover-img').attr('src', window.domain+'/assets/images/inspection/'+inspectionId+'/'+imageId+'.jpg');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

//endregion