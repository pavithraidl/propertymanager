/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 28/01/17 16:00.
 */

"use strict";

//region Global Variables
getInspectionList(1);
//endregion

//region Default settings

//endregion

//region Events
$('#btn-ins-add').on('click', function () {
    addInspection();
});
//endregion

//region Functions
function addInspection() {
    var el = $('#add-inspection-container');
    blockUI(el);
    var assetId = $('#ins-reference').val();
    var date = $('#ins-inspect-date').val();
    var timeHours = $('#ins-inspect-time-hours').val();
    var timeMin = $('#ins-inspect-time-min').val();
    var timeAmpm = $('#ins-inspect-time-ampm').val();
    var frequency = $('#ins-frequency').val();
    var type = $.trim($('#ins-inspection-type').val());
    var time = timeHours+':'+timeMin+timeAmpm;

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'inspection/add-inspection',
        data: {assetid:assetId , date:date, time:time, frequency:frequency, type: type},
        success: function (data) {
            unBlockUI(el);
            $('#btn-ins-close').trigger('click');
        },
        error: function (xhr, textStatus, errorThrown) {
            unBlockUI(el);
            internetError();
        }
    });
}

function getInspectionList(paginate) {
    var el = $('#ins-schedule-list-container');
    blockUI(el);
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'inspection/get-schedule-list',
        data: {paginate: paginate},
        success: function (data) {
            var res = JSON.parse(data);

            var appendHtml = '';

            if(res.length > 0) {
                for(var i = 0; i < res.length; i++) {
                    appendHtml += ' <tr id="schedule-row-'+res[i].id+'">' +
                        '               <td>'+res[i].address+'</td><br/><span class="blue text-sm">'+res[i].reference+'</span>' +
                        '               <td>'+res[i].schedule+'</td>' +
                        '               <td>'+res[i].inspector+'</td>' +
                        '               <td><a href="'+window.domain+'inspection/new/'+res[i].id+'"> <button class="btn btn-xs btn-success">Go</button> </a></td>' +
                        '           </tr>';
                }
            }
            else {
                appendHtml = '<tr id="pro-row-na"> ' +
                    '               <td colspan="4" style="text-align: center;">No records to display!</td> ' +
                    '           </tr>';
            }

            $("#schedule-data").append(appendHtml);
            unBlockUI(el);
        },
        error: function (xhr, textStatus, errorThrown) {
            unBlockUI(el);
            internetError();
        }
    });
}

//endregion