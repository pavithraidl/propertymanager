/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 10/02/17 23:51.
 */

"use strict";

//region Global Variables
window.selectedOrg = null;
window.ownOrgId = null;
window.aLPaginate = 0;
//endregion

//region Default settings
getOrgsList();
//endregion

//region Events
$('#org-tab-activity-log-container').scroll(function () {
    var elementHeight = $("#org-tab-activity-log-container")[0].scrollHeight;
    var scrollPosition = $("#org-tab-activity-log-container").height() + $("#org-tab-activity-log-container").scrollTop();

    if (elementHeight -100 < scrollPosition) {
        if (window.aLPaginate != 0) {
            window.aLPaginate++;
            var orgId = window.selectedOrg;
            getActivityLog(window.aLPaginate, orgId);
        }
    }
});
//endregion

//region Functions
function getOrgsList() {
    var el = $('#org-manage-widget');
    blockUI(el);

    jQuery.ajax({
        type: 'GET',
        url: window.domain+'org/get-org-list',
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else if(data == 'Unauthorized') {
                window.location.reload();
            }
            else {
                var res = JSON.parse(data);
                var htmlAppend = '';

                if(window.selectedOrg == null) {
                    window.ownOrgId = res[0].id;
                    window.selectedOrg = res[0].id;
                }

                for(var i = 0; i < res.length; i++) {
                    htmlAppend += '  <li id="org-list-item-'+res[i].id+'" class="user-list-1 ' + (res[i].id == window.selectedOrg ? 'user-list-1-selected' : '') + '" onclick="selectOrg(' + res[i].id + ');" ' + (res[i].status == 3 ? 'data-toggle="tooltip" title="Account not activated yet"' : "") + (res[i].status ==2 ? 'style="opacity: 0.4;"' : '') + '>' +
                        '                   <div class="row" style="margin-bottom: -15px;"> ' +
                        '                       <div class="col-md-3 col-xs-4"> ' +
                        '                           <span class="rounded-image topbar-profile-image"> ' +
                        '                               <img id="org-avatar-'+res[i].id+'" ' + (res[i].status == 3 ? 'class="gray-scale"' : "") + ' src="'+res[i].avatar+'"> ' +
                        '                           </span> ' +
                        '                       </div> ' +
                        '                       <div class="col-md-9 col-xs-8" style="margin-left: -8px;"> ' +
                        '                           <div class="row"> ' +
                        '                               <span id="o-m-org-list-name-1" class="text-blue-3 pull-left" style="font-size: 15px;' + (res[i].status == 3 ? 'color:#A4A4A4 !important;' : "") + '"><strong>'+res[i].name+'</strong></span> ' +
                        '                               <span id="o-m-org-list-loading-container-1"></span> ' +
                        '                           </div> ' +
                        '                       <div class="row"> ' +
                        '                       <p id="o-m-org-list-roll-1" style="color: rgba(100, 100, 100, 0.31); font-size:12px;"></p> ' +
                        '                       </div>' +
                        '                           <input type="hidden" id="o-m-active-status-1" value="Value" /> ' +
                        '                       </div> ' +
                        '                   </div> ' +
                        '                   <hr/> ' +
                        '               </li>';
                }
                getOrgBasicDetails();
                $('#org-list-container').html(htmlAppend);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function getOrgBasicDetails() {
    var orgId = window.selectedOrg;
    jQuery.ajax({
        type: 'GET',
        url: window.domain+'org/get-org-basic-details',
        data: {orgid:orgId},
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                $('.ios-switch-md').removeClass('on');
                $('.ios-switch-sm').removeClass('on');
                if(res.status == 3) {
                    $('#is-org-status').fadeOut(200).addClass('on');
                    $('#org-image').addClass('gray-scale').attr('src', res.avatar);
                    $('#org-name').attr('style', 'color:#A4A4A4 !important;');
                    $('#org-status-icon').addClass('text-gray-3').removeClass('text-green-2').removeClass('text-yellow-1');
                    $('#org-status-text').text('Pending Activation');
                    $('#org-resend-activation-email').fadeIn(300);
                }
                else if(res.status == 2) {
                    $('#is-org-status').removeClass('on');
                    $('#org-image').removeClass('gray-scale').attr('src', res.avatar);
                    $('#org-name').removeAttr('style');
                    $('#org-status-icon').addClass('text-yellow-1').removeClass('text-green-2').removeClass('text-gray-3');
                    $('#org-status-text').text('Deactivated');
                    $('#org-resend-activation-email').fadeOut(300);

                }
                else if(res.status == 1) {
                    $('#is-org-status').addClass('on');
                    $('#org-image').removeClass('gray-scale').attr('src', res.avatar);
                    $('#org-idntity').removeAttr('style');
                    $('#org-status-icon').addClass('text-green-2').removeClass('text-yellow-1').removeClass('text-gray-3');
                    $('#org-status-text').text('Active');
                    $('#org-url').attr('href', 'http://' + res.url);
                    $('#org-resend-activation-email').fadeOut(300);
                }

                //add values to settings input fields
                $('#org-settings-name').val(res.name);
                $('#org-settings-email').val(res.email);

                if(window.selectedOrg == window.ownOrgId) {
                    $('#is-org-status').fadeOut(200).removeClass('active');
                    $('#org-tab-access').fadeOut(200);
                    $('#org-tab-access-container').removeClass('active in');
                    $('#org-tab-activity-log').addClass('active');
                    $('#org-tab-activity-log-container').addClass('active in');
                    $('#org-delete-org').fadeOut(200);
                    $('#org-tab-settings').removeClass('active');
                    $('#org-tab-settings-container').removeClass('active in');
                }
                else {
                    $('#org-tab-access').fadeIn(200).removeClass('active');
                    $('#org-tab-access-container').removeClass('active in');
                    $('#org-tab-activity-log').addClass('active');
                    $('#org-tab-activity-log-container').addClass('active in');
                    $('#org-delete-org').fadeIn(200);
                    $('#org-tab-settings').removeClass('active');
                    $('#org-tab-settings-container').removeClass('active in');
                }

                if(res.email == null || res.email == '') {
                    $('#org-email').fadeOut(200);
                    $('#org-resend-activation-email').fadeOut(200);
                }
                else {
                    $("#org-email").fadeIn(200);
                    $('#org-email').attr('href', 'mailto:'+res.email);
                }

                if(res.url == null || res.url == '') {
                    $('#org-url').fadeOut(200);
                }
                else {
                    $('#org-url').attr('href', 'http://' + res.url);
                    $('#org-url').fadeIn(200);
                }

                $('#org-name').text(res.name);

                changeAccess();
                var orgId = window.selectedOrg;
                getActivityLog(1, orgId);
                var el = $('#org-manage-widget');
                unBlockUI(el);
            }


        },
        error: function (xhr, textShort, errorThrown, data) {
            noConnection(errorThrown);
            unBlockUI(el);
        }
    })
}

function changeAccess() {
    var orgId = window.selectedOrg;
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'org/get-access-list',
        data: {orgid: orgId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data != 0) {
                var res = JSON.parse(data);
                for(var i = 0; res.sys.length > i; i++) {
                    $("#is-sa-sy-"+res.sys[i]).addClass('on');
                }
                for(var i = 0; res.ope.length > i; i++) {
                    $("#is-sa-op-"+res.ope[i]).addClass('on');
                }
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

function getActivityLog(paginate, orgId) {
    if (window.aLPaginate != -1) {
        var temPaginate = window.aLPaginate;
        window.aLPaginate = 0;
        var el = $('#org-tab-activity-log-container');
        blockUI(el);

        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'org/get-activity-list',
            data: {paginate: paginate, orgid: orgId},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == -1) {
                    temPaginate = -1;
                    unBlockUI(el);
                }
                else if (data != 0) {
                    var ret = JSON.parse(data);
                    var activityList = "";
                    for (var i = 0; i < ret.length; i++) {
                        activityList += (
                            '               <li class="media">' +
                            '                   <p>' + ret[i].activity + '<br />' +
                            '                       <i class="pull-right" style="color: rgba(0, 0, 0, 0.25); font-style: italic;" data-toggle="tooltip" title="' + ret[i].createdat + '">' + ret[i].timeago + '</i>' +
                            '                   </p>' +
                            '               </li>'
                        );
                    }
                    $('#org-activity-list').append(activityList);
                    window.aLPaginate = temPaginate;
                    window.aLPaginate++;
                    unBlockUI(el);
                }
                else {
                    dbError();
                    unBlockUI(el);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection(textStatus);
                unBlockUI(el);
            }
        });
    }
}

function selectOrg(id) {
    $('.user-list-1-selected').removeClass('user-list-1-selected');
    $('#org-list-item-'+id).addClass('user-list-1-selected');
    window.selectedOrg = id;
    if(window.selectedOrg == window.ownOrgId) {
        $('#org-delete-org').fadeOut(200);
        $('#is-org-status').fadeOut(200);
    }
    else {
        $('#org-delete-org').fadeIn(200);
        $('#is-org-status').fadeIn(200)
    }
    getOrgBasicDetails();
    window.aLPaginate = 0;
    $('#org-activity-list').html('');
    triggerActivityListLoad();
}

function iSwitchOnChange(id, status) {
    var orgId = window.selectedOrg;
    var el = $('#org-manage-widget');
    blockUI(el);

    if (id == 'org-status') {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'org/change-org-status',
            data: {orgid:orgId, status:status},
            success: function (data) {
                if(data == 1) {
                    getOrgsList();
                }
                else if(data == 0) {
                    dbError();
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection(errorThrown);
                unBlockUI(el);
            }
        });
    }
    else if(id.substring(0, 5) == 'sa-sy') {
        var systemId = id.slice(6);
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'org/change-system-access',
            data: {systemid: systemId, status:status, selectedorg:window.selectedOrg},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
    else if(id.substring(0, 5) == 'sa-op') {
        var operationId = id.slice(6);
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'org/change-operation-access',
            data: {operationid: operationId, status:status, selectedorg:window.selectedOrg},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}

function deleteOrg() {
    var id = 'org-delete-org';
    nconfirm(id, 'Delete Permanently', 'Do you really want to remove this organization account ?', 'Remove', 'danger', 30, -5);
}
window.confirmfunction = (function () {
    var orgId = window.selectedOrg;
    var el = $('#org-manage-widget');
    $('#org-delete-org').trigger('click');
    blockUI(el);
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'org/remove-org-account',
        data: {orgid: orgId},
        success: function (data) {
            if(data == 1) {
                notifyDone('You have successfully removed that org account');
                window.selectedOrg = null;
                getOrgsList();
                unBlockUI(el);
            }
            else if(data == 0) {
                dbError();
                unBlockUI(el);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection(errorThrown);
            unBlockUI(el);
        }
    });
});

window.notConfirmed = (function () {});
function resendActivationEmail() {
    var orgId = window.selectedOrg;
    var el = $('#org-manage-widget');
    blockUI(el);

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'org/resend-activation-email',
        data: {orgid: orgId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data != 0) {
                var res = JSON.parse(data);
                notifyDone('Activation email has resent to '+res);
            }
            else {
                dbError();
            }
            unBlockUI(el);
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function openAddOrg() {
    setTimeout(function () {
        //$('#mm-org-name').focus();
    }, 300);

}
function triggerActivityListLoad() {
    if (window.aLPaginate == 0) {
        var orgId = window.selectedOrg;
        getActivityLog(1, orgId);
    }
}

function addOrg() {
    var name = $.trim($('#mm-org-name').val());
    var email = $.trim($('#mm-org-email').val());
    var jobTitle = $.trim($('#mm-org-job-title').val());
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var emailObject = $('#mm-org-email');

    if(name == '' || email == '') {
        $('#mm-org-name').trigger('blur');
        $('#mm-org-email').trigger('blur');
    }
    else if(email != '' && !re.test(email)) {
        $('#mm-org-email').trigger('blur');
    }
    else {
        var el = $('#modal-add-org-body');
        blockUI(el);
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'org/add-org',
            data: {name:name, email:email, jobtitle: jobTitle},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0){
                    $('#mm-btn-org-cancel').trigger('click');
                    dbError();
                }
                else if(data == -1) {
                    unBlockUI(el);
                    errorExecution(emailObject, 'Sorry! This email has already used.', 'duplicate');

                }
                else {
                    //getOrgsList();
                    $('#mm-btn-org-cancel').trigger('click');
                    notifyDone('Organization has added. Account activation link has sent.');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

}

function cancelOrg() {
    $('#org-modal-close').trigger('click');
    $('#mm-org-name').val('');
    $('#mm-org-email').val('');

    $('#model-add-new-org .has-error').removeClass('has-error');
    $('#model-add-new-org .error-msg').remove();
}
//endregion