/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 23/05/17 13:11.
 */

"use strict";

//region Global Variables

//endregion

//region Default settings

//endregion

//region Events
$('.panel-title').on('click', function () {
    $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
        type: 'bar',
        height: '40',
        barWidth: 9,
        colorMap: {
            '7': '#a1a1a1'
        },
        barSpacing: 2,
        barColor: '#26B99A'
    });

    $(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
        type: 'line',
        width: '200',
        height: '40',
        lineColor: '#26B99A',
        fillColor: 'rgba(223, 223, 223, 0.57)',
        lineWidth: 2,
        spotColor: '#26B99A',
        minSpotColor: '#26B99A'
    });
});
//endregion

//region Functions
function iSwitchOnChange(id, status) {
    var systemId = id.slice(14);
    var el = $('#panel-body-'+systemId);
    blockUI(el);
    $('#system-led-status-'+systemId).attr('class', '').addClass('led-green g-blink-n');

    if(id.substring(0, 13) == 'system-status') {
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'system/change-system-status',
            data: {systemid: systemId, status:status},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    $('#system-led-status-'+systemId).attr('class', '').addClass('led-green g-blink-f');
                    if(status == 1) {
                        setTimeout(function () {
                            $('#system-led-status-'+systemId).attr('class', '').addClass('led-green');
                        }, 1500);
                    }
                    else {
                        setTimeout(function () {
                            $('#system-led-status-'+systemId).attr('class', '').addClass('led-yellow');
                        }, 1500);
                    }
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}

//endregion