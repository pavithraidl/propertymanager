/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 19/12/16 12:22.
 */

"use strict";

"use strict";
function errorExecution(id, msg, from) {
    if (!id.parent().hasClass('has-error')) {
        var idName = id.attr('id');
        id.parent().addClass('has-error');
        id.parent().append('<span id="' + idName + '-' + from + '-error-msg" class="error-msg" style="color: #d92122; font-size: 12px !important;"><i class="fa fa-exclamation-circle"></i> ' + msg + '</span>').hide().fadeIn(300);
    }
}
function removeError(id, from) {
    id.parent().removeClass('has-error');
    var idName = id.attr('id');
    $('#' + idName + '-' + from + '-error-msg').fadeOut(200, function () {
        $(this).remove();
    })
}
function requiredValidator(object, msg) {
    var id = $('#' + object.id);
    var value = $.trim(id.val());
    if (value == '' || value == null) {
        errorExecution(id, msg, 'required')
    } else {
        removeError(id, 'required');
    }
}
function emailValidator(object, msg) {
    var id = $('#' + object.id);
    var value = $.trim(id.val());
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (value != '') {
        if (!re.test(value)) {
            if (!id.parent().find('.error-msg').length > 0) {
                errorExecution(id, msg, 'email');
            }
        } else {
            removeError(id, 'email');
        }
    } else {
        var idName = id.attr('id');
        $('#' + idName + '-email' + '-error-msg').fadeOut(200, function () {
            $(this).remove();
        })
    }
}

function maxVal(object, max) {
    var id = $('#'+object.id);
    var value = id.val();
    if(value >= max) {
        return false;
    }
}

$('.only-digits').bind('keypress', function (e) {
    if (e.keyCode < 48 || e.keyCode > 57) {
        if(e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 46 || e.keyCode == 40 || e.keyCode == 39) {

        }
        else {
            return false;
        }
    }
});

$('.phone-number').bind('keypress', function (e) {
    console.log(e.keyCode);
    if (e.keyCode < 48 || e.keyCode > 57) {
        if(e.keyCode == 43 || e.keyCode == 35) {

        }
        else {
            return false;
        }
    }
});

$('.only-numbers').bind('keypress', function (e) {
    if (e.keyCode < 48 || e.keyCode > 57) {
        if (e.keyCode == 46) {

        }
        else {
            return false;
        }
    }
});

//check whether file exsist
function doesFileExist(urlToFile)
{
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', urlToFile, false);
    xhr.send();

    if (xhr.status == "404") {
        return false;
    } else {
        return true;
    }
}