-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 25, 2017 at 08:49 AM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pavithra_propertymanagerdb`
--
CREATE DATABASE IF NOT EXISTS `pavithra_propertymanagerdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `pavithra_propertymanagerdb`;

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE IF NOT EXISTS `activity_log` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `activity` varchar(128) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `userid`, `orgid`, `activity`, `created_at`) VALUES
(1, 10, 1, 'Login to the system', '2017-02-10 21:18:47'),
(2, 10, 1, 'Login to the system', '2017-02-10 21:18:50'),
(3, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(4, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(5, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(6, 11, 1, 'Login to the system', '2017-02-10 21:21:15'),
(7, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(8, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(9, 10, 1, 'Enabled user - Yahoo Yahoo(pavithraidl@yahoo.com) access to Dashboard', '2017-02-10 21:21:15'),
(10, 10, 1, 'Enabled user - Yahoo Yahoo(pavithraidl@yahoo.com) access to Dashboard', '2017-02-10 21:21:15'),
(11, 10, 1, 'Enabled user - Yahoo Yahoo(pavithraidl@yahoo.com) access to Dashboard', '2017-02-10 21:21:15'),
(12, 10, 1, 'Disabled user - Yahoo Yahoo(pavithraidl@yahoo.com) access to Dashboard', '2017-02-10 21:21:15'),
(13, 10, 1, 'Enabled user - Yahoo Yahoo(pavithraidl@yahoo.com) access to Dashboard', '2017-02-10 21:21:15'),
(14, 10, 1, 'Enabled user - Yahoo Isuru(pavithraidl@yahoo.com) access to Assets', '2017-02-10 21:21:15'),
(15, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(16, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(17, 10, 1, 'Disabled user - Yahoo Isuru(pavithraidl@yahoo.com) User account', '2017-02-10 21:21:15'),
(18, 10, 1, 'Enabled user - Yahoo Isuru(pavithraidl@yahoo.com) User account', '2017-02-10 21:21:15'),
(19, 10, 1, 'Resent the activation email to Test (test@test.com)', '2017-02-10 21:21:15'),
(20, 10, 1, 'Resent the activation email to Test (test@test.com)', '2017-02-10 21:21:15'),
(21, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(22, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(23, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(24, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(25, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(26, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(27, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(28, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(29, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(30, 10, 1, 'Enabled user - Yahoo Isuru(pavithraidl@yahoo.com) access to Inspections', '2017-02-10 21:21:15'),
(31, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(32, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(33, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(34, 10, 1, 'Enabled user - Yahoo Isuru() User account', '2017-02-10 21:21:15'),
(35, 10, 1, 'Disabled user - Yahoo Isuru() User account', '2017-02-10 21:21:15'),
(36, 10, 1, 'Enabled user - Yahoo Isuru() User account', '2017-02-10 21:21:15'),
(37, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(38, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(39, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(40, 11, 1, 'Login to the system', '2017-02-10 21:21:15'),
(41, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(42, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(43, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(44, 10, 1, 'Enabled user - Yahoo Isuru(pavithraidl@yahoo.com) access to Inspections', '2017-02-10 21:21:15'),
(45, 10, 1, 'Disabled user - Yahoo Isuru() User account', '2017-02-10 21:21:15'),
(46, 10, 1, 'Enabled user - Yahoo Isuru() User account', '2017-02-10 21:21:15'),
(47, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(48, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(49, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(50, 11, 1, 'Login to the system', '2017-02-10 21:21:15'),
(51, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(52, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(53, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(54, 10, 1, 'Enabled user - Yahoo Isuru(pavithraidl@yahoo.com) access to Inspections', '2017-02-10 21:21:15'),
(55, 10, 1, 'Login to the system', '2017-02-10 14:00:03'),
(56, 10, 1, 'Enabled Organization - Test Org''s Organization account', '2017-02-10 14:11:48'),
(57, 10, 1, 'Enabled Organization -   access to Assets', '2017-02-10 14:21:16'),
(58, 10, 1, 'Enabled Organization - Test Org  access to Assets', '2017-02-10 14:22:08'),
(59, 10, 1, 'Deleted - Test Org''s Organization account User account', '2017-02-10 14:41:25'),
(60, 10, 1, 'Login to the system', '2017-02-11 11:13:58'),
(61, 10, 1, 'Login to the system', '2017-02-11 11:19:32'),
(62, 10, 1, 'Login to the system', '2017-02-12 05:11:05'),
(63, 10, 1, 'Deleted - Test Org''s Organization account User account', '2017-02-12 05:11:27'),
(64, 13, 3, 'Login to the system', '2017-02-14 08:13:01'),
(65, 10, 1, 'Login to the system', '2017-02-14 08:17:18'),
(66, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Dashboard', '2017-02-14 08:20:45'),
(67, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Assets', '2017-02-14 08:20:48'),
(68, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Inspections', '2017-02-14 08:20:50'),
(69, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Services', '2017-02-14 08:20:54'),
(70, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Users', '2017-02-14 08:20:58'),
(71, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Settings', '2017-02-14 08:21:00'),
(72, 10, 1, 'Disabled Organization - Gamma (Pvt) Ltd  access to Assets', '2017-02-14 08:21:28'),
(73, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Users', '2017-02-14 08:39:27'),
(74, 10, 1, 'Disabled Organization - Gamma (Pvt) Ltd  access to Users', '2017-02-14 08:39:35'),
(75, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Users', '2017-02-14 08:39:51'),
(76, 10, 1, 'Disabled Organization - Gamma (Pvt) Ltd  access to Users', '2017-02-14 08:42:33'),
(77, 10, 1, 'Login to the system', '2017-02-14 10:35:09'),
(78, 10, 1, 'Login to the system', '2017-02-14 12:23:14'),
(79, 10, 1, 'Login to the system', '2017-02-14 14:40:05'),
(80, 10, 1, 'Login to the system', '2017-02-15 06:54:44'),
(81, 10, 1, 'Login to the system', '2017-02-15 08:12:26'),
(82, 13, 3, 'Login to the system', '2017-02-15 10:01:03'),
(83, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Inspectors', '2017-02-15 10:02:16'),
(84, 10, 1, 'Disabled Organization - Gamma (Pvt) Ltd''s Organization account', '2017-02-15 10:07:25'),
(85, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd''s Organization account', '2017-02-15 10:07:29'),
(86, 10, 1, 'Login to the system', '2017-02-15 10:47:07'),
(87, 10, 1, 'Login to the system', '2017-02-19 22:35:36'),
(88, 10, 1, 'Login to the system', '2017-02-20 21:59:49'),
(89, 10, 1, 'Login to the system', '2017-02-20 22:29:54'),
(90, 10, 1, 'Login to the system', '2017-02-22 08:33:10'),
(91, 10, 1, 'Login to the system', '2017-02-22 12:35:42'),
(92, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 13:47:55'),
(93, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 14:34:16'),
(94, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 15:10:08'),
(95, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 15:10:29'),
(96, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 15:10:38'),
(97, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 15:10:42'),
(98, 10, 1, 'Login to the system', '2017-02-22 18:34:40'),
(99, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 18:34:51'),
(100, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 18:39:15'),
(101, 10, 1, 'Login to the system', '2017-02-22 21:48:30'),
(102, 10, 1, 'Login to the system', '2017-02-22 21:49:15'),
(103, 10, 1, 'Login to the system', '2017-02-24 07:19:07'),
(104, 10, 1, 'Login to the system', '2017-02-24 11:02:47'),
(105, 10, 1, 'Login to the system', '2017-02-24 11:04:39'),
(106, 10, 1, 'Login to the system', '2017-02-25 07:29:45'),
(107, 10, 1, 'Login to the system', '2017-02-25 07:41:13'),
(108, 10, 1, 'Login to the system', '2017-02-25 10:30:34'),
(109, 10, 1, 'Login to the system', '2017-02-26 07:24:50'),
(110, 10, 1, 'Login to the system', '2017-02-26 14:08:13'),
(111, 10, 1, 'Login to the system', '2017-02-26 21:09:29'),
(112, 10, 1, 'Login to the system', '2017-02-26 21:12:50'),
(113, 10, 1, 'Login to the system', '2017-02-27 22:00:02'),
(114, 10, 1, 'Login to the system', '2017-02-28 08:05:25'),
(115, 10, 1, 'Login to the system', '2017-02-28 19:06:32'),
(116, 10, 1, 'Login to the system', '2017-03-01 04:51:06'),
(117, 10, 1, 'Login to the system', '2017-03-01 08:33:59'),
(118, 10, 1, 'Login to the system', '2017-03-01 19:38:28'),
(119, 10, 1, 'Login to the system', '2017-03-02 03:54:58'),
(120, 10, 1, 'Login to the system', '2017-03-02 09:51:05'),
(121, 10, 1, 'Login to the system', '2017-03-02 14:38:31'),
(122, 10, 1, 'Login to the system', '2017-03-03 16:30:04'),
(123, 10, 1, 'Login to the system', '2017-03-03 18:01:12'),
(124, 10, 1, 'Login to the system', '2017-03-03 18:11:38'),
(125, 10, 1, 'Login to the system', '2017-03-03 18:54:10'),
(126, 10, 1, 'Login to the system', '2017-03-03 22:28:31'),
(127, 10, 1, 'Login to the system', '2017-03-03 22:38:06'),
(128, 10, 1, 'Login to the system', '2017-03-05 08:50:44'),
(129, 10, 1, 'Login to the system', '2017-03-05 13:32:48'),
(130, 10, 1, 'Login to the system', '2017-03-05 23:12:15'),
(131, 10, 1, 'Started a new inspection in property - 10000', '2017-03-06 00:05:07'),
(132, 10, 1, 'Login to the system', '2017-03-06 08:38:48'),
(133, 10, 1, 'Disabled user - Yahoo Isuru(pavithra@yahoo.com) User account', '2017-03-06 09:14:14'),
(134, 10, 1, 'Enabled user - Yahoo Isuru(pavithra@yahoo.com) User account', '2017-03-06 09:14:37'),
(135, 10, 1, 'Login to the system', '2017-03-08 08:59:33'),
(136, 10, 1, 'Login to the system', '2017-03-10 07:38:17'),
(137, 10, 1, 'Login to the system', '2017-03-13 04:49:12'),
(138, 10, 1, 'Login to the system', '2017-03-13 09:20:15'),
(139, 10, 1, 'Disabled Organization - Test Org''s Organization account', '2017-03-13 09:21:14'),
(140, 10, 1, 'Enabled Organization - Test Org''s Organization account', '2017-03-13 09:21:22'),
(141, 10, 1, 'Login to the system', '2017-03-13 12:54:08'),
(142, 10, 1, 'Login to the system', '2017-03-13 14:45:00'),
(143, 10, 1, 'Login to the system', '2017-03-16 00:21:24'),
(144, 10, 1, 'Login to the system', '2017-03-16 00:23:42'),
(145, 10, 1, 'Login to the system', '2017-03-16 05:49:59'),
(146, 10, 1, 'Deleted - Test User User account', '2017-03-16 05:51:11'),
(147, 16, 1, 'Login to the system', '2017-03-16 05:53:37'),
(148, 17, 1, 'Login to the system', '2017-03-16 05:57:33'),
(149, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access to Properties', '2017-03-16 06:18:06'),
(150, 10, 1, 'Disabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access to Properties', '2017-03-16 06:18:07'),
(151, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access to Organizations', '2017-03-16 06:18:13'),
(152, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access to Inspectors', '2017-03-16 06:18:16'),
(153, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access to Inspectors', '2017-03-16 06:18:29'),
(154, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access to Organizations', '2017-03-16 06:18:31'),
(155, 17, 1, 'Disabled user - Pavithra Isuru(pavithraisuru@gmail.com) User account', '2017-03-16 06:27:03'),
(156, 17, 1, 'Enabled user - Pavithra Isuru(pavithraisuru@gmail.com) User account', '2017-03-16 06:27:22'),
(157, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access to Properties', '2017-03-16 06:34:11'),
(158, 10, 1, 'Disabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access to Properties', '2017-03-16 06:34:36'),
(159, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access to Inspections', '2017-03-16 06:34:55'),
(160, 10, 1, 'Disabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access to Inspections', '2017-03-16 06:35:02'),
(161, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access to Properties', '2017-03-16 06:35:11'),
(162, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access to Inspections', '2017-03-16 06:35:14'),
(163, 10, 1, 'Deleted - Testing  User account', '2017-03-16 06:36:33'),
(164, 10, 1, 'Login to the system', '2017-03-16 06:41:28'),
(165, 10, 1, 'Login to the system', '2017-03-16 09:02:59'),
(166, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access in the Inspectors Page to Remove User', '2017-03-16 09:03:17'),
(167, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access in the Inspectors Page to Change Users Access', '2017-03-16 09:03:19'),
(168, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access in the Inspectors Page to View Activity Log', '2017-03-16 09:03:20'),
(169, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access in the Inspectors Page to Remove User', '2017-03-16 09:03:26'),
(170, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access in the Inspectors Page to Change Users Access', '2017-03-16 09:03:27'),
(171, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access in the Inspectors Page to View Activity Log', '2017-03-16 09:03:28'),
(172, 13, 3, 'Login to the system', '2017-03-16 09:05:36'),
(173, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-03-16 09:09:49'),
(174, 20, 1, 'Login to the system', '2017-03-16 09:13:24'),
(175, 10, 1, 'Enabled user - Yahoo Test(pavithraidl@yahoo.com) access to Properties', '2017-03-16 09:13:46'),
(176, 10, 1, 'Enabled user - Yahoo Test(pavithraidl@yahoo.com) access to Inspections', '2017-03-16 09:13:48'),
(177, 10, 1, 'Enabled user - Yahoo Test(pavithraidl@yahoo.com) access to Inspectors', '2017-03-16 09:13:51'),
(178, 10, 1, 'Enabled user - Yahoo Test(pavithraidl@yahoo.com) access to Organizations', '2017-03-16 09:13:53'),
(179, 10, 1, 'Enabled user - Yahoo Test(pavithraidl@yahoo.com) access in the Inspectors Page to Activate/Deactivate User', '2017-03-16 09:19:06'),
(180, 10, 1, 'Enabled user - Yahoo Test(pavithraidl@yahoo.com) access in the Inspectors Page to View Activity Log', '2017-03-16 09:19:07'),
(181, 10, 1, 'Enabled user - Yahoo Test(pavithraidl@yahoo.com) access in the Inspectors Page to Change Users Access', '2017-03-16 09:19:09'),
(182, 10, 1, 'Enabled user - Yahoo Test(pavithraidl@yahoo.com) access in the Inspectors Page to Remove User', '2017-03-16 09:19:10'),
(183, 20, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) access in the Inspectors Page to Activate/Deactivate User', '2017-03-16 09:21:44'),
(184, 10, 1, 'Login to the system', '2017-03-16 09:48:28'),
(185, 18, 1, 'Login to the system', '2017-03-16 12:48:32'),
(186, 18, 1, 'Deleted - Avfdsgxv  User account', '2017-03-16 12:59:06'),
(187, 18, 1, 'Deleted - Zxseqwe  User account', '2017-03-16 13:04:04'),
(188, 18, 1, 'Deleted - 1a  User account', '2017-03-16 13:35:58'),
(189, 18, 1, 'Deleted - Abcdefghijklmnop  User account', '2017-03-16 13:56:05'),
(190, 10, 1, 'Login to the system', '2017-03-16 13:57:41'),
(191, 18, 1, 'Deleted - -1  User account', '2017-03-16 14:19:04'),
(192, 18, 1, 'Deleted - Abc  User account', '2017-03-16 14:19:11'),
(193, 10, 1, 'Login to the system', '2017-03-16 23:15:52'),
(194, 10, 1, 'Login to the system', '2017-03-17 00:45:06'),
(195, 10, 1, 'Login to the system', '2017-03-17 06:50:02'),
(196, 28, 1, 'Login to the system', '2017-03-17 06:54:28'),
(197, 28, 1, 'Login to the system', '2017-03-17 06:55:24'),
(198, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access to Dashboard', '2017-03-17 06:55:41'),
(199, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access to Properties', '2017-03-17 06:55:45'),
(200, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access to Inspections', '2017-03-17 06:55:48'),
(201, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access to Services', '2017-03-17 06:55:50'),
(202, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access to Inspectors', '2017-03-17 06:55:52'),
(203, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access in the Inspectors Page to Remove User', '2017-03-17 06:55:54'),
(204, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access in the Inspectors Page to Change Users Access', '2017-03-17 06:55:54'),
(205, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access in the Inspectors Page to View Activity Log', '2017-03-17 06:55:55'),
(206, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access in the Inspectors Page to Activate/Deactivate User', '2017-03-17 06:55:55'),
(207, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access to Organizations', '2017-03-17 06:55:57'),
(208, 10, 1, 'Enabled user - Tana Turei(tdprturei@gmail.com) access to Settings', '2017-03-17 06:55:59'),
(209, 10, 1, 'Login to the system', '2017-03-17 06:57:00'),
(210, 10, 1, 'Login to the system', '2017-03-17 08:42:11'),
(211, 10, 1, 'Deleted - 1222222222222222  User account', '2017-03-17 09:49:40'),
(212, 28, 1, 'Login to the system', '2017-03-17 09:54:34'),
(213, 28, 1, 'Login to the system', '2017-03-17 11:39:06'),
(214, 18, 1, 'Login to the system', '2017-03-17 12:31:11'),
(215, 18, 1, 'Deleted - Abbbbbbbbbbbbbbb  User account', '2017-03-17 12:42:27'),
(216, 18, 1, 'Resent the activation email to Nav (navu171jeet@gmail.com)', '2017-03-17 12:48:56'),
(217, 18, 1, 'Resent the activation email to Nav (navu171jeet@gmail.com)', '2017-03-17 13:11:51'),
(218, 18, 1, 'Deleted - Nav Jeet User account', '2017-03-17 13:43:30'),
(219, 18, 1, 'Resent the activation email to Navjeet (navu171jeet@gmail.com)', '2017-03-17 13:46:31'),
(220, 18, 1, 'Resent the activation email to !@#$%^&&*( (#$%%^^@gmail.com)', '2017-03-17 14:01:42'),
(221, 18, 1, 'Deleted - Navjeet  User account', '2017-03-17 14:07:58'),
(222, 18, 1, 'Resent the activation email to !@#$%^&&*( (#$%%^^@gmail.com)', '2017-03-17 14:10:28'),
(223, 18, 1, 'Enabled user - !@#$%^&&*( (#$%%^^@gmail.com) access to Dashboard', '2017-03-17 14:10:34'),
(224, 18, 1, 'Enabled user - !@#$%^&&*( (#$%%^^@gmail.com) access to Inspectors', '2017-03-17 14:10:37'),
(225, 18, 1, 'Enabled user - !@#$%^&&*( (#$%%^^@gmail.com) access to Organizations', '2017-03-17 14:10:42'),
(226, 18, 1, 'Disabled user - !@#$%^&&*( (#$%%^^@gmail.com) access to Organizations', '2017-03-17 14:10:46'),
(227, 18, 1, 'Disabled user - !@#$%^&&*( (#$%%^^@gmail.com) access to Inspectors', '2017-03-17 14:10:49'),
(228, 18, 1, 'Disabled user - !@#$%^&&*( (#$%%^^@gmail.com) access to Dashboard', '2017-03-17 14:10:51'),
(229, 18, 1, 'Deleted - !@#$%^&&*(  User account', '2017-03-17 14:11:38'),
(230, 18, 1, 'Login to the system', '2017-03-17 14:16:19'),
(231, 18, 1, 'Login to the system', '2017-03-17 14:19:04'),
(232, 10, 1, 'Login to the system', '2017-03-17 14:59:47'),
(233, 10, 1, 'Login to the system', '2017-03-18 03:06:11'),
(234, 10, 1, 'Login to the system', '2017-03-18 06:17:30'),
(235, 10, 1, 'Login to the system', '2017-03-18 12:00:12'),
(236, 10, 1, 'Login to the system', '2017-03-18 14:02:34'),
(237, 10, 1, 'Login to the system', '2017-03-19 01:00:22'),
(238, 10, 1, 'Login to the system', '2017-03-19 11:40:41'),
(239, 10, 1, 'Started a new inspection in property - 1110009', '2017-03-19 12:19:49'),
(240, 10, 1, 'Login to the system', '2017-03-19 12:21:07'),
(241, 10, 1, 'Login to the system', '2017-03-19 13:03:26'),
(242, 10, 1, 'Deleted - Navjeet Kaur User account', '2017-03-19 13:05:42'),
(243, 28, 1, 'Login to the system', '2017-03-20 01:40:55'),
(244, 28, 1, 'Started a new inspection in property - 1110013', '2017-03-20 01:47:00'),
(245, 10, 1, 'Login to the system', '2017-03-20 01:47:20'),
(246, 28, 1, 'Login to the system', '2017-03-20 11:07:29'),
(247, 10, 1, 'Login to the system', '2017-03-20 12:28:00'),
(248, 10, 1, 'Login to the system', '2017-03-20 12:34:03'),
(249, 28, 1, 'Login to the system', '2017-03-21 01:41:51'),
(250, 28, 1, 'Login to the system', '2017-03-21 07:25:15'),
(251, 28, 1, 'Login to the system', '2017-03-21 07:25:22'),
(252, 28, 1, 'Enabled user - James Turei(jturei007@gmail.com) access to Properties', '2017-03-21 08:34:55'),
(253, 28, 1, 'Enabled user - James Turei(jturei007@gmail.com) access to Inspections', '2017-03-21 08:34:59'),
(254, 28, 1, 'Enabled user - James Turei(jturei007@gmail.com) access to Dashboard', '2017-03-21 08:35:10'),
(255, 28, 1, 'Login to the system', '2017-03-22 00:22:18'),
(256, 10, 1, 'Login to the system', '2017-03-23 03:03:31'),
(257, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access to Inspectors', '2017-03-23 03:10:10'),
(258, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access in the Inspectors Page to Remove User', '2017-03-23 03:10:11'),
(259, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access in the Inspectors Page to Change Users Access', '2017-03-23 03:10:12'),
(260, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access in the Inspectors Page to View Activity Log', '2017-03-23 03:10:14'),
(261, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access in the Inspectors Page to Activate/Deactivate User', '2017-03-23 03:10:15'),
(262, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access to Inspections', '2017-03-23 03:10:19'),
(263, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access to Properties', '2017-03-23 03:10:21'),
(264, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access to Organizations', '2017-03-23 03:10:28'),
(265, 34, 1, 'Login to the system', '2017-03-23 03:10:43'),
(266, 10, 1, 'Disabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access to Properties', '2017-03-23 03:11:47'),
(267, 10, 1, 'Disabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access to Inspections', '2017-03-23 03:11:51'),
(268, 17, 1, 'Login to the system', '2017-03-23 03:37:25'),
(269, 10, 1, 'Login to the system', '2017-03-23 03:48:14'),
(270, 10, 1, 'Login to the system', '2017-03-23 04:04:21'),
(271, 10, 1, 'Disabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) User account', '2017-03-23 04:04:57'),
(272, 10, 1, 'Enabled user - Rupinder Kaur(rupinder.sekhon1993@gmail.com) User account', '2017-03-23 04:05:01'),
(273, 10, 1, 'Deleted - Navjeet Kaur User account', '2017-03-23 04:05:31'),
(274, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access to Properties', '2017-03-23 04:27:26'),
(275, 10, 1, 'Enabled user - Navjeet Kaur(navjeetkaur809@gmail.com) access to Inspections', '2017-03-23 04:27:28'),
(276, 10, 1, 'Login to the system', '2017-03-23 07:08:34'),
(277, 10, 1, 'Login to the system', '2017-03-23 09:38:15'),
(278, 10, 1, 'Login to the system', '2017-03-23 13:58:58'),
(279, 10, 1, 'Login to the system', '2017-03-23 23:41:47'),
(280, 28, 1, 'Login to the system', '2017-03-24 00:03:58'),
(281, 10, 1, 'Login to the system', '2017-03-24 00:19:44'),
(282, 10, 1, 'Login to the system', '2017-03-24 02:57:55'),
(283, 28, 1, 'Login to the system', '2017-03-24 07:07:07'),
(284, 28, 1, 'Login to the system', '2017-03-25 06:57:53'),
(285, 10, 1, 'Login to the system', '2017-03-25 13:45:46'),
(286, 10, 1, 'Login to the system', '2017-03-25 17:32:47');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL,
  `fname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_address`
--

DROP TABLE IF EXISTS `contact_address`;
CREATE TABLE IF NOT EXISTS `contact_address` (
  `id` int(11) NOT NULL,
  `contactid` int(11) DEFAULT NULL,
  `street` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adline1` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adline2` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postalcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_email`
--

DROP TABLE IF EXISTS `contact_email`;
CREATE TABLE IF NOT EXISTS `contact_email` (
  `id` int(11) NOT NULL,
  `contactid` int(11) DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_phone`
--

DROP TABLE IF EXISTS `contact_phone`;
CREATE TABLE IF NOT EXISTS `contact_phone` (
  `id` int(11) NOT NULL,
  `contactid` int(11) DEFAULT NULL,
  `number` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inspections`
--

DROP TABLE IF EXISTS `inspections`;
CREATE TABLE IF NOT EXISTS `inspections` (
  `id` int(11) NOT NULL,
  `scheduleid` int(11) DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `coverimage` int(11) DEFAULT NULL,
  `inspector` varchar(32) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `residential` varchar(3) DEFAULT NULL,
  `age` varchar(32) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `bedrooms` int(11) DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `whether` varchar(16) DEFAULT NULL,
  `present_people` varchar(128) DEFAULT NULL,
  `other` text,
  `limitation` text,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspections`
--

INSERT INTO `inspections` (`id`, `scheduleid`, `orgid`, `coverimage`, `inspector`, `address`, `residential`, `age`, `type`, `bedrooms`, `bathrooms`, `whether`, `present_people`, `other`, `limitation`, `status`, `created_at`) VALUES
(8, 2, 1, NULL, '10', NULL, 'Yes', 'Nearly 10 years', 0, 5, 3, 'Fine', 'Property Owner', 'Nothing', '<p>(a)This is a report of a visual only, non-invasive inspection of the areas of the building which were readily visible at the time of inspection. The inspection did not include any areas or components which were concealed or closed in behind finished surfaces (such as plumbing, drainage, heating, framing, ventilation, insulation or wiring) or which required the moving of anything which impeded access or limited visibility (such as floor coverings, furniture, appliances, personal (property, vehicles, vegetation, debris or soil).</p><p>(b)The inspection did not assess compliance with the NZ Building Code including the Code’s weather tightness<span>&nbsp;</span>requirements, or structural aspects. On request, specialist inspections can be arranged of weather tightness or structure or of any systems including electrical, plumbing, gas or heating.</p><p>(c) The purpose of the inspection was to assess the general condition of the building based on the limited visual inspection described in this report and may not identify all past, present or future defects. Descriptions in this report of systems or appliances relate to existence only and not adequacy or life expectancy. Any area or component of the building or any item or system not specifically identified in this report as having been inspected was excluded from the scope of the inspection.</p><p><span>(d) This report has been prepared on the basis of a visual inspection of the building works using normal readily available access, and without testing of components for the assessment of the overall structural condition of it and associated items, and without recourse to construction drawings.</span></p><p>(e) This report is based on experience and reasonable opinion however is not a guarantee against moisture ingress at the time ofinspection or in the future. This inspection has been done to the writer’s best ability with all reasonable care taken using&nbsp;visual and non-invasive testing with meters as noted. This report is a guide only (as per NZ Standard) and not a guarantee against moisture ingress or structural failure and is to be accepted as such by the owner.</p><p><span>(f) It is confirmed that no detailed geotechnical investigation has been included in this brief. An investigation of the condition and location of underground drainage and services and of electrical, gas and plumbing (except as otherwise may be described in this report) is not included in this brief.</span></p><p>(g) No warranty can be given as to other defects, not apparent to visual inspection at the time; inclusive of underground services, waterproofing, soil stability or the moisture content in partitions or exterior claddings.</p><p>(h) Weather conditions can affect moisture found e.g. long dry spells, driving rain in certain directions which can cause localised leaks and may only occur three to four times per year. Guidelines as below, flashings, ground levels, etc. This stresses the importance of flashings, ground levels, etc., which may be highlighted in this report.</p><p>(i) This property report does not include the structural, electrical, plumbing or gas piping and fitting, home heating state of the premises, as our consultants are not qualified for this but can arrange for these areas to be inspected by those people whose qualifications enable them to do so.</p><p><span>(j) This report does not include any positioning of building or improvements in relation to site boundaries, or provide any guarantee whatsoever those items surveyed will not fail at some later date, and information herein pertains strictly to observations the day of inspection and accessibility only.</span></p><p>(k) If the property is controlled by a Body Corporate or similar it would be recommended prior to purchase a copy of the minutes be obtained from the Corporate Secretary to establish the history of the inspected property or other properties under such Body Corporate. This inspection has been untaken on this sole dwelling and does not extend to remainder of complex, or common areas. The inspection is confined to the above property only and does not cover structural integrity of the entire complex.</p><p>(l) This document and information contained within is intended only for the use of the addressee named above.</p>', 1, '2017-03-18 09:00:19'),
(9, 1, 1, 98, '10', NULL, 'No', 'Nearly 10 yers', 0, 1, 1, 'Rainey', '', '', '<p>(a)This is a report of a visual only, non-invasive inspection of the areas of the building which were readily visible at the time of inspection. The inspection did not include any areas or components which were concealed or closed in behind finished surfaces (such as plumbing, drainage, heating, framing, ventilation, insulation or wiring) or which required the moving of anything which impeded access or limited visibility (such as floor coverings, furniture, appliances, personal (property, vehicles, vegetation, debris or soil).</p><p>(b)The inspection did not assess compliance with the NZ Building Code including the Code’s weather tightness<span>&nbsp;</span>requirements, or structural aspects. On request, specialist inspections can be arranged of weather tightness or structure or of any systems including electrical, plumbing, gas or heating.</p><p>(c) The purpose of the inspection was to assess the general condition of the building based on the limited visual inspection described in this report and may not identify all past, present or future defects. Descriptions in this report of systems or appliances relate to existence only and not adequacy or life expectancy. Any area or component of the building or any item or system not specifically identified in this report as having been inspected was excluded from the scope of the inspection.</p><p><span>(d) This report has been prepared on the basis of a visual inspection of the building works using normal readily available access, and without testing of components for the assessment of the overall structural condition of it and associated items, and without recourse to construction drawings.</span></p><p>(e) This report is based on experience and reasonable opinion however is not a guarantee against moisture ingress at the time of inspection or in the future. This inspection has been done to the writer’s best ability with all reasonable care taken using&nbsp;visual and non-invasive testing with meters as noted. This report is a guide only (as per NZ Standard) and not a guarantee against moisture ingress or structural failure and is to be accepted as such by the owner.</p><p><span>(f) It is confirmed that no detailed geotechnical investigation has been included in this brief. An investigation of the condition and location of underground drainage and services and of electrical, gas and plumbing (except as otherwise may be described in this report) is not included in this brief.</span></p><p>(g) No warranty can be given as to other defects, not apparent to visual inspection at the time; inclusive of underground services, waterproofing, soil stability or the moisture content in partitions or exterior claddings.</p><p>(h) Weather conditions can affect moisture found e.g. long dry spells, driving rain in certain directions which can cause localised leaks and may only occur three to four times per year. Guidelines as below, flashings, ground levels, etc. This stresses the importance of flashings, ground levels, etc., which may be highlighted in this report.</p><p>(i) This property report does not include the structural, electrical, plumbing or gas piping and fitting, home heating state of the premises, as our consultants are not qualified for this but can arrange for these areas to be inspected by those people whose qualifications enable them to do so.</p><p><span>(j) This report does not include any positioning of building or improvements in relation to site boundaries, or provide any guarantee whatsoever those items surveyed will not fail at some later date, and information herein pertains strictly to observations the day of inspection and accessibility only.</span></p><p>(k) If the property is controlled by a Body Corporate or similar it would be recommended prior to purchase a copy of the minutes be obtained from the Corporate Secretary to establish the history of the inspected property or other properties under such Body Corporate. This inspection has been untaken on this sole dwelling and does not extend to remainder of complex, or common areas. The inspection is confined to the above property only and does not cover structural integrity of the entire complex.</p><p>(l) This document and information contained within is intended only for the use of the addressee named above.</p>', 4, '2017-03-19 02:05:14'),
(10, 14, 1, 112, '10', NULL, 'Yes', 'Nearly 3 Year', 0, 3, 2, 'Fine', 'Owner', 'No other relavent information', NULL, 4, '2017-03-23 07:08:52'),
(11, 18, 1, 115, '28', NULL, 'Yes', '', 0, 1, 1, '', '', '', NULL, 4, '2017-03-20 03:03:55');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_first_page_fields`
--

DROP TABLE IF EXISTS `inspection_first_page_fields`;
CREATE TABLE IF NOT EXISTS `inspection_first_page_fields` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `field` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_first_page_fields`
--

INSERT INTO `inspection_first_page_fields` (`id`, `name`, `field`, `status`, `created_at`) VALUES
(1, 'Inspection ID', 'id', 1, '2017-03-02 17:02:14'),
(2, 'Ordered by', NULL, 0, '2017-03-02 17:02:45'),
(3, 'Mobile Number', NULL, 0, '2017-03-02 17:02:50'),
(4, 'Property Inspector', 'inspector', 1, '2017-03-04 05:51:28'),
(5, 'Address of Property', 'address', 1, '2017-03-02 17:10:30'),
(6, 'Residential ', 'residential', 1, '2017-03-02 17:11:15'),
(7, 'Age of the Property', 'age', 1, '2017-03-02 17:11:21'),
(8, 'Number of bedrooms', 'bedrooms', 1, '2017-03-02 17:11:25'),
(9, 'Number of Bathrooms', 'bathrooms', 1, '2017-03-02 17:11:27'),
(10, 'Other relevant Information', 'other', 1, '2017-03-02 17:11:42'),
(11, 'Date of Property Inspection', 'created_at', 1, '2017-03-02 17:12:11'),
(12, 'Time', NULL, 0, '2017-03-02 17:12:16'),
(13, 'Present at time of Inspection', 'present_people', 1, '2017-03-02 17:13:01'),
(14, 'Weather Conditions', 'whether', 1, '2017-03-02 17:13:05');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_limitations`
--

DROP TABLE IF EXISTS `inspection_limitations`;
CREATE TABLE IF NOT EXISTS `inspection_limitations` (
  `id` int(11) NOT NULL,
  `limitation` text,
  `orgid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_limitations`
--

INSERT INTO `inspection_limitations` (`id`, `limitation`, `orgid`, `userid`, `status`, `created_at`) VALUES
(1, '<p>(a)This is a report of a visual only, non-invasive inspection of the areas of the building which were readily visible at the time of inspection. The inspection did not include any areas or components which were concealed or closed in behind finished surfaces (such as plumbing, drainage, heating, framing, ventilation, insulation or wiring) or which required the moving of anything which impeded access or limited visibility (such as floor coverings, furniture, appliances, personal (property, vehicles, vegetation, debris or soil).</p><p>(b)The inspection did not assess compliance with the NZ Building Code including the Code’s weather tightness<span>&nbsp;</span>requirements, or structural aspects. On request, specialist inspections can be arranged of weather tightness or structure or of any systems including electrical, plumbing, gas or heating.</p><p>(c) The purpose of the inspection was to assess the general condition of the building based on the limited visual inspection described in this report and may not identify all past, present or future defects. Descriptions in this report of systems or appliances relate to existence only and not adequacy or life expectancy. Any area or component of the building or any item or system not specifically identified in this report as having been inspected was excluded from the scope of the inspection.</p><p><span>(d) This report has been prepared on the basis of a visual inspection of the building works using normal readily available access, and without testing of components for the assessment of the overall structural condition of it and associated items, and without recourse to construction drawings.</span></p><p>(e) This report is based on experience and reasonable opinion however is not a guarantee against moisture ingress at the time ofinspection or in the future. This inspection has been done to the writer’s best ability with all reasonable care taken using&nbsp;visual and non-invasive testing with meters as noted. This report is a guide only (as per NZ Standard) and not a guarantee against moisture ingress or structural failure and is to be accepted as such by the owner.</p><p><span>(f) It is confirmed that no detailed geotechnical investigation has been included in this brief. An investigation of the condition and location of underground drainage and services and of electrical, gas and plumbing (except as otherwise may be described in this report) is not included in this brief.</span></p><p>(g) No warranty can be given as to other defects, not apparent to visual inspection at the time; inclusive of underground services, waterproofing, soil stability or the moisture content in partitions or exterior claddings.</p><p>(h) Weather conditions can affect moisture found e.g. long dry spells, driving rain in certain directions which can cause localised leaks and may only occur three to four times per year. Guidelines as below, flashings, ground levels, etc. This stresses the importance of flashings, ground levels, etc., which may be highlighted in this report.</p><p>(i) This property report does not include the structural, electrical, plumbing or gas piping and fitting, home heating state of the premises, as our consultants are not qualified for this but can arrange for these areas to be inspected by those people whose qualifications enable them to do so.</p><p><span>(j) This report does not include any positioning of building or improvements in relation to site boundaries, or provide any guarantee whatsoever those items surveyed will not fail at some later date, and information herein pertains strictly to observations the day of inspection and accessibility only.</span></p><p>(k) If the property is controlled by a Body Corporate or similar it would be recommended prior to purchase a copy of the minutes be obtained from the Corporate Secretary to establish the history of the inspected property or other properties under such Body Corporate. This inspection has been untaken on this sole dwelling and does not extend to remainder of complex, or common areas. The inspection is confined to the above property only and does not cover structural integrity of the entire complex.</p><p>(l) This document and information contained within is intended only for the use of the addressee named above.</p>', 1, 10, 1, '2017-03-05 22:00:24');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_main_category`
--

DROP TABLE IF EXISTS `inspection_main_category`;
CREATE TABLE IF NOT EXISTS `inspection_main_category` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `icon` varchar(16) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_main_category`
--

INSERT INTO `inspection_main_category` (`id`, `name`, `icon`, `status`, `created_at`) VALUES
(1, 'Exterior', 'fa fa-bank', 1, '2017-02-22 21:44:42'),
(2, 'Interior', 'fa fa-qrcode', 1, '2017-02-22 21:44:56'),
(3, 'Other Findings', 'fa fa-bars', 1, '2017-02-22 21:45:08');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_media`
--

DROP TABLE IF EXISTS `inspection_media`;
CREATE TABLE IF NOT EXISTS `inspection_media` (
  `id` int(11) NOT NULL,
  `inspectionid` int(11) DEFAULT NULL,
  `rowid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `ext` varchar(8) DEFAULT NULL,
  `caption` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_media`
--

INSERT INTO `inspection_media` (`id`, `inspectionid`, `rowid`, `type`, `ext`, `caption`, `status`, `created_at`) VALUES
(112, 10, 20, 1, 'jpg', NULL, 1, '2017-03-19 12:26:18'),
(113, 10, 20, 1, 'jpg', NULL, 0, '2017-03-22 23:29:02'),
(114, 10, 20, 1, 'jpg', NULL, 1, '2017-03-19 12:29:18'),
(115, 11, 21, 1, 'jpg', NULL, 1, '2017-03-20 01:48:57'),
(116, 10, 23, 1, 'jpg', NULL, 1, '2017-03-20 12:35:00'),
(117, 10, 20, 1, 'jpg', NULL, 1, '2017-03-23 04:08:20');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_records`
--

DROP TABLE IF EXISTS `inspection_records`;
CREATE TABLE IF NOT EXISTS `inspection_records` (
  `id` int(11) NOT NULL,
  `orgid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_rows`
--

DROP TABLE IF EXISTS `inspection_rows`;
CREATE TABLE IF NOT EXISTS `inspection_rows` (
  `id` int(11) NOT NULL,
  `inspectionid` int(11) DEFAULT NULL,
  `mainid` int(11) DEFAULT NULL,
  `subid` int(11) DEFAULT NULL,
  `sectorid` int(11) DEFAULT NULL,
  `topic` varchar(64) DEFAULT NULL,
  `description` text,
  `general` text,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_rows`
--

INSERT INTO `inspection_rows` (`id`, `inspectionid`, `mainid`, `subid`, `sectorid`, `topic`, `description`, `general`, `status`, `created_at`) VALUES
(20, 10, 1, 1, 1, 'Good Condition', 'Used xyz materials to build the roof', NULL, 1, '2017-03-19 07:25:24'),
(21, 11, 1, 1, 1, NULL, NULL, NULL, 1, '2017-03-20 01:48:57'),
(22, 11, 1, 1, 3, 'all need replacing', NULL, NULL, 1, '2017-03-20 02:05:06'),
(23, 10, 1, 2, 6, NULL, NULL, NULL, 1, '2017-03-20 12:35:00');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_schedule`
--

DROP TABLE IF EXISTS `inspection_schedule`;
CREATE TABLE IF NOT EXISTS `inspection_schedule` (
  `id` int(11) NOT NULL,
  `propertyid` int(11) DEFAULT NULL,
  `inspectionid` int(11) DEFAULT NULL,
  `scheduledatetime` datetime DEFAULT NULL,
  `inspector` int(11) DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `end_at` datetime DEFAULT NULL,
  `reschedule` int(1) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_schedule`
--

INSERT INTO `inspection_schedule` (`id`, `propertyid`, `inspectionid`, `scheduledatetime`, `inspector`, `started_at`, `end_at`, `reschedule`, `status`, `created_at`) VALUES
(13, 1110008, NULL, '2017-03-24 10:00:00', 0, NULL, NULL, NULL, 1, '2017-03-19 07:45:35'),
(14, 1110009, 10, '2017-03-16 16:00:00', 18, '2017-03-19 07:19:49', NULL, NULL, 1, '2017-03-19 07:49:18'),
(15, 1110010, NULL, '2017-03-27 15:30:00', 28, NULL, NULL, NULL, 1, '2017-03-21 03:32:56'),
(16, 1110011, NULL, '2017-04-04 16:30:00', NULL, NULL, NULL, NULL, 1, '2017-03-19 11:45:42'),
(17, 1110012, NULL, '2017-03-19 08:00:00', 20, NULL, NULL, NULL, 1, '2017-03-19 08:08:15'),
(18, 1110013, 11, '2017-03-23 10:30:00', 28, '2017-03-19 20:47:00', NULL, NULL, 1, '2017-03-19 20:47:00'),
(19, 1110014, NULL, '2017-03-23 10:30:00', 28, NULL, NULL, NULL, 1, '2017-03-19 21:02:58'),
(20, 1110015, NULL, '2017-03-24 12:00:00', 28, NULL, NULL, NULL, 1, '2017-03-21 03:31:51');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_sections`
--

DROP TABLE IF EXISTS `inspection_sections`;
CREATE TABLE IF NOT EXISTS `inspection_sections` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `listorder` int(11) DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_sections`
--

INSERT INTO `inspection_sections` (`id`, `name`, `listorder`, `orgid`, `status`, `created_at`) VALUES
(1, 'Exterior', 1, 0, 1, '2017-02-15 03:11:13'),
(2, 'Garage', 2, 0, 1, '2017-02-15 03:11:13'),
(3, 'Dining Room', 3, 0, 1, '2017-02-15 03:15:06'),
(4, 'Kitchen', 4, 0, 1, '2017-02-15 03:15:06'),
(5, 'Master Bedroom', 5, 0, 1, '2017-02-15 03:15:06'),
(6, 'Bedroom', 6, 0, 1, '2017-02-15 03:15:06'),
(7, 'Bonus Room', 7, 0, 1, '2017-02-15 03:15:06'),
(8, 'Bathroom', 8, 0, 1, '2017-02-15 03:15:06'),
(9, 'Living Room', 9, 0, 1, '2017-02-15 03:15:06'),
(10, 'Laundry', 10, 0, 1, '2017-02-15 03:15:06');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_sectors`
--

DROP TABLE IF EXISTS `inspection_sectors`;
CREATE TABLE IF NOT EXISTS `inspection_sectors` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `subid` int(11) DEFAULT NULL,
  `multiple` int(11) DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_sectors`
--

INSERT INTO `inspection_sectors` (`id`, `name`, `subid`, `multiple`, `status`, `created_at`) VALUES
(1, 'Roof', 1, 0, 1, '2017-02-21 11:39:55'),
(2, 'Spouting', 1, 0, 1, '2017-02-21 11:40:02'),
(3, 'Downpipes', 1, 0, 1, '2017-02-21 11:40:11'),
(4, 'Penetrations', 1, 0, 1, '2017-02-21 11:40:20'),
(5, 'Soffits', 1, 0, 1, '2017-02-21 11:40:33'),
(6, 'Construction', 2, 0, 1, '2017-02-21 11:40:46'),
(7, 'Fascia, Barges', 2, 0, 1, '2017-02-21 11:40:59'),
(8, 'Flashings', 2, 0, 1, '2017-02-21 11:41:08'),
(9, 'Exterior Walls (inc Cladding, Roof Gables, Door & Window Heads etc)', 2, 0, 1, '2017-02-21 11:41:41'),
(10, 'Risk Junctions: Flashings', 2, 0, 1, '2017-02-21 11:42:08'),
(11, 'Risk Junctions: Cladding', 2, 0, 1, '2017-02-21 11:42:23'),
(12, 'High Risk Junctions', 2, 0, 1, '2017-02-21 11:42:34'),
(13, 'Base Cladding', 3, 0, 1, '2017-02-21 11:42:49'),
(14, 'Block Work/Concrete', 3, 0, 1, '2017-02-21 11:43:03'),
(15, 'Footing/Slabs', 3, 0, 1, '2017-02-21 11:43:13'),
(16, 'Joists', 3, 0, 1, '2017-02-21 11:43:21'),
(17, 'Sub Floor Construction', 3, 0, 1, '2017-02-21 11:43:35'),
(18, 'Doors/Hardware', 4, 0, 1, '2017-02-21 11:43:50'),
(19, 'Garage Within Roof Line: Door', 4, 0, 1, '2017-02-21 11:44:14'),
(20, 'Windows', 4, 0, 1, '2017-02-21 11:44:29'),
(21, 'Deck Barriers/Handrails/Steps/Balcony', 4, 0, 1, '2017-02-21 11:44:51'),
(22, 'Paintwork', 4, 0, 1, '2017-02-21 11:44:59'),
(23, 'Hot Water Cylinder', 5, 0, 1, '2017-02-21 11:45:14'),
(24, 'Pipers, Wastes', 5, 0, 1, '2017-02-21 11:45:26'),
(25, 'Internal Taps, Mixers, Toilets', 5, 0, 1, '2017-02-21 11:45:48'),
(26, 'Exterior Plumbing & Drainage', 5, 0, 1, '2017-02-21 11:46:04'),
(27, 'Driveway/Paths', 6, 0, 1, '2017-02-21 11:46:16'),
(28, 'Decks/Patio/Pergolas etc', 6, 0, 1, '2017-02-21 11:46:35'),
(29, 'Fencing/Retaining walls', 6, 0, 1, '2017-02-21 11:46:47'),
(30, 'Sleep-out/Shed/Carport', 7, 0, 1, '2017-02-21 11:47:07'),
(31, 'Flooring', 8, 0, 1, '2017-02-22 22:28:17'),
(32, 'Ceiling', 8, 0, 1, '2017-02-22 22:28:33'),
(33, 'Walls', 8, 0, 1, '2017-02-22 22:28:44'),
(34, 'Stairwells', 8, 0, 1, '2017-02-22 22:29:00'),
(35, 'Doors/Door Hardware', 8, 0, 1, '2017-02-22 22:29:16'),
(36, 'Windows/ Window Hardware', 8, 0, 1, '2017-02-22 22:29:35'),
(37, 'Security Hardware', 8, 0, 1, '2017-02-22 22:29:48'),
(38, 'Rooms', 9, 0, 1, '2017-02-22 22:43:10'),
(39, 'Interior Decoration', 9, 0, 1, '2017-02-22 22:43:12'),
(40, 'Floor Coverings', 9, 0, 1, '2017-02-22 22:43:23'),
(41, 'Heating', 10, 0, 1, '2017-02-22 22:43:38'),
(42, 'Gas', 10, 0, 1, '2017-02-22 22:43:49'),
(43, 'Ceiling Insulation', 10, 0, 1, '2017-02-22 22:44:00'),
(44, 'Subfloor Insulation', 10, 0, 1, '2017-02-22 22:44:10'),
(45, 'Wall Insulation', 10, 0, 1, '2017-02-22 22:44:21'),
(46, 'Internal Distribution Board', 11, 0, 1, '2017-02-22 22:44:58'),
(47, 'Fittings', 11, 0, 1, '2017-02-22 22:45:12'),
(48, 'Lights', 11, 0, 1, '2017-02-22 22:45:25'),
(49, 'Wiring', 11, 0, 1, '2017-02-22 22:45:37'),
(50, 'Flooring', 12, 0, 1, '2017-02-22 22:46:05'),
(51, 'Bench Tops/Cabinets', 12, 0, 1, '2017-02-22 22:46:19'),
(52, 'Appliances/Accessories', 12, 0, 1, '2017-02-22 22:46:31'),
(53, 'First Level', 13, 0, 1, '2017-02-22 22:47:03'),
(54, 'Ensuite', 13, 0, 1, '2017-02-22 22:47:14'),
(55, 'Ground Floor', 13, 0, 1, '2017-02-22 22:47:39'),
(56, 'Laundry in General', 14, 0, 1, '2017-02-22 22:48:08'),
(57, 'Evidence of Moisture & Mould', 15, 0, 1, '2017-02-22 22:48:56'),
(58, 'Alterations', 15, 0, 1, '2017-02-22 22:49:12'),
(59, 'Pest Evidence', 15, 0, 1, '2017-02-22 22:49:22'),
(60, 'Contamination to Site or Building', 15, 0, 1, '2017-02-22 22:49:37');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_services`
--

DROP TABLE IF EXISTS `inspection_services`;
CREATE TABLE IF NOT EXISTS `inspection_services` (
  `id` int(11) NOT NULL,
  `detailid` int(11) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `contactid` int(11) DEFAULT NULL,
  `action` varchar(16) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_sub_category`
--

DROP TABLE IF EXISTS `inspection_sub_category`;
CREATE TABLE IF NOT EXISTS `inspection_sub_category` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `mainid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_sub_category`
--

INSERT INTO `inspection_sub_category` (`id`, `name`, `mainid`, `status`, `created_at`) VALUES
(1, 'Roofline', 1, 1, '2017-02-21 11:20:34'),
(2, 'Exterior Construction', 1, 1, '2017-02-21 11:20:47'),
(3, 'Foundation', 1, 1, '2017-02-21 11:21:00'),
(4, 'Other Exterior Features', 1, 1, '2017-02-21 11:21:29'),
(5, 'Plumbing & Drainage ', 1, 1, '2017-02-21 11:21:43'),
(6, 'Outdoor Extras', 1, 1, '2017-02-21 11:22:02'),
(7, 'Out Buildings', 1, 1, '2017-02-21 11:22:14'),
(8, 'Interior Linings & Hardware', 2, 1, '2017-02-22 22:27:35'),
(9, 'Interior Rooms & Decoration', 2, 1, '2017-02-22 22:30:29'),
(10, 'Heating & Insulation', 2, 1, '2017-02-22 22:42:18'),
(11, 'Electrical', 2, 1, '2017-02-22 22:44:33'),
(12, 'Kitchen', 2, 1, '2017-02-22 22:45:54'),
(13, 'Bathrooms', 2, 1, '2017-02-22 22:46:48'),
(14, 'Laundry', 2, 1, '2017-02-22 22:47:53'),
(15, 'General', 3, 1, '2017-02-22 22:48:26');

-- --------------------------------------------------------

--
-- Table structure for table `org`
--

DROP TABLE IF EXISTS `org`;
CREATE TABLE IF NOT EXISTS `org` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `ownerid` int(11) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `org`
--

INSERT INTO `org` (`id`, `name`, `ownerid`, `url`, `status`, `created_at`) VALUES
(1, 'IDL Creations', 10, NULL, 1, '2017-02-10 11:06:06'),
(2, 'Test Org', 12, 'www.testorg.org', 1, '2017-03-13 22:21:22'),
(3, 'Gamma (Pvt) Ltd', 13, NULL, 1, '2017-02-15 23:07:29');

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

DROP TABLE IF EXISTS `property`;
CREATE TABLE IF NOT EXISTS `property` (
  `id` int(11) NOT NULL,
  `addressid` int(11) DEFAULT NULL,
  `frequency` varchar(2) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `insdate` datetime DEFAULT NULL,
  `reference` varchar(16) DEFAULT NULL,
  `alarm` varchar(16) DEFAULT NULL,
  `keylocation` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=501 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_categories`
--

DROP TABLE IF EXISTS `service_categories`;
CREATE TABLE IF NOT EXISTS `service_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_contact`
--

DROP TABLE IF EXISTS `service_contact`;
CREATE TABLE IF NOT EXISTS `service_contact` (
  `id` int(11) NOT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `companyname` varchar(64) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `telephone` varchar(16) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `systems`
--

DROP TABLE IF EXISTS `systems`;
CREATE TABLE IF NOT EXISTS `systems` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `route` varchar(32) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `listorder` int(11) DEFAULT NULL,
  `visibility` int(11) DEFAULT NULL,
  `allow_default` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `systems`
--

INSERT INTO `systems` (`id`, `name`, `route`, `icon`, `listorder`, `visibility`, `allow_default`, `status`, `created_at`) VALUES
(1, 'Dashboard', 'dashboard', 'fa-dashboard', 10, 1, 1, 1, '2017-02-03 21:33:07'),
(2, 'Property', 'property', 'fa-list', 100, 1, 0, 1, '2017-03-25 09:13:57'),
(3, 'Inspections', 'inspections', 'fa-edit', 101, 1, 0, 1, '2017-02-03 21:41:15'),
(4, 'Services', 'services', 'fa-table', 102, 1, 0, 1, '2017-02-03 21:41:15'),
(5, 'Inspectors', 'users', 'fa-user', 103, 1, 0, 1, '2017-02-14 22:08:05'),
(6, 'Organizations', 'organizations', ' fa-building-o', 99, 1, 0, 1, '2017-02-03 21:41:15'),
(7, 'Settings', 'settings', 'fa-cog', 200, 1, 0, 1, '2017-02-03 21:41:15');

-- --------------------------------------------------------

--
-- Table structure for table `system_exceptions`
--

DROP TABLE IF EXISTS `system_exceptions`;
CREATE TABLE IF NOT EXISTS `system_exceptions` (
  `id` int(11) NOT NULL,
  `controller` varchar(16) DEFAULT NULL,
  `func` varchar(16) DEFAULT NULL,
  `exception` varchar(1024) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_exceptions`
--

INSERT INTO `system_exceptions` (`id`, `controller`, `func`, `exception`, `user`, `status`, `created_at`) VALUES
(1, 'UserController', 'addUser', 'InvalidArgumentException: Route [active] not defined. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php:231\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(211): Illuminate\\Routing\\UrlGenerator->route(''active'', Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(55): Illuminate\\Support\\Facades\\Facade::__callStatic(''route'', Array)\n#2 [internal function]: UserController->addUser()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''addUser'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserContro', NULL, 2, '2017-02-01 19:49:56'),
(2, 'UserController', 'addUser', 'InvalidArgumentException: View [admin.email.welcome-user] not found. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php:146\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php(83): Illuminate\\View\\FileViewFinder->findInPaths(''admin.email.wel...'', Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Factory.php(124): Illuminate\\View\\FileViewFinder->find(''admin.email.wel...'')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(402): Illuminate\\View\\Factory->make(''admin.email.wel...'', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(272): Illuminate\\Mail\\Mailer->getView(''admin.email.wel...'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(147): Illuminate\\Mail\\Mailer->addContent(Object(Illuminate\\Mail\\Message), ''admin.email.w', NULL, 2, '2017-02-01 20:04:44'),
(3, 'UserController', 'addUser', 'InvalidArgumentException: View [email.active] not found. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php:146\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php(83): Illuminate\\View\\FileViewFinder->findInPaths(''email.active'', Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Factory.php(124): Illuminate\\View\\FileViewFinder->find(''email.active'')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(402): Illuminate\\View\\Factory->make(''email.active'', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(272): Illuminate\\Mail\\Mailer->getView(''email.active'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(147): Illuminate\\Mail\\Mailer->addContent(Object(Illuminate\\Mail\\Message), ''email.active'', NULL, Array)\n#5 /Users/isuru/Proje', NULL, 2, '2017-02-01 23:47:43'),
(4, 'UserController', 'addUser', 'ErrorException: Undefined variable: company in /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705:204\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705(204): Illuminate\\Exception\\Handler->handleError(8, ''Undefined varia...'', ''/Users/isuru/Pr...'', 204, Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/PhpEngine.php(37): include(''/Users/isuru/Pr...'')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(56): Illuminate\\View\\Engines\\PhpEngine->evaluatePath(''/Users/isuru/Pr...'', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(140): Illuminate\\View\\Engines\\CompilerEngine->get(''/Users/isuru/Pr...'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(109): Illuminate\\View\\View->getContents()\n#5 /Users/isuru/Projects/easyinspect/vendor/lar', NULL, 2, '2017-02-01 23:51:17'),
(5, 'UserController', 'addUser', 'ErrorException: Undefined variable: company in /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705:204\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705(204): Illuminate\\Exception\\Handler->handleError(8, ''Undefined varia...'', ''/Users/isuru/Pr...'', 204, Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/PhpEngine.php(37): include(''/Users/isuru/Pr...'')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(56): Illuminate\\View\\Engines\\PhpEngine->evaluatePath(''/Users/isuru/Pr...'', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(140): Illuminate\\View\\Engines\\CompilerEngine->get(''/Users/isuru/Pr...'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(109): Illuminate\\View\\View->getContents()\n#5 /Users/isuru/Projects/easyinspect/vendor/lar', NULL, 2, '2017-02-01 23:52:17'),
(6, 'UserController', 'addUser', 'ErrorException: Array to string conversion in /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705:204\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705(204): Illuminate\\Exception\\Handler->handleError(8, ''Array to string...'', ''/Users/isuru/Pr...'', 204, Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/PhpEngine.php(37): include(''/Users/isuru/Pr...'')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(56): Illuminate\\View\\Engines\\PhpEngine->evaluatePath(''/Users/isuru/Pr...'', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(140): Illuminate\\View\\Engines\\CompilerEngine->get(''/Users/isuru/Pr...'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(109): Illuminate\\View\\View->getContents()\n#5 /Users/isuru/Projects/easyinspect/vendor/lara', NULL, 2, '2017-02-01 23:59:57'),
(7, 'UserController', 'addUser', 'ErrorException: Undefined variable: company in /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705:204\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705(204): Illuminate\\Exception\\Handler->handleError(8, ''Undefined varia...'', ''/Users/isuru/Pr...'', 204, Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/PhpEngine.php(37): include(''/Users/isuru/Pr...'')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(56): Illuminate\\View\\Engines\\PhpEngine->evaluatePath(''/Users/isuru/Pr...'', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(140): Illuminate\\View\\Engines\\CompilerEngine->get(''/Users/isuru/Pr...'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(109): Illuminate\\View\\View->getContents()\n#5 /Users/isuru/Projects/easyinspect/vendor/lar', NULL, 2, '2017-02-02 00:03:19'),
(8, 'UserController', 'getActivate', 'InvalidArgumentException: View [admin.activate] not found. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php:146\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php(83): Illuminate\\View\\FileViewFinder->findInPaths(''admin.activate'', Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Factory.php(124): Illuminate\\View\\FileViewFinder->find(''admin.activate'')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(211): Illuminate\\View\\Factory->make(''admin.activate'', Array)\n#3 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(96): Illuminate\\Support\\Facades\\Facade::__callStatic(''make'', Array)\n#4 [internal function]: UserController->getActivate(''NsL6sqcBEiD3N3y...'')\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', NULL, 2, '2017-02-03 10:38:52'),
(9, 'UserController', 'postLogin', 'BadMethodCallException: Method [saveActivity] does not exist. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(61): Illuminate\\Routing\\Controller->__call(''saveActivity'', Array)\n#1 [internal function]: UserController->postLogin()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\Routing\\Route), ''postLogin'')\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Router.php(967): Ill', NULL, 2, '2017-02-03 11:02:08'),
(10, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:10:26'),
(11, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:10:49'),
(12, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:12:11'),
(13, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:13:27'),
(14, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:13:29'),
(15, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:13:38'),
(16, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:42:00'),
(17, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:42:38'),
(18, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:43:07'),
(19, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:44:48'),
(20, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:46:17'),
(21, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:47:21'),
(22, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:48:37'),
(23, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(66): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\Ro', NULL, 2, '2017-02-10 10:15:42'),
(24, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(66): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\Ro', NULL, 2, '2017-02-10 10:15:51'),
(25, 'OrganizationCont', 'getOrgList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column ''name'' in ''field list'' in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(''select `name` f...'')\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), ''select `name` f...'', Array)\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(581): Illuminate\\Database\\Connection->runQueryCallback(''select `name` f...'', Array, Object(Closure))\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(304): Illuminate\\Database\\Connection->run(''select `name` f...'', Array, Object(Closure))\n#4 /Users/isuru/Projects/easyinspect/v', NULL, 2, '2017-02-10 11:03:12'),
(26, 'OrganizationCont', 'postOrgStatus', 'BadMethodCallException: Method [saveActivity] does not exist. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/OrganizationController.php(170): Illuminate\\Routing\\Controller->__call(''saveActivity'', Array)\n#1 [internal function]: OrganizationController->postOrgStatus()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postOrgStatus'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(OrganizationController), Object(Illuminate\\Routing\\Route), ''postOrgStatus'')\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Il', NULL, 2, '2017-02-11 03:10:44'),
(27, 'UserController', 'addUser', 'Swift_TransportException: Connection could not be established with host smtp.gmail.com [Operation timed out #60] in /Users/isuru/Projects/easyinspect/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/StreamBuffer.php:269\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/StreamBuffer.php(62): Swift_Transport_StreamBuffer->_establishSocketConnection()\n#1 /Users/isuru/Projects/easyinspect/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(113): Swift_Transport_StreamBuffer->initialize(Array)\n#2 /Users/isuru/Projects/easyinspect/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mailer.php(79): Swift_Transport_AbstractSmtpTransport->start()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(329): Swift_Mailer->send(Object(Swift_Message), Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(151): Illuminate\\Mail\\Mailer->sendSwif', NULL, 2, '2017-02-15 23:05:22'),
(28, 'InspectionsContr', 'saveBasic', 'PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table ''inspectdb.inspections'' doesn''t exist in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(''insert into `in...'')\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), ''insert into `in...'', Array)\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(581): Illuminate\\Database\\Connection->runQueryCallback(''insert into `in...'', Array, Object(Closure))\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(370): Illuminate\\Database\\Connection->run(''insert into `in...'', Array, Object(Closure))\n#4 /Users/isuru/Proj', NULL, 2, '2017-02-23 02:44:54'),
(29, 'InspectionsContr', 'saveBasic', 'Illuminate\\Database\\Eloquent\\ModelNotFoundException: No query results for model [Inspection]. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php:140\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/InspectionController.php(99): Illuminate\\Database\\Eloquent\\Builder->firstOrFail()\n#1 [internal function]: InspectionController->saveBasic()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''saveBasic'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(InspectionController), Object(Illuminate\\Routing\\Route), ''saveBasic'')\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/frame', NULL, 2, '2017-02-23 07:38:12'),
(30, 'InspectionsContr', 'saveTopic', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column ''satatus'' in ''field list'' in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(''insert into `in...'')\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), ''insert into `in...'', Array)\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(581): Illuminate\\Database\\Connection->runQueryCallback(''insert into `in...'', Array, Object(Closure))\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(370): Illuminate\\Database\\Connection->run(''insert into `in...'', Array, Object(Closure))\n#4 /Users/isuru/Projects/easyinspec', NULL, 2, '2017-02-23 07:40:12'),
(31, 'InspectionsContr', 'getSector', 'Illuminate\\Database\\Eloquent\\ModelNotFoundException: No query results for model [InspectionRow]. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php:140\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/InspectionController.php(192): Illuminate\\Database\\Eloquent\\Builder->firstOrFail()\n#1 [internal function]: InspectionController->getFuck()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getFuck'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(InspectionController), Object(Illuminate\\Routing\\Route), ''getFuck'')\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framewo', NULL, 2, '2017-02-23 08:33:28'),
(32, 'InspectionsContr', 'getSector', 'Illuminate\\Database\\Eloquent\\ModelNotFoundException: No query results for model [InspectionRow]. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php:140\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/InspectionController.php(192): Illuminate\\Database\\Eloquent\\Builder->firstOrFail()\n#1 [internal function]: InspectionController->getInspectionSector()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getInspectionSe...'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(InspectionController), Object(Illuminate\\Routing\\Route), ''getInspectionSe...'')\n#5 /Users/isuru/Projects/', NULL, 2, '2017-02-23 08:36:45'),
(33, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:198\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 198, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-23 10:48:42'),
(34, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:198\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/Users/isuru/Pr...'', 198, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\Ro', NULL, 2, '2017-03-14 03:44:43'),
(35, 'InspectionsContr', 'uploadImage', 'Symfony\\Component\\HttpFoundation\\File\\Exception\\FileException: The file "image.jpg" was only partially uploaded. in /Users/isuru/Projects/easyinspect/vendor/symfony/http-foundation/Symfony/Component/HttpFoundation/File/UploadedFile.php:251\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/InspectionController.php(377): Symfony\\Component\\HttpFoundation\\File\\UploadedFile->move(''assets/images/i...'', ''45.jpg'')\n#1 [internal function]: InspectionController->uploadImage()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''uploadImage'', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(InspectionController), Object(Illuminate\\Routing\\R', NULL, 2, '2017-03-14 03:52:03'),
(36, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-16 13:22:22'),
(37, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-16 13:22:54'),
(38, 'imageProcessingC', 'BaseController', 'exception ''ErrorException'' with message ''imagecreatefromjpeg(assets/images/inspection/9/64.jpg): failed to open stream: No such file or directory'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/imageProcessingController.php:30\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, ''imagecreatefrom...'', ''/home1/pavithra...'', 30, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/imageProcessingController.php(30): imagecreatefromjpeg(''assets/images/i...'')\n#2 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/InspectionController.php(350): imageProcessingController->setImageSizes(''assets/images/i...'', ''assets/images/i...'', 80, ''jpg'', 1280, 720)\n#3 [internal function]: InspectionController->uploadImage()\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/staging/proper', NULL, 2, '2017-03-17 14:04:23'),
(39, 'imageProcessingC', 'BaseController', 'exception ''ErrorException'' with message ''Undefined variable: saveAs'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/imageProcessingController.php:48\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/imageProcessingController.php(48): Illuminate\\Exception\\Handler->handleError(8, ''Undefined varia...'', ''/home1/pavithra...'', 48, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/InspectionController.php(350): imageProcessingController->setImageSizes(''assets/images/i...'', ''assets/images/i...'', 80, ''jpg'', 1280, 720)\n#2 [internal function]: InspectionController->uploadImage()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''uploadImage'',', NULL, 2, '2017-03-17 14:05:51'),
(40, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/', NULL, 2, '2017-03-17 19:55:03'),
(41, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/', NULL, 2, '2017-03-17 19:57:23'),
(42, 'imageProcessingC', 'BaseController', 'exception ''ErrorException'' with message ''imagecreatefromjpeg(): gd-jpeg: JPEG library reports unrecoverable error: '' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/imageProcessingController.php:30\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, ''imagecreatefrom...'', ''/home1/pavithra...'', 30, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/imageProcessingController.php(30): imagecreatefromjpeg(''assets/images/i...'')\n#2 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/InspectionController.php(350): imageProcessingController->setImageSizes(''assets/images/i...'', 80, 1280, 720)\n#3 [internal function]: InspectionController->uploadImage()\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/C', NULL, 2, '2017-03-17 22:01:01'),
(43, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/', NULL, 2, '2017-03-18 03:16:08'),
(44, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/', NULL, 2, '2017-03-18 03:16:39');
INSERT INTO `system_exceptions` (`id`, `controller`, `func`, `exception`, `user`, `status`, `created_at`) VALUES
(45, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/', NULL, 2, '2017-03-18 03:29:25'),
(46, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-20 02:25:58'),
(47, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-20 02:26:13'),
(48, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-20 02:26:28'),
(49, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-20 02:27:32'),
(50, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-20 02:28:53'),
(51, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-20 02:30:21'),
(52, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-20 17:03:13'),
(53, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-21 01:27:36'),
(54, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/', NULL, 2, '2017-03-21 20:25:16'),
(55, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-23 16:10:35'),
(56, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/', NULL, 2, '2017-03-23 16:37:11'),
(57, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/', NULL, 2, '2017-03-23 16:37:44'),
(58, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-23 17:03:56'),
(59, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(72): UserController->saveActivity(''Trying to login...'')\n#2 [internal function]: UserController->postLogin()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''postLogin'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/f', NULL, 2, '2017-03-23 17:04:08'),
(60, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/', NULL, 2, '2017-03-23 17:16:15'),
(61, 'UserController', 'saveActivity', 'exception ''ErrorException'' with message ''Trying to get property of non-object'' in /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php:198\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, ''Trying to get p...'', ''/home1/pavithra...'', 198, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/app/controllers/UserController.php(191): UserController->saveActivity(''Logout from the...'')\n#2 [internal function]: UserController->getLogOut()\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(''getLogOut'', Array)\n#5 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/', NULL, 2, '2017-03-23 18:30:21'),
(62, 'PropertiesContro', 'getPropertyList', 'exception ''PDOException'' with message ''SQLSTATE[42S02]: Base table or view not found: 1146 Table ''pavithra_propertymanagerdb.assets'' doesn''t exist'' in /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(''select `id` fro...'')\n#1 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), ''select `id` fro...'', Array)\n#2 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php(581): Illuminate\\Database\\Connection->runQueryCallback(''select `id` fro...'', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/', NULL, 2, '2017-03-26 04:38:53'),
(63, 'PropertiesContro', 'getPropertyList', 'exception ''PDOException'' with message ''SQLSTATE[42S22]: Column not found: 1054 Unknown column ''propertyid'' in ''where clause'''' in /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(''select * from `...'')\n#1 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), ''select * from `...'', Array)\n#2 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php(581): Illuminate\\Database\\Connection->runQueryCallback(''select * from `...'', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Co', NULL, 2, '2017-03-26 04:39:34'),
(64, 'PropertiesContro', 'getPropertyList', 'exception ''PDOException'' with message ''SQLSTATE[42S22]: Column not found: 1054 Unknown column ''propertyid'' in ''where clause'''' in /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(''select * from `...'')\n#1 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), ''select * from `...'', Array)\n#2 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Connection.php(581): Illuminate\\Database\\Connection->runQueryCallback(''select * from `...'', Array, Object(Closure))\n#3 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Co', NULL, 2, '2017-03-26 04:41:22'),
(65, 'PropertiesContro', 'getPropertyManag', 'exception ''ErrorException'' with message ''Argument 1 passed to Illuminate\\Database\\Grammar::columnize() must be of the type array, string given, called in /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Query/Grammars/Grammar.php on line 105 and defined'' in /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Grammar.php:97\nStack trace:\n#0 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Grammar.php(97): Illuminate\\Exception\\Handler->handleError(4096, ''Argument 1 pass...'', ''/home1/pavithra...'', 97, Array)\n#1 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Query/Grammars/Grammar.php(105): Illuminate\\Database\\Grammar->columnize(''fname'')\n#2 /home1/pavithraidl/public_html/staging/propertymanager/vendor/laravel/framework/src/Illuminate/Database/Query/Grammars/Grammar.php(60): Illuminate\\Database\\Query', NULL, 2, '2017-03-26 05:35:38');

-- --------------------------------------------------------

--
-- Table structure for table `system_operations`
--

DROP TABLE IF EXISTS `system_operations`;
CREATE TABLE IF NOT EXISTS `system_operations` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `operation` varchar(32) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_operations`
--

INSERT INTO `system_operations` (`id`, `systemid`, `operation`, `icon`, `status`, `created_at`) VALUES
(1, 5, 'Remove User', 'fa fa-trash', 1, '2017-03-16 04:00:41'),
(2, 5, 'Change Users Access', 'fa fa-sliders', 1, '2017-03-16 04:01:06'),
(3, 5, 'View Activity Log', 'fa fa-bullhorn', 1, '2017-03-16 04:01:34'),
(4, 5, 'Activate/Deactivate User', 'fa fa-unlock', 1, '2017-03-16 04:16:01');

-- --------------------------------------------------------

--
-- Table structure for table `system_operation_access`
--

DROP TABLE IF EXISTS `system_operation_access`;
CREATE TABLE IF NOT EXISTS `system_operation_access` (
  `id` int(11) NOT NULL,
  `operationid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `allow` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_operation_access`
--

INSERT INTO `system_operation_access` (`id`, `operationid`, `userid`, `allow`, `created_at`) VALUES
(1, 1, 17, 1, '2017-03-16 09:03:17'),
(2, 2, 17, 1, '2017-03-16 09:03:19'),
(3, 3, 17, 1, '2017-03-16 09:03:20'),
(4, 1, 18, 1, '2017-03-16 09:03:26'),
(5, 2, 18, 1, '2017-03-16 09:03:27'),
(6, 3, 18, 1, '2017-03-16 09:03:28'),
(7, 4, 20, 1, '2017-03-16 09:19:06'),
(8, 3, 20, 1, '2017-03-16 09:19:07'),
(9, 2, 20, 1, '2017-03-16 09:19:09'),
(10, 1, 20, 1, '2017-03-16 09:19:10'),
(11, 4, 17, 1, '2017-03-16 09:21:44'),
(12, 1, 28, 1, '2017-03-17 06:55:54'),
(13, 2, 28, 1, '2017-03-17 06:55:54'),
(14, 3, 28, 1, '2017-03-17 06:55:55'),
(15, 4, 28, 1, '2017-03-17 06:55:55'),
(16, 1, 34, 1, '2017-03-23 03:10:11'),
(17, 2, 34, 1, '2017-03-23 03:10:12'),
(18, 3, 34, 1, '2017-03-23 03:10:14'),
(19, 4, 34, 1, '2017-03-23 03:10:15');

-- --------------------------------------------------------

--
-- Table structure for table `system_routingaccess`
--

DROP TABLE IF EXISTS `system_routingaccess`;
CREATE TABLE IF NOT EXISTS `system_routingaccess` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `allow` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_routingaccess`
--

INSERT INTO `system_routingaccess` (`id`, `systemid`, `userid`, `allow`, `created_at`) VALUES
(1, 1, 11, 1, '2017-02-04 20:59:18'),
(2, 2, 11, 1, '2017-02-04 08:26:02'),
(3, 3, 11, 1, '2017-02-09 08:41:00'),
(4, 2, NULL, 1, '2017-02-10 14:21:16'),
(5, 2, 12, 1, '2017-02-10 14:22:08'),
(6, 1, 13, 1, '2017-02-14 08:20:45'),
(7, 2, 13, 1, '2017-02-14 21:35:57'),
(8, 3, 13, 1, '2017-02-14 08:20:50'),
(9, 4, 13, 1, '2017-02-14 08:20:54'),
(10, 5, 13, 1, '2017-02-15 23:02:16'),
(11, 7, 13, 1, '2017-02-14 08:21:00'),
(12, 2, 17, 1, '2017-03-16 01:35:11'),
(13, 6, 17, 1, '2017-03-16 06:18:13'),
(14, 5, 17, 1, '2017-03-16 06:18:16'),
(15, 5, 18, 1, '2017-03-16 06:18:29'),
(16, 6, 18, 1, '2017-03-16 06:18:31'),
(17, 3, 17, 1, '2017-03-16 01:35:14'),
(18, 2, 20, 1, '2017-03-16 09:13:46'),
(19, 3, 20, 1, '2017-03-16 09:13:48'),
(20, 5, 20, 1, '2017-03-16 09:13:51'),
(21, 6, 20, 1, '2017-03-16 09:13:53'),
(22, 1, 28, 1, '2017-03-17 06:55:41'),
(23, 2, 28, 1, '2017-03-17 06:55:45'),
(24, 3, 28, 1, '2017-03-17 06:55:48'),
(25, 4, 28, 1, '2017-03-17 06:55:50'),
(26, 5, 28, 1, '2017-03-17 06:55:52'),
(27, 6, 28, 1, '2017-03-17 06:55:57'),
(28, 7, 28, 1, '2017-03-17 06:55:59'),
(29, 1, 33, 0, '2017-03-17 09:10:51'),
(30, 5, 33, 0, '2017-03-17 09:10:49'),
(31, 6, 33, 0, '2017-03-17 09:10:46'),
(32, 2, 29, 1, '2017-03-21 08:34:55'),
(33, 3, 29, 1, '2017-03-21 08:34:59'),
(34, 1, 29, 1, '2017-03-21 08:35:10'),
(35, 5, 34, 1, '2017-03-23 03:10:10'),
(36, 3, 34, 0, '2017-03-22 22:11:51'),
(37, 2, 34, 0, '2017-03-22 22:11:47'),
(38, 6, 34, 1, '2017-03-23 03:10:28'),
(39, 2, 35, 1, '2017-03-23 04:27:26'),
(40, 3, 35, 1, '2017-03-23 04:27:28');

-- --------------------------------------------------------

--
-- Table structure for table `tenant`
--

DROP TABLE IF EXISTS `tenant`;
CREATE TABLE IF NOT EXISTS `tenant` (
  `id` int(11) NOT NULL,
  `contactid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(16) DEFAULT NULL,
  `lname` varchar(16) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `password_temp` varchar(60) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `remember_token` varchar(120) DEFAULT NULL,
  `jobtitle` varchar(32) DEFAULT NULL,
  `roll` int(11) DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `gender`, `email`, `password`, `password_temp`, `code`, `active`, `remember_token`, `jobtitle`, `roll`, `orgid`, `status`, `updated_at`, `created_at`) VALUES
(10, 'Pavithra', 'Isuru', NULL, 'pavithraisuru@gmail.com', '$2y$10$iIBVz3lmWdA2cX5pB75jPuk/ebFjdl05zBtuN6/JDOT6jMusQwcb6', NULL, '', 1, 'vB0YVr6Iribj0Jb6bDuvfzYrsFf95gp6VoHFUqdqbUX9Ut5wKDcRD0oMCOiL', 'System Administrator', 1, 1, 1, '2017-03-22 23:16:15', '2017-03-22 23:16:15'),
(17, 'Rupinder', 'Kaur', NULL, 'rupinder.sekhon1993@gmail.com', '$2y$10$ubAcPSgXpVG/QWDT1EtOzunLTbC054VhGPewp2y3/Fx0vnOYT6wtO', NULL, '', 1, 'HWQqZR4B1Kt8VTQCxxPCn9L2QsguosGzEW9jFcq8O1qqZd9nTvFQe7S7ElYA', 'System Tester', 4, 1, 1, '2017-03-23 00:30:21', '2017-03-23 00:30:21'),
(18, 'Navjeet', 'Kaur', NULL, '', '$2y$10$Hs8TVxYgQHvwyIs.GM2s8esgRXgnd9mmeutfrz6pO/GBueX5N15A2', NULL, '', 0, '8QmnVsHDradPoWwcctEOYMZZDR8YbqZeeeSJN0wLmdcH5RXOB6HvkKfn5ht7', 'System Tester', 4, 1, 0, '2017-03-19 08:05:42', '2017-03-19 08:05:42'),
(19, 'Testing', '', NULL, '', NULL, NULL, 'FJF5wL7HzuKr3NHWp5uxHss4b2UKbOncZwadAtb7lkV47GaGPwnAu7EFM7a4', 0, NULL, 'test', 4, 1, 0, '2017-03-16 01:36:33', '2017-03-16 01:36:33'),
(20, 'Yahoo', 'Test', NULL, 'pavithraidl@yahoo.com', '$2y$10$bIXX24Psd0yYUgkh5t6P1Ox/QhMDH/OYEAayvq0LTJpIuzDe9iNlu', NULL, '', 1, NULL, 'System Test', 4, 1, 1, '2017-03-16 04:13:14', '2017-03-16 04:13:14'),
(21, 'Avfdsgxv', '', NULL, '', NULL, NULL, 'Cor82OAYoSOJB31TyqfkOttVDchHZlS7bMeZZ9ykS4NErTDeH8XfIVBVexhN', 0, NULL, 'xcvdfgdv', 4, 1, 0, '2017-03-16 07:59:06', '2017-03-16 07:59:06'),
(22, 'Zxseqwe', '', NULL, '', NULL, NULL, '4YgzoVXz3Xnimenk2YHYPNatygyfs2HkoHQZeJ9AUPMsAlHYSIfbsdXzd30F', 0, NULL, 'afsdsgt', 4, 1, 0, '2017-03-16 08:04:04', '2017-03-16 08:04:04'),
(23, '1a', '', NULL, '', NULL, NULL, 's1HKX2u4p3uSP7wGTx7pHaOXp2zNXjhlmzQX6sMSpaPmM5ykr3IyXdenusKC', 0, NULL, '-1', 4, 1, 0, '2017-03-16 08:35:58', '2017-03-16 08:35:58'),
(24, 'Abcdefghijklmnop', '', NULL, '', NULL, NULL, 'slfRv0hRsQudaVVVszN7iRpBTO9rCpwSWchiR69FoucUn3q7up5AVnsoUtcG', 0, NULL, 'abcwxyzabcdefdefghijklmnopqrstuv', 4, 1, 0, '2017-03-16 08:56:05', '2017-03-16 08:56:05'),
(25, '-1', '', NULL, '', NULL, NULL, 'LTJmEg0fMddIS6D4Oyio1rYxCdjiiGPugXUTHOulJlkvFpBWYQlfpTsoJIV7', 0, NULL, '-123461000000000000000', 4, 1, 0, '2017-03-16 09:19:04', '2017-03-16 09:19:04'),
(26, 'Abc', '', NULL, '', NULL, NULL, 'Y5WWFBhToPDX6VgAw9KUR3GkTmOQdlfpb1hl2jzjUfzraVjbnsdxcJFsau7G', 0, NULL, '', 4, 1, 0, '2017-03-16 09:19:11', '2017-03-16 09:19:11'),
(27, '1222222222222222', '', NULL, '', NULL, NULL, 'hkfV6UYa2OLf97sPQa1QdmRVCVKm3UO0UPwOY7WKGTwSogA2MSFiCfu6CaMq', 0, NULL, '44444444444444444444444444444444', 4, 1, 0, '2017-03-17 04:49:40', '2017-03-17 04:49:40'),
(28, 'Tana', 'Turei', NULL, 'tdprturei@gmail.com', '$2y$10$X7GzHbSXp5qFDMQOwpBJreG0YzedHlNP68Fxnb/5rYuASE84SDmSG', NULL, '', 1, 'AEb296CdT23Od4UOsuGOPJw9DocjMksbC9GjKm65gUG4h95sknGRhjPlcEtd', 'Owner', 4, 1, 1, '2017-03-21 02:25:16', '2017-03-21 02:25:16'),
(29, 'James', 'Turei', NULL, 'jturei007@gmail.com', NULL, NULL, 'O69eUOy8eVLm3tO79LOaBiEdI3AMH8rv1XjVIK3j4QCIaDuoVMAK5tWh1jLm', 0, NULL, 'House Inspector', 4, 1, 3, '2017-03-17 02:05:06', '2017-03-17 07:05:06'),
(30, 'Abbbbbbbbbbbbbbb', '', NULL, '', NULL, NULL, 'apmtcljHYDTWOpSFFseU3KM6Eb59SGUfck8uM9K95KaDKrKGefO6lUxflSKm', 0, NULL, 'abc', 4, 1, 0, '2017-03-17 07:42:27', '2017-03-17 07:42:27'),
(31, 'Nav', 'Jeet', NULL, '', '$2y$10$sNZb6bH7AMT9q.I66uTWV.5TR7A8GVvRa19LLpD0PgzKGSIx62LLW', NULL, '', 0, NULL, 'jeet', 4, 1, 0, '2017-03-17 08:43:30', '2017-03-17 08:43:30'),
(32, 'Navjeet', '', NULL, '', NULL, NULL, 'QujOe97ZZTXIMX68nOCJEL8IHG1nfn8n4m2Q8R44EYCyiGvcWa2mei2bsn6Z', 0, NULL, 'navjeet', 4, 1, 0, '2017-03-17 09:07:58', '2017-03-17 09:07:58'),
(33, '!@#$%^&&*(', '', NULL, '', NULL, NULL, 'UR7hstpPKmEkq88fC4qG6dYoMXmMcmsqt8wNTZqZhp6TgvRorHMjs8RH8m2G', 0, NULL, '#$%^&*&*', 4, 1, 0, '2017-03-17 09:11:38', '2017-03-17 09:11:38'),
(34, 'Navjeet', 'Kaur', NULL, '', '$2y$10$JjW2LUQmhxFDWm1bxTOHoeW6C.YgVh2UdUJBlaafbBfHyLlXiA0jK', NULL, '', 0, 'CEAZVlx2bGSYkxzXZD9fm7VcJx2HUJRqpvdmW3FpZbI6AJPO22JpMvMhsmez', 'System Tester', 4, 1, 0, '2017-03-22 23:05:31', '2017-03-22 23:05:31'),
(35, 'Navjeet', 'Kaur', NULL, 'navjeetkaur809@gmail.com', '$2y$10$.rUoarLRGI6JNw4Vc1A6vOUS.SFZGtXCnnYBseswitNBqZgu/dEcu', NULL, '', 1, NULL, 'System Tester', 4, 1, 1, '2017-03-23 00:31:38', '2017-03-23 00:31:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_address`
--
ALTER TABLE `contact_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_email`
--
ALTER TABLE `contact_email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_phone`
--
ALTER TABLE `contact_phone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspections`
--
ALTER TABLE `inspections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_first_page_fields`
--
ALTER TABLE `inspection_first_page_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_limitations`
--
ALTER TABLE `inspection_limitations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_main_category`
--
ALTER TABLE `inspection_main_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_media`
--
ALTER TABLE `inspection_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_records`
--
ALTER TABLE `inspection_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_rows`
--
ALTER TABLE `inspection_rows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_schedule`
--
ALTER TABLE `inspection_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_sections`
--
ALTER TABLE `inspection_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_sectors`
--
ALTER TABLE `inspection_sectors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_services`
--
ALTER TABLE `inspection_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_sub_category`
--
ALTER TABLE `inspection_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `org`
--
ALTER TABLE `org`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_categories`
--
ALTER TABLE `service_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_contact`
--
ALTER TABLE `service_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `systems`
--
ALTER TABLE `systems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_exceptions`
--
ALTER TABLE `system_exceptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_operations`
--
ALTER TABLE `system_operations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_operation_access`
--
ALTER TABLE `system_operation_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_routingaccess`
--
ALTER TABLE `system_routingaccess`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenant`
--
ALTER TABLE `tenant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=287;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact_address`
--
ALTER TABLE `contact_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact_email`
--
ALTER TABLE `contact_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact_phone`
--
ALTER TABLE `contact_phone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inspections`
--
ALTER TABLE `inspections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `inspection_first_page_fields`
--
ALTER TABLE `inspection_first_page_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `inspection_limitations`
--
ALTER TABLE `inspection_limitations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inspection_main_category`
--
ALTER TABLE `inspection_main_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `inspection_media`
--
ALTER TABLE `inspection_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT for table `inspection_records`
--
ALTER TABLE `inspection_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inspection_rows`
--
ALTER TABLE `inspection_rows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `inspection_schedule`
--
ALTER TABLE `inspection_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `inspection_sections`
--
ALTER TABLE `inspection_sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `inspection_sectors`
--
ALTER TABLE `inspection_sectors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `inspection_services`
--
ALTER TABLE `inspection_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inspection_sub_category`
--
ALTER TABLE `inspection_sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `org`
--
ALTER TABLE `org`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=501;
--
-- AUTO_INCREMENT for table `service_categories`
--
ALTER TABLE `service_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `service_contact`
--
ALTER TABLE `service_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `systems`
--
ALTER TABLE `systems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `system_exceptions`
--
ALTER TABLE `system_exceptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `system_operations`
--
ALTER TABLE `system_operations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `system_operation_access`
--
ALTER TABLE `system_operation_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `system_routingaccess`
--
ALTER TABLE `system_routingaccess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `tenant`
--
ALTER TABLE `tenant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
