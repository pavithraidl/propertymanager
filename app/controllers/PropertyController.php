<?php
use Carbon\Carbon;

class PropertyController extends BaseController
{
    /************************| PropertiesController |***********************
     *
     * CONFIDENTIAL
     * __________________
     *
     *  [2014] - [2024]
     *  All Rights Reserved.
     *
     * Developed by - Pavithra Isuru
     * Created on - 30/10/16 15:42.
     */

    public function getProperty() {
        return View::make('backend.property.property');
    }


    //region Get Data
    public function getPropertyList() {
        try {
            $user = (array) null;
            $arr = [];

            $userList = User::where('status', '!=', 0)->where('orgid', Auth::user()->orgid)->lists('id');
            foreach ($userList as $userId) {
                $name = sprintf('%s %s', User::where('id', $userId)->pluck('fname'), User::where('id', $userId)->pluck('lname'));
                $user[] = array(
                    'id' => $userId,
                    'name' => $name
                );
            }
            $arr[] = $user;

            $shortDate = new CalculationController();

            $propertyList = Property::where('status', '!=', 0)->where('orgid', Auth::user()->orgid)->lists('id');
            if(sizeof($propertyList) > 0) {
                foreach ($propertyList as $propertyId) {
                    $addressId = Property::where('id', $propertyId)->pluck('addressid');
                    $street = ContactAddress::where('id', $addressId)->pluck('street');
                    $adLine1 = ContactAddress::where('id', $addressId)->pluck('adline1');
                    $city = ContactAddress::where('id', $addressId)->pluck('adline2');
                    $postalCode = ContactAddress::where('id', $addressId)->pluck('adline2');
                    $address = sprintf('%s %s %s %s', $street, $adLine1, $city, $postalCode);
                    $displayId = Property::where('id', $propertyId)->pluck('displayid');

                    $managerId = Property::where('id', $propertyId)->pluck('propertymanager');
                    $ownerContactId = Property::where('id', $propertyId)->pluck('ownercontactid');
                    $ownerName = null;

                    try {
                        $owner = Contact::where('id', $ownerContactId)->first();
                        $ownerName = $owner->fname.' '.$owner->lname;
                    }catch (Exception $no) {
                        $ownerName = '<span style="color: #b9b9b9;">N/A</span>';
                    }


                    $reference = Property::where('id', $propertyId)->pluck('reference');
                    $frequency = Property::where('id', $propertyId)->pluck('frequency');
                    $alarm = Property::where('id', $propertyId)->pluck('alarm');
                    $key = Property::where('id', $propertyId)->pluck('keylocation');
                    $managerName = sprintf('%s %s', User::where('id', $managerId)->pluck('fname'), User::where('id', $managerId)->pluck('lname'));

                    if($managerName == null || $managerName == ' ') {
                        $managerName = '<span style="color: #b9b9b9;">N/A</span>';
                    }

                    $dueDate = null;


                    $lastSchedule = InspectionSchedule::where('propertyid', $propertyId)->where('status', '!=', 0)->orderBy('created_at', 'desc')->first();

                    if ($lastSchedule) {
                        $scheduleId = $lastSchedule->id;
                        $scheduleDate = $lastSchedule->scheduledatetime;
                        $scheduleInspectorId = $lastSchedule -> inspector;
                        $inspectorName = User::where('id', $scheduleInspectorId)->pluck('fname').' '. User::where('id', $scheduleInspectorId)->pluck('fname');
                        $scheduleDate = new DateTime($scheduleDate);
                        $scheduleDate = $scheduleDate->format('D j M Y, g:i a');
                        //has a last schedule
                        if ($lastSchedule->status > 1) {
                            //Last one already has done
                            if ($frequency > 0) {
                                //Repeating Inspection
                                if ($lastSchedule->reschedule == 1) {
                                    $lastInspectionDate = $lastSchedule->started_at;
                                } else {
                                    $lastInspectionDate = $lastSchedule->scheduledatetime;
                                }

                                //generate the new schedule date
                                $due = null;
                                $lastInspectionDate = Carbon::parse($lastInspectionDate);
                                if ($frequency == "1") $due = $lastInspectionDate->addWeek();
                                else if ($frequency == "2") $due = $lastInspectionDate->addWeeks(2);
                                else if ($frequency == "3") $due = $lastInspectionDate->addWeeks(3);
                                else if ($frequency == "4") $due = $lastInspectionDate->addMonth();
                                else if ($frequency == "5") $due = $lastInspectionDate->addMonths(2);
                                else if ($frequency == "6") $due = $lastInspectionDate->addMonths(3);
                                else if ($frequency == "7") $due = $lastInspectionDate->addMonths(4);
                                else if ($frequency == "8") $due = $lastInspectionDate->addMonths(6);
                                else if ($frequency == "9") $due = $lastInspectionDate->addMonths(9);
                                else if ($frequency == "10") $due = $lastInspectionDate->addYear();

                                $schedule = new InspectionSchedule();
                                $schedule->propertyid = $propertyId;
                                $schedule->scheduledatetime = $due;
                                $schedule->status = 1;
                                $schedule->created_at = Carbon::now('UTC');
                                $schedule->save();

                                $dueDate = array(
                                    'status' => 'Due',
                                    'date' => $due->format('D j M Y, g:i a')
                                );
                            } else {
                                //Not Repeating Inspection, Already has done.
                                $dueDate = array(
                                    'status' => 'Completed',
                                    'date' => $scheduleDate
                                );
                            }
                        } else {
                            //Last one not done yet
                            $dueDate = array(
                                'status' => 'Due',
                                'date' => $scheduleDate
                            );
                        }
                    } else {
                        //no any schedule
                    }

                    $arr[] = array(
                        'id' => $propertyId,
                        'displayid' => $displayId,
                        'adline1' => $adLine1,
                        'address' => $address,
                        'managerid' => $managerId,
                        'managername' => $managerName,
                        'ownername' => $ownerName,
                        'reference' => $reference,
                        'inspectorname' => $inspectorName,
                        'due' => $dueDate,
                        'frequency' => $frequency,
                        'alarm' => $alarm,
                        'key' => $key
                    );
                }
            }

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'getPropertyList', $ex);
            return $ex;
        }
    }

    public function getPropertyDetails() {
        $propertyId = Input::get('propertyid');

        try {
            $property = Property::where('id', $propertyId)->first();

            //Owner
            $ownerId = $property->ownercontactid;
            $ownerName = Contact::where('id', $ownerId)->pluck('fname').' '.Contact::where('id', $ownerId)->pluck('lname');
            $ownerAvatar = Contact::where('id', $ownerId)->pluck('avatarid');

            //Address
            $addressId = $property->addressid;
            $address = ContactAddress::where('id', $addressId)->first();
            $street = $address->street;
            $suburb = $address->adline1;
            $city = $address->city;
            $postalCode = $address->postalcode;

            //reference
            $reference = $property->reference;

            //property manager
            $managerId = $property->propertymanager;
            $userList = User::where('status', '!=', 0)->where('orgid', Auth::user()->orgid)->lists('id');
            foreach ($userList as $userId) {
                $name = sprintf('%s %s', User::where('id', $userId)->pluck('fname'), User::where('id', $userId)->pluck('lname'));
                $user[] = array(
                    'id' => $userId,
                    'name' => $name
                );
            }

            //tenants
            $tenantList = PropertyTenant::where('propertyid', $propertyId)->where('status', 1)->lists('tenantid');
            $tenantArr = array();
            foreach ($tenantList as $tenant) {
                $tenantArr[] = array(
                    'id' => $tenant,
                    'name' => Contact::where('id', $tenant)->pluck('fname').' '.Contact::where('id', $tenant)->pluck('lname'),
                    'avatar' => Contact::where('id', $tenant)->pluck('avatarid')
                );
            }

            $inspectionList = InspectionSchedule::where('propertyid', $propertyId)->where('status', 1)->lists('id');
            $inspection = array();
            foreach ($inspectionList as $inspectionId) {
                $currentInspection = InspectionSchedule::where('id', $inspectionId)->first();
                $timeAgo = new CalculationController();

                if(new DateTime($currentInspection->scheduledatetime) < new DateTime(date('Y-m-d H:i:s')))
                    $due = ($timeAgo->timeAgo((new DateTime($currentInspection->scheduledatetime))->format('Y-m-d H:i:s')));
                else
                    $due = ($timeAgo->shortDate((new DateTime($currentInspection->scheduledatetime))->format('Y-m-d H:i:s')));

                $endAt = $currentInspection->end_at;

                if($endAt != null || $endAt != '')
                    $endAt = ($timeAgo->timeAgo((new DateTime($endAt))->format('Y-m-d H:i:s')));
                else
                    $endAt = 'Pending';

                $scheduleTime = Carbon::parse($currentInspection->scheduledatetime);
                $toDay = Carbon::parse(date("m/d/Y"));
                if($scheduleTime < $toDay && $endAt == 'Pending') {
                    $endAt = "Over Due!";
                    $overDue = 1;
                }
                else {
                    $overDue = 0;
                }

                $inspector = $currentInspection->inspector;

                $inspection[] = array(
                    'id' => $inspectionId,
                    'inspectionid' => str_pad($inspectionId, 6, '0', STR_PAD_LEFT),
                    'due' => $due,
                    'done' => $endAt,
                    'overdue' => $overDue,
                    'inspector' => $inspector
                );
            }

            $ret = array(
                'ownerid' => $ownerId,
                'ownername' => $ownerName,
                'owneravatar' => $ownerAvatar,
                'addressid' => $addressId,
                'street' => $street,
                'suburb' => $suburb,
                'city' => $city,
                'postalcode' => $postalCode,
                'reference' => $reference,
                'managerid' => $managerId,
                'userlist' => $user,
                'tenants' => $tenantArr,
                'inspection' => $inspection
            );

            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'getPropertyDetails', $ex);
            return $ex;
        }
    }

    public function getUserList() {
        try {
            $userList = User::where('status', 1)->where('active', 1)->where('orgid', Auth::user()->orgid)->lists('id');
            if(sizeof($userList) < 1) $ret = null;

            foreach ($userList as $userId) {
                $ret[] = array(
                    'id' => $userId,
                    'fname' => User::where('id', $userId)->pluck('fname'),
                    'lname' => User::where('id', $userId)->pluck('lname'),
                    'jobtitle' => User::where('id', $userId)->pluck('jobtitle')
                );
            }

            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'getPropertyManagers', $ex);
            return 0;
        }
    }

    public function getSingleInspection() {
        $inspectionId = Input::get('inspectionid');

        try {
            $inspectionSchedule = InspectionSchedule::where('id', $inspectionId)->first();
            $date = $inspectionSchedule->scheduledatetime;
            $timeAgo = new CalculationController();
            $date = ($timeAgo->shortDate((new DateTime($date))->format('Y-m-d H:i:s')));
            $time = (new DateTime($inspectionSchedule->scheduledatetime))->format('g:i A');
            $createdById = $inspectionSchedule->created_by;
            $createdBy = User::where('id', $createdById)->pluck('fname').' '.User::where('id', $createdById)->pluck('lname');
            $createdAt = $timeAgo->timeAgo((new DateTime($inspectionSchedule->created_at))->format('Y-m-d H:i:s'));
            $inspectorId = $inspectionSchedule->inspector;

            $userList = json_decode($this->getUserList());

            $property = Property::where('id', $inspectionSchedule->propertyid)->first();
            $frequency = $property->frequency;
            $type = $inspectionSchedule->type;

            $status = Inspection::where('id', $inspectionSchedule->inspectionid)->pluck('status');

            $arr = array(
                'date' => $date,
                'propertyid' => $property->id,
                'time' => $time,
                'inspector' => $inspectorId,
                'frequency' => $frequency,
                'type' => $type,
                'userlist' => $userList,
                'status' => $status,
                'createdby' => $createdBy,
                'createdat' => $createdAt
            );

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'getSingleInspection', $ex);
            return 0;
        }

    }

    public function getActivityList() {
        $paginate = Input::get('paginate');
        $propertyId = Input::get('propertyid');
        $arr = [];

        try {
            $activityList = DB::table('activity_log')->where('propertyid', $propertyId)->orderBy('created_at', 'desc')->forPage($paginate, 50)->lists('id');

            if(sizeof($activityList) > 0){
                //creating objects
                $timeScence = new CalculationController();

                foreach ($activityList as $activityId) {
                    //get values form database
                    $user = DB::table('activity_log')->where('id', $activityId)->pluck('userid');
                    $userName = User::where('id', $user)->pluck('fname'). ' ' . User::where('id', $user)->pluck('lname');

                    $activity = DB::table('activity_log')->where('id', $activityId)->pluck('activity');
                    $activity = '<span style="color: #b3b3b3; font-style: italic;">'.$userName . ':</span> '. $activity;
                    $createdAt = DB::table('activity_log')->where('id', $activityId)->pluck('created_at');

                    //calculations
                    $setCreatedAt = new DateTime($createdAt, new DateTimeZone('UTC'));
                    $setCreatedAt = $setCreatedAt->setTimezone(new DateTimeZone('Pacific/Auckland'));
                    $setCreatedAt = $setCreatedAt->format('Y-m-d H:i:s');
                    $setCreatedAt = $timeScence->timeAgo($setCreatedAt);

                    //return json
                    $arr[] = array(
                        'activity' => $activity,
                        'timeago' => $setCreatedAt,
                        'createdat' => $createdAt
                    );
                }
                return json_encode($arr);
            }
            else{
                return -1;
            }
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertyController', 'getActivityList', $ex);
            return 0;
        }
    }
    //endregion

    //region Add Data
    public function addNewProperty() {
        $ownerId = Input::get('ownerid');
        $street = Input::get('street');
        $suburb = Input::get('suburb');
        $city = Input::get('city');
        $postalCode = Input::get('postalcode');
        $reference = Input::get('reference');
        $propertyManager = Input::get('propertymanager');
        $tenants = Input::get('tenants');
        $insDate = Input::get('insdate');
        $insTime = Input::get('instime');
        $insFrequency = Input::get('insfrequency');
        $insType = Input::get('instype');
        $inspector = Input::get('inspector');
        $alarm = Input::get('alarm');
        $key = Input::get('key');

        try {
            if($insDate != null || $insDate != '') {
                $dateTime = sprintf('%s %s', $insDate, $insTime);
                $dateTime = DateTime::createFromFormat('d F, Y g:i A', $dateTime);
                $dateTime = $dateTime->format('Y-m-d H:i:s');
            }
            else {
                $dateTime = null;
            }


            //add property contact contact details
            $address = new ContactAddress();
            $address -> street = $street;
            $address -> adline1 = $suburb;
            $address -> city = $city;
            $address -> postalcode = $postalCode;
            $address -> country = 'New Zealand';
            $address -> status = 1;
            $address -> created_at = Carbon::now('UTC');
            $address -> save();
            $addressId = $address -> id;

            //add new property
            //region generate display id
            $displayId = Property::where('orgid', Auth::user()->orgid)->max('displayid');
            if($displayId == null || $displayId == '') {
                $displayId = 0;
            }
            //endregion

            $property = new Property();
            $property -> displayid = ++$displayId;
            $property -> ownercontactid = $ownerId;
            $property -> propertymanager = $propertyManager;
            $property -> addressid = $addressId;
            $property -> frequency = $insFrequency;
            $property -> insdate = $dateTime;
            $property -> reference = $reference;
            $property -> alarm = $alarm;
            $property -> keylocation = $key;
            $property -> orgid = Auth::user()->orgid;
            $property -> status = 1;
            $property -> created_at = Carbon::now('UTC');
            $property -> save();
            $propertyId = $property -> id;

            //Add tenants
            if(is_array($tenants)) {
                foreach ($tenants as $tenant) {
                    $propertyTenant = new PropertyTenant();
                    $propertyTenant -> propertyid = $propertyId;
                    $propertyTenant -> tenantid = $tenant;
                    $propertyTenant -> status = 1;
                    $propertyTenant -> created_at = Carbon::now('UTC');
                    $propertyTenant -> save();
                }
            }

            //add inspection schedule
            if($dateTime != null) {
                $schedule = new InspectionSchedule();
                $schedule -> propertyid = $propertyId;
                $schedule -> scheduledatetime = $dateTime;
                $schedule -> inspector = $inspector;
                $schedule -> type = $insType;
                $schedule -> status = 1;
                $schedule -> created_by = Auth::user()->id;
                $schedule -> created_at = Carbon::now('UTC');
                $schedule -> save();
                $scheduleId = $schedule->id;

                if($inspector != null || $inspector != '') {
                    $sendEmail = new EmailController();
                    $sendEmail ->updateNewInspection($scheduleId, $inspector, null);
                }
            }

            $activity = new UserController();
            $activity -> saveActivity('Created the property', $propertyId, null);

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'addProperty', $ex);
            return 0;
        }
    }
    //endregion

    //region Update Data
    public function setInspector() {
        $inspectorId = Input::get('inspectorid');
        $propertyId = Input::get('propertyid');

        try {
            $inspectionId = InspectionSchedule::where('propertyid', $propertyId)->where('status', 1)->where('inspector', null)->pluck('id');
            InspectionSchedule::where('id', $inspectionId)->update(array(
                'inspector' => $inspectorId
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'setInspector', $ex);
            return 0;
        }
    }

    public function updateProperty() {
        $id = Input::get('id');
        $adLine1 = Input::get('adline1');
        $adLine2 = Input::get('adline2');
        $reference = Input::get('reference');
        $date = Input::get('date');
        $time = Input::get('time');
        $frequency = Input::get('frequency');
        $alarm = Input::get('alarm');
        $keyLocation = Input::get('keylocation');

        try {
            $lastId = Property::max('id');
            $newId = substr($lastId, 0, -5);
            $newId = sprintf('%d%d',++$newId,$id);

            $dateTime = sprintf('%s %s', $date, $time);
            $dateTime = strtotime($dateTime);
            $dateTime = date('Y-m-d H:i:s',$dateTime);

            Property::where('id', $id)->update(array(
                'adline1' => $adLine1,
                'adline2' => $adLine2,
                'reference' => $reference,
                'insdate' => $dateTime,
                'frequency' => $frequency,
                'alarm' => $alarm,
                'keylocation' => $keyLocation
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'updateProperty', $ex);
            return 0;
        }
    }

    public function updateInspectionDate() {
        $table = Input::get('table');
        $where = Input::get('where');
        $field = Input::get('field');
        $value = Input::get('value');
        $activityLog = Input::get('activity');
        $id = Input::get('id');

        try {
            $propertyId = InspectionSchedule::where('id', $id)->pluck('propertyid');
            $insTime = InspectionSchedule::where('id', $id)->pluck('scheduledatetime');
            $insTime = (new DateTime($insTime))->format('g:i A');

            if($insTime == null || $insTime == '') {
                $insTime = '00:00:00';
            }

            if($value != null || $value != '') {
                $dateTime = sprintf('%s %s', $value, $insTime);
                $dateTime = DateTime::createFromFormat('d F, Y g:i A', $dateTime);
                $dateTime = $dateTime->format('Y-m-d H:i:s');
            }

            $oldValue = DB::table($table)->where($where, $id)->pluck($field);
            $execute = DB::table($table)->where($where, $id)->update(array(
                    $field => $dateTime
                )
            );

            if($execute) {
                $activity = new UserController();
                $activity -> saveActivity('Updated '.$activityLog.' from '.$oldValue.' to '.$value, $propertyId, $id);
            }

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'updateInspectionDate', $ex);
            return 0;
        }
    }

    public function updateInspectionTime() {
        $table = Input::get('table');
        $where = Input::get('where');
        $field = Input::get('field');
        $value = Input::get('value');
        $activityLog = Input::get('activity');
        $id = Input::get('id');

        try {
            $propertyId = InspectionSchedule::where('id', $id)->pluck('propertyid');
            $insTimeDate = InspectionSchedule::where('id', $id)->pluck('scheduledatetime');
            $insDate = (new DateTime($insTimeDate))->format('Y-m-d');

            if($insDate == null || $insDate == '') {
                $insDate = new DateTime(date('Y-m-d'));
            }

            if($value != null || $value != '') {
                $dateTime = sprintf('%s %s', $insDate, $value);
                $dateTime = DateTime::createFromFormat('Y-m-d g:i A', $dateTime);
                $dateTime = $dateTime->format('Y-m-d H:i:s');
            }

            $oldValue = DB::table($table)->where($where, $id)->pluck($field);
            $execute = DB::table($table)->where($where, $id)->update(array(
                    $field => $dateTime
                )
            );

            if($execute) {
                $activity = new UserController();
                $activity -> saveActivity('Updated '.$activityLog.' from '.$oldValue.' to '.$value, $propertyId, $id);
            }

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'updateInspectionTime', $ex);
            return $ex;
        }
    }

    public function updateInspectionInspector() {
        $table = Input::get('table');
        $where = Input::get('where');
        $field = Input::get('field');
        $value = Input::get('value');
        $activityLog = Input::get('activity');
        $id = Input::get('id');

        try {
            $propertyId = InspectionSchedule::where('id', $id)->pluck('propertyid');
            $oldValue = DB::table($table)->where($where, $id)->pluck($field);
            $execute = DB::table($table)->where($where, $id)->update(array(
                    $field => $value
                )
            );

            if($execute) {
                $activity = new UserController();
                $activity -> saveActivity('Updated '.$activityLog.' from '.$oldValue.' to '.$value,  $propertyId, $id);

                $notification = new NotificationController();
                $notification -> saveNotification('You have been assigned to a new property inspection', $value, URL::Route('inspections'));

                //Need to implyment sending email to both inspectors about the update

                if($oldValue != null && $oldValue != '') {
                    $notification -> saveNotification('A scheduled inspection is removed from your inspection list', $value, URL::Route('inspections'));
                }
            }

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'updateInspectionInspector', $ex);
            return 0;
        }
    }

    public function UpdatePropertyOwner() {
        //Need to send old owner to the history table
        $newOwner = Input::get('owner');
        $propertyId = Input::get('propertyid');

        try {

            //Need to work on this part

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'UpdatePropertyOwner', $ex);
            return 0;
        }
    }

    public function updateAddress() {
        try {

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'updateAddress', $ex);
            return 0;
        }
    }

    public function updateReference() {
        try {

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'updateReference', $ex);
            return 0;
        }
    }

    public function updatePropertyManager() {
        //old property manager need to send to the history table
        try {

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'updatePropertyManager', $ex);
            return 0;
        }
    }

    public function updateTenant() {
        try {

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'updateTenant', $ex);
            return 0;
        }
    }
    //endregion

    //region Remove Data
    public function removePropertyOwner() {
        $propertyId = Input::get('propertyid');

        try {
            $ownerContactId = Property::where('id', $propertyId)->pluck('ownercontactid');
            $ownerName = Contact::where('id', $ownerContactId)->pluck('fname').' '.Contact::where('id', $ownerContactId)->pluck('lname');

            Property::where('id', $propertyId)->update(array(
                'ownercontactid' => null
            ));

            //activity log
            $reference = Property::where('id', $propertyId)->pluck('reference');
            $activity = new UserController();
            $activity -> saveActivity('Removed '.$ownerName.' from the property owner list', $propertyId, null);

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'removeProperty', $ex);
            return 0;
        }
    }

    public function removeProperty() {
        $propertyId = Input::get('propertyid');

        try {
            Property::where('id', $propertyId)->update(array(
                'status' => 0
            ));

            //activity log
            $reference = Property::where('id', $propertyId)->pluck('reference');
            $activity = new UserController();
            $activity -> saveActivity('Removed a property (Removed property reference: '.$reference.')', $propertyId, null);

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('PropertiesController', 'removeProperty', $ex);
            return 0;
        }
    }
    //endregion
}