<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 18/12/16
 * Time: 1:01 PM
 */

class ErrorController extends BaseController
{
    public function getException() {
        return View::make('backend.exception');
    }

    public function saveException($controller, $func, $ex) {
        try {
            $exception = new SystemException();
            $exception -> controller = $controller;
            $exception -> func = $func;
            $exception -> exception = $ex;
            $exception -> user = null;
            $exception -> status = 2;
            $exception -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $exception -> save();

            return 1;
        } catch (Exception $ex) {
            Log::error($ex);

            return 0;
        }
    }


}