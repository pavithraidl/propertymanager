<?php
/*************************************************************************
 * 
 * IDL CONFIDENTIAL
 * __________________
 * 
 *  [20014] - [2024] IDL Creations Incorporated 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru D. Liyanage
 * Authorized by - IDL Creaitons Incorporated
 * Created on - 1/13/2015.
 */

class imageProcessingController extends BaseController
{

    public function setImageSizes($src, $quality, $width, $height){
        try{
            $img = imagecreatefromjpeg($src);

            //get this values from user by submitting form ( either by crop or by textboxes)
            list($old_width, $old_height, $type, $attr) = getimagesize($src);

            $dest = ImageCreateTrueColor($width, $height);
            imagecopyresampled(
                $dest, //destination image
                $img, //source image
                0, //top left coordinate of the destination image in x direction
                0, //top left coordinate of the destination image in y direction
                0, //top left coordinate in x direction of source image that I want copying to start at
                0, //top left coordinate in y direction of source image that I want copying to start at
                $width, //190, thumbnail width
                $height, //190, thumbnail height
                $old_width, //how wide the rectangle from the source image we want thumbnailed is
                $old_height //how high the rectangle from the source image we want thumbnailed is
            );
            imagejpeg($dest, $src, $quality);
            return 1;
        }catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('imageProcessingController', 'BaseController', $ex);
            return $ex;
        }
    }
}