<?php

class InspectionController extends BaseController
{

    /************************| InspcetionsController |***********************
     *
     * CONFIDENTIAL
     * __________________
     *
     *  [2014] - [2024]
     *  All Rights Reserved.
     *
     * Developed by - Pavithra Isuru
     * Created on - 30/10/16 15:43.
     */


    public function getInspection() {
        return View::make("backend.inspection.inspection");
    }

    public function getNew($scheduleId) {
        return View::make('backend.inspection.new', array('scheduleId' => $scheduleId));
    }

    public function addInspection() {
        $assetId = Input::get('assetid');
        $date = Input::get('date');
        $time = Input::get('time');
        $frequency = Input::get('frequency');
        $type = Input::get('type');

        try {
            Asset::where('id', $assetId)->update(array(
                'date' => $date,
                'time' => $time,
                'frequency' => $frequency,
                'type' => $type
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionController', 'addInspection', $ex);
            return 0;
        }
    }

    public function saveStatus() {
        $step = Input::get('step');
        $inspectionId = Input::get('inspectionid');
        $limitationData = Input::get('limitationdata');

        try {
            if($inspectionId != null && $inspectionId != '') {

                $status = Inspection::where('id', $inspectionId)->pluck('status');
                if($status <= $step) {
                    Inspection::where('id', $inspectionId)->update(array(
                        'status' => ++$step
                    ));
                }
            }

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'saveStatus', $ex);
            return 0;
        }
    }

    public function saveLimitations() {
        $limitations = Input::get('limitations');
        $inspectionId = Input::get('inspectionid');

        try {
            Inspection::where('id', $inspectionId)->update(array(
                'limitation' => $limitations
            ));

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'saveLimitations', $ex);
            return 0;
        }
    }

    public function getScheduleInspection() {
        $paginate = Input::get('paginate');

        try {
            $dueList = InspectionSchedule::where('status', 1)->orderBy('scheduledatetime', 'asc')->forPage($paginate, 50)->lists('id');

            foreach ($dueList as $scheduleId) {
                $propertyId = InspectionSchedule::where('id', $scheduleId)->pluck('propertyid');
                $propertyStatus = Property::where('id', $propertyId)->pluck('status');

                if($propertyStatus == 1) {
                    $addressId = Property::where('id', $propertyId)->pluck('addressid');
                    $street = ContactAddress::where('id', $addressId)->pluck('street');
                    $adLine1 = ContactAddress::where('id', $addressId)->pluck('adline1');
                    $address = sprintf('%s %s', $street, $adLine1);
                    $reference = Property::where('id', $propertyId)->pluck('reference');

                    $schedule = InspectionSchedule::where('id', $scheduleId)->pluck('scheduledatetime');
                    $inspectorId = InspectionSchedule::where('id', $scheduleId)->pluck('inspector');
                    $inspectorName = User::where('id', $inspectorId)->pluck('fname').' '.User::where('id', $inspectorId)->pluck('lname');

                    $arr[] = array(
                        'id' => $scheduleId,
                        'address' => $address,
                        'reference' => $reference,
                        'schedule' => $schedule,
                        'inspector' => $inspectorName
                    );
                }
            }

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'getScheduleInspection', $ex);
            return 0;
        }
    }

    //region New Inspection Section
    public function saveBasic() {
        $inspectionScheduleId = Input::get('inspectionscheduleid');
        $inspectionId = Input::get('inspectionid');
        $residential = Input::get('residential');
        $age = Input::get('age');
        $type = Input::get('type');
        $bedrooms = Input::get('bedrooms');
        $bathrooms = Input::get('bathrooms');
        $whether = Input::get('whether');
        $present = Input::get('present');
        $other = Input::get('other');
        $step = Input::get('step');
        $residential == 1 ? $residential = 'Yes' : $residential = 'No';

        try {
            if($inspectionId != null || $inspectionId != '' || $inspectionId != 0) {
                $inspection = Inspection::where('id', $inspectionId)->firstOrFail();
                $inspection -> scheduleid = $inspectionScheduleId;
                $inspection -> orgid = Auth::user() -> orgid;
                $inspection -> inspector = Auth::user() -> id;
                $inspection -> residential = $residential;
                $inspection -> age = $age;
                $inspection -> type = $type;
                $inspection -> bedrooms = $bedrooms;
                $inspection -> bathrooms = $bathrooms;
                $inspection -> whether = $whether;
                $inspection -> present_people = $present;
                $inspection -> other = $other;
//                $inspection -> status = ++$step;
                $inspection -> created_at = \Carbon\Carbon::now('UTC');
                $inspection -> save();
                $inspectionId = $inspection -> id;

                return json_encode($inspectionId);
            }
            else {
                $inspection = new Inspection();
                $inspection -> scheduleid = $inspectionScheduleId;
                $inspection -> orgid = Auth::user() -> orgid;
                $inspection -> inspector = Auth::user() -> id;
                $inspection -> residential = $residential;
                $inspection -> age = $age;
                $inspection -> type = $type;
                $inspection -> bedrooms = $bedrooms;
                $inspection -> bathrooms = $bathrooms;
                $inspection -> whether = $whether;
                $inspection -> present_people = $present;
                $inspection -> other = $other;
                $inspection -> created_at = \Carbon\Carbon::now('UTC');
                $inspection -> save();
                $inspectionId = $inspection -> id;

                InspectionSchedule::where('id', $inspectionScheduleId)->update(array(
                    'inspectionid' => $inspectionId,
                    'started_at' => \Carbon\Carbon::now('UTC')
                ));

//              Save user activity
                $propertyId = InspectionSchedule::where('id', $inspectionScheduleId)->pluck('propertyId');
                $assetId = InspectionSchedule::where('id', $inspectionScheduleId)->pluck('assetid');
                $activity = new UserController();
                $activity -> saveActivity('Started a new inspection in property - '.$assetId, $propertyId, $inspectionId);

                return json_encode($inspectionId);
            }
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'saveBasic', $ex);
            return 0;
        }
    }

    public function saveTopic() {
        $inspectionId = Input::get('inspectionid');
        $topic = Input::get('topic');
        $mainId = Input::get('mainid');
        $catId = Input::get('catid');
        $secId = Input::get('secid');

        try {
            $rowId = InspectionRow::where('inspectionid', $inspectionId)->where('mainid', $mainId)->where('subid', $catId)->where('sectorid', $secId)->pluck('id');
            if(!$rowId) {
                $row = new InspectionRow();
                $row -> inspectionid = $inspectionId;
                $row -> mainid = $mainId;
                $row -> subid = $catId;
                $row -> sectorid = $secId;
                $row -> topic = $topic;
                $row -> status = 1;
                $row -> created_at = \Carbon\Carbon::now('UTC');
                $row -> save();
                $rowId = $row -> id;

                return json_encode($rowId);
            }
            else {
                InspectionRow::where('id', $rowId)->update(array('topic' => $topic));

                return 1;
            }
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'saveTopic', $ex);
            return 0;
        }
    }

    public function saveDescription() {
        $inspectionId = Input::get('inspectionid');
        $description = Input::get('description');
        $mainId = Input::get('mainid');
        $catId = Input::get('catid');
        $secId = Input::get('secid');

        try {
            $rowId = InspectionRow::where('inspectionid', $inspectionId)->where('mainid', $mainId)->where('subid', $catId)->where('sectorid', $secId)->pluck('id');
            if(!$rowId) {
                $row = new InspectionRow();
                $row -> inspectionid = $inspectionId;
                $row -> mainid = $mainId;
                $row -> subid = $catId;
                $row -> sectorid = $secId;
                $row -> description = $description;
                $row -> status = 1;
                $row -> created_at = \Carbon\Carbon::now('UTC');
                $row -> save();
            }
            else {
                InspectionRow::where('id', $rowId)->update(array('description' => $description));

                return 1;
            }
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'saveDescription', $ex);
            return 0;
        }
    }

    public function savePoints() {
        $inspectionId = Input::get('inspectionid');
        $points = Input::get('points');
        $mainId = Input::get('mainid');
        $catId = Input::get('catid');
        $secId = Input::get('secid');

        try {
            $rowId = InspectionRow::where('inspectionid', $inspectionId)->where('mainid', $mainId)->where('subid', $catId)->where('sectorid', $secId)->pluck('id');
            if(!$rowId) {
                $row = new InspectionRow();
                $row -> inspectionid = $inspectionId;
                $row -> mainid = $mainId;
                $row -> subid = $catId;
                $row -> sectorid = $secId;
                $row -> points = $points;
                $row -> status = 1;
                $row -> created_at = \Carbon\Carbon::now('UTC');
                $row -> save();
                $rowId = $row -> id;

                return json_encode($rowId);
            }
            else {
                InspectionRow::where('id', $rowId)->update(array('general' => $points));
                return 1;
            }
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'savePoints', $ex);
            return 0;
        }
    }

    public function uploadImage() {
        $inspectionId = Input::get('inspectionid');
        $mainId = Input::get('mainid');
        $catId = Input::get('catid');
        $secId = Input::get('secid');
        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $fileSize = 0;
        $fileType = 0;
        $ext = strtolower($imageFileType);


        try {
            //get the directory name from the database
            $dirPath = "assets/images/inspection/".$inspectionId."/";
            //create the directory named by user id
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777);
            }

            if($ext == "jpg" || $ext == "jpeg" || $ext == 'png') {
                $fileType = 1;
            }
            else if($ext == 'mp4' || $ext == 'avi' || $ext == 'flv' || $ext == 'wmv' || $ext == 'mov') {
                $fileType = 2;
            }
            else {
                $fileType = 0;
            }

            if($fileType == 0){
                return json_encode("-1");
            }
            else{
                $rowId = InspectionRow::where('inspectionid', $inspectionId)->where('mainid', $mainId)->where('subid', $catId)->where('sectorid', $secId)->pluck('id');
                if(!$rowId) {
                    $row = new InspectionRow();
                    $row -> inspectionid = $inspectionId;
                    $row -> mainid = $mainId;
                    $row -> subid = $catId;
                    $row -> sectorid = $secId;
                    $row -> status = 1;
                    $row -> created_at = \Carbon\Carbon::now('UTC');
                    $row -> save();
                    $rowId = $row -> id;
                }

                $media = new InspectionMedia();
                $media -> inspectionid = $inspectionId;
                $media -> rowid = $rowId;
                $media -> type = $fileType;
                $media -> ext = $ext;
                $media -> caption = null;
                $media -> status = 1;
                $media -> created_at = \Carbon\Carbon::now('UTC');
                $media -> save();
                $mediaId = $media -> id;


                $name = sprintf("%s.%s", $mediaId, $ext);
                $ret = sprintf('%s%s', $dirPath, $name);

                Input::file('file')->move($dirPath,$name);

                if($fileType == 1) {
                    $imageResize = new imageProcessingController();
                    $resizeResult = $imageResize->setImageSizes($ret,80, 1280, 720);
                }

                $ret = array(
                    'id' => $mediaId,
                    'src' => $ret,
                    'type' => $fileType
                );

                return $ret;
            }
        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'uploadImage', $ex);
            return 0;
        }
    }

    public function removeMedia() {
        $mediaId = Input::get('mediaid');

        try {
            InspectionMedia::where('id', $mediaId)->update(array(
                'status' => 0
            ));

            return 1;

        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'removeMedia', $ex);
            return 0;
        }
    }

    public function getInspectionSector() {
        $mainId = Input::get('mainid');
        $catId = Input::get('catid');
        $secId = Input::get('secid');
        $inspectionId = Input::get('inspectionid');

        try {
            if(InspectionRow::where('mainid', $mainId)->where('subid', $catId)->where('sectorid', $secId)->where('inspectionid', $inspectionId)->pluck('id')) {
                $sector = InspectionRow::where('mainid', $mainId)->where('subid', $catId)->where('sectorid', $secId)->where('inspectionid', $inspectionId)->firstOrFail();
                $rowId = $sector->id;
                $mediaList = InspectionMedia::where('rowid', $rowId)->where('status', 1)->lists('id');

                $media = null;

                foreach ($mediaList as $mediaId) {
                    $media[] = array(
                        'id' => $mediaId,
                        'type' => InspectionMedia::where('id', $mediaId)->pluck('type'),
                        'ext' => InspectionMedia::where('id', $mediaId)->pluck('ext'),
                        'caption' => InspectionMedia::where('id', $mediaId)->pluck('caption'),
                        'status' => InspectionMedia::where('id', $mediaId)->pluck('status')
                    );
                }

                $arr = array(
                    'topic' => $sector -> topic,
                    'description' => $sector -> description,
                    'general' => $sector -> general,
                    'media' => $media
                );

                return json_encode($arr);
            }
            else {
                $arr = array(
                    'topic' => '',
                    'description' => '',
                    'general' => ''
                );

                return json_encode($arr);
            }


        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'getSector', $ex);
            return 0;
        }
    }

    public function addCoverImage() {
        $inspectionId = Input::get('inspectionid');
        $imageId = Input::get('imageid');

        try {
            Inspection::where('id', $inspectionId)->update(array(
                'coverimage' => $imageId
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('InspectionsController', 'addCoverImage', $ex);
            return 0;
        }
    }
    //endregion
}