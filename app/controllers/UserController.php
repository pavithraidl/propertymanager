<?php

class UserController extends BaseController
{

    /************************| Users |***********************
     *
     * CONFIDENTIAL
     * __________________
     *
     *  [2014] - [2024]
     *  All Rights Reserved.
     *
     * Developed by - Pavithra Isuru
     * Created on - 30/10/16 15:46.
     */


    public function getUsers() {
        $access = new AccessController();
        if($access -> getSecureRoute(5)) {
            return View::make('backend.user.users');
        }
        else {
            return View::make('backend.error.404');
        }
    }

    public function getLogin() {
        return View::make('backend.auth.login');
    }

    public function postLogin() {
        try{
            //validate the login details
            $validator = Validator::make(Input::all(), array(
                'email' => 'required|email',
                'password' => 'required'
            ));

            if ($validator->fails()) {
                // Redirect to the sign in page with errors
                return Redirect::route('admin-get-login')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                //remember user
                $remember = (Input::has('chk-remember')) ? true : false;

                // Sign in user to the dash board
                $email = Input::get('email');
                $password = Input::get('password');
                $previous = Input::get('previous');

                $auth = Auth::attempt(array(
                    'email' => $email,
                    'password' => $password,
                    'active' => 1
                ), $remember);

                if ($auth) {
                    if(User::where('email', $email)->pluck('active') == 0){
                        return Redirect::route('login')
                            ->with('global', 'This account is not Activated yet. Check your emails to active!');
                    }
                    else{
                        $this->saveActivity('Login to the system', null, null);
                        return Redirect::intended('dashboard');
                    }
                }
                else {
                    $this->saveActivity('Trying to login with wrong user details or deactivated account', null, null);
                    return Redirect::route('get-login')
                        ->with('global', 'Email/password wrong, or account not activated!');
                }
            }
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UserController', 'postLogin', $ex);
            return $ex;
        }
    }

    public function addUser() {
        $name = Input::get('name');
        $email = Input::get('email');
        $jobTitle = Input::get('jobtitle');


        try {
            if(User::where('email', $email)->where('status', '!=', 0)->pluck('id') && $email != '') {
                return -1;
            }
            else {

                $words = explode( ' ', $name);
                array_shift( $words);
                $lName = implode( ' ', $words);
                $name = explode(" ", $name);
                $fName = $name[0];

                $roll = 4;
                $orgId = Auth::user()->orgid;
                $orgName = 'ABC Company';
                $code = str_random(60);

                $user = new User();
                $user -> fname = ucfirst($fName);
                $user -> lname = ucfirst($lName);
                $user -> email = $email;
                $user -> code = $code;
                $user -> active = 0;
                $user -> jobtitle = $jobTitle;
                $user -> roll = $roll;
                $user -> orgid = $orgId;
                $user -> status = 3;
                $user -> created_at = \Carbon\Carbon::now('UTC');
                $user -> save();

                if($email != '') {
                    Mail::send( 'emails.active', array(
                        'link'      => URL::route( 'active', $code ),
                        'orgName' => $orgName,
                        'name' => $fName.' '.$lName,
                        'homeurl' => URL::To('/')
                    ), function ( $message ) use ( $user, $email, $fName) {
                        $message->to( $email, $fName )->subject( 'Activate your account' );
                    });
                }

                return 1;
            }
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UserController', 'addUser', $ex);
            return 0;
        }
    }

    public function getActivate($code) {
        try {
            if (User::where('code', '=', $code)->where('active', '=', 0)->pluck('id')) {
                $user = User::where('code', $code)->first();
                $fName = $user->fname;
                $lName = $user->lname;

                return View::make('backend.auth.activate', array(
                    'code' => $code,
                    'email' => $user->email,
                    'fName' => $fName,
                    'lName' => $lName
                ));
            } else {
                return View::make('backend.invalid-activation');
            }
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UserController', 'getActivate', $ex);
        }
    }

    public function postActive() {
        $password = Input::get('password');
        $email = Input::get('email');
        $fName = Input::get('fname');
        $lName = Input::get('lname');
        $code = Input::get('code');

        $userId = User::where('code', '=', $code)->where('active', 0)->where('email', $email)->pluck('id');

        if($userId) {
            User::where('id', $userId)->update(array(
                'fname' => $fName,
                'lname' => $lName,
                'active' => 1,
                'code' => '',
                'status' => 1,
                'password' => Hash::make($password)
            ));

            return 1;
        }
        else {
            return -1;
        }
    }

    public function getLogOut()
    {
        Auth::logout();
        $this->saveActivity('Logout from the system', null, null);
        return Redirect::route('get-login');
    }

    public function saveActivity($activity, $propertyId, $inspectionId) {
        try {
            if (!isset($propertyId)) $propertyId = null;
            if (!isset($inspectionId)) $inspectionId = null;

            $record = new ActivityLog();
            $record -> userid = Auth::user()->id;
            $record -> orgid = Auth::user()->orgid;
            $record -> propertyid = $propertyId;
            $record -> inspectionid = $inspectionId;
            $record -> activity = $activity;
            $record -> created_at = \Carbon\Carbon::now('UTC');
            $record -> save();

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UserController', 'saveActivity', $ex);
        }
    }

    public function getUserList() {
        try {
            $orgId = Auth::user()->orgid;
            $ownUserId = Auth::user()->id;

            $userList = User::where('status', '!=', 0)->where('orgid', $orgId)->where('roll', '>=', Auth::user()->roll)->lists('id');

            $key = array_search($ownUserId, $userList);
            unset($userList[$key]);
            array_unshift($userList, $ownUserId);

            foreach ($userList as $userId) {
                $fName = User::where('id', $userId)->pluck('fname');
                $lName = User::where('id', $userId)->pluck('lname');
                $jobTitle = User::where('id', $userId)->pluck('jobtitle');
                $status = User::where('id', $userId)->pluck('status');

                $name = $fName.' '.$lName;

                $profilePic = URL::To('/').'/assets/images/users/'.$userId.'/user-35.jpg';
                try {
                    getimagesize($profilePic);
                }
                catch(Exception $ex) {
                    $profilePic = URL::To('/').'/assets/images/users/default/user-35.jpg';
                }

                $arr[] = array(
                    'id' => $userId,
                    'name' => $name,
                    'jobtitle' => $jobTitle,
                    'status' => $status,
                    'avatar' => $profilePic
                );
            }

            return json_encode($arr);
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UserController', 'getUserList', $ex);
        }
    }

    public function getUserBasicDetails() {
        $userId = Input::get('userid');

        try {
            $name = User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname');
            $email = User::where('id', $userId)->pluck('email');
            $jobTitle = User::where('id', $userId)->pluck('jobtitle');
            $status = User::where('id', $userId)->pluck('status');

            $profilePic = URL::To('/').'/assets/images/users/'.$userId.'/user-100.jpg';
            try {
                getimagesize($profilePic);
            }
            catch(Exception $ex) {
                $profilePic = URL::To('/').'/assets/images/users/default/user-100.jpg';
            }

            $arr = array(
                'id' => $userId,
                'name' => $name,
                'email' => $email,
                'status' => $status,
                'jobtitle' => $jobTitle,
                'avatar' => $profilePic
            );

            return json_encode($arr);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UsersController', 'getUserBasicDetails', $ex);
            return 0;
        }
    }

    public function postUserStatus() {
        $userId = Input::get('userid');
        $status = Input::get('status');

        try {
            if($status == 0) $status = 2;
            User::where('id', $userId)->update(array(
                'status' => $status
            ));

            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $this -> saveActivity($status.' user - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').'('.User::where('id', $userId)->pluck('email').') User account', null, null);

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UsersController', 'postUserStatus', $ex);
            return 0;
        }
    }

    public function postRemoveUserAccount() {
        $userId = Input::get('userid');

        try {
            User::where('id', $userId)->update(array(
                'status' => 0,
                'active' => 0,
                'email' => ''
            ));

            $this -> saveActivity('Deleted - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').' User account', null, null);

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UsersController', 'postRemoveUserAccount', $ex);
            return 0;
        }
    }

    public function postResendActivationEmail() {
        $userId = Input::get('userid');

        try {
            $code = User::where('id', $userId)->pluck('code');
            $fName = User::where('id', $userId)->pluck('fname');
            $lName = User::where('id', $userId)->pluck('lname');
            $email = User::where('id', $userId)->pluck('email');
            $orgName = Org::where('id', Auth::user()->orgid)->pluck('name');
            $name = $fName.' '.$lName;

            Mail::send( 'emails.active', array(
                'link'      => URL::route( 'active', $code ),
                'orgName' => $orgName,
                'name' => $fName.' '.$lName,
                'homeurl' => URL::To('/')
            ), function ( $message ) use ( $email, $fName) {
                $message->to( $email, $fName )->subject( 'Activate your account' );
            });

            $this->saveActivity('Resent the activation email to '.$name.'('.$email.')', null, null);

            return json_encode($email);
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UsersController', 'postResendActivationEmail', $ex);
            return 0;
        }
    }

    public function getActivityList() {
        $paginate = Input::get('paginate');
        $userId = Input::get('userid');
        $arr = [];

        try {
            $activityList = DB::table('activity_log')->where('userid', $userId)->orderBy('created_at', 'desc')->forPage($paginate, 50)->lists('id');

            if(sizeof($activityList) > 0){
                //creating objects
                $timeScence = new CalculationController();

                foreach ($activityList as $activityId) {
                    //get values form database
                    $activity = DB::table('activity_log')->where('id', $activityId)->pluck('activity');
                    $createdAt = DB::table('activity_log')->where('id', $activityId)->pluck('created_at');

                    //calculations
                    $setCreatedAt = new DateTime($createdAt, new DateTimeZone('UTC'));
                    $setCreatedAt = $setCreatedAt->setTimezone(new DateTimeZone('Pacific/Auckland'));
                    $setCreatedAt = $setCreatedAt->format('Y-m-d H:i:s');
                    $setCreatedAt = $timeScence->timeAgo($setCreatedAt);

                    //return json
                    $arr[] = array(
                        'activity' => $activity,
                        'timeago' => $setCreatedAt,
                        'createdat' => $createdAt
                    );
                }
                return json_encode($arr);
            }
            else{
                return -1;
            }
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UsersController', 'getActivityList', $ex);
            return 0;
        }
    }

    public function changeUserAccess() {
        $userId = Input::get('selecteduser');
        $status = Input::get('status');
        $systemId = Input::get('systemid');

        try {
            if(SystemRoutingAccess::where('systemid', $systemId)->where('userid', $userId)->pluck('id')) {
                SystemRoutingAccess::where('systemid', $systemId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemRoutingAccess();
                $systemAccess -> systemid = $systemId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $this -> saveActivity($status.' user - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').'('.User::where('id', $userId)->pluck('email').') access to '.System::where('id', $systemId)->pluck('name'), null, null);

            return 1;

        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UsersController', 'changeUserAccess', $ex);
            return 0;
        }
    }

    public function changeUserOperationAccess() {
        $userId = Input::get('selecteduser');
        $status = Input::get('status');
        $operationId = Input::get('operationid');

        try {
            if(SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->pluck('id')) {
                SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemOperationAccess();
                $systemAccess -> operationid = $operationId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $systemId = SystemOperation::where('id', $operationId)->pluck('systemid');
            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $this -> saveActivity($status.' user - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').'('.User::where('id', $userId)->pluck('email').') access in the '.System::where('id', $systemId)->pluck('name').' Page to '.SystemOperation::where('id', $operationId)->pluck('operation'), null, null);

            return 1;

        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UsersController', 'changeUserAccess', $ex);
            return 0;
        }
    }

    public function getAccessList() {
        $userId = Input::get('userid');

        try {
            $systemList = SystemRoutingAccess::where('userid', $userId)->where('allow', 1)->lists('systemid');
            $operationList = SystemOperationAccess::where('userid', $userId)->where('allow', 1)->lists('operationid');

            $ret = array(
                'sys' => $systemList,
                'ope' => $operationList
            );

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('UsersController', 'getAccessList', $ex);
            return 0;
        }
    }
}