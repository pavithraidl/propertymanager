<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {23/05/17} {11:50}.
 */
 
class SystemController extends BaseController {

    public function getManage() {
        return View::make('backend.system.manage');
    }

    public function changeStatus() {
        $systemId = Input::get('systemid');
        $status = Input::get('status');

        try {
            System::where('id', $systemId)->update(array(
                'status' => $status
            ));

            $name = Auth::user()->fname. ' ' . Auth::user()->lname;
            $systemName = System::where('id', $systemId)->pluck('name');

            $activity = $name. ' is ';

            if($status == 1) $activity = $activity. 'activated'.' '.$systemName. ' system.';
            else $activity = $activity. 'deactivated'.' '.$systemName. ' system.';

            $activityLog = new UserController();
            $activityLog ->saveActivity($activity, null, null);

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('SystemController', 'changeStatus', $ex);
            return 0;
        }
    }
}