<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {7/05/17} {20:39}.
 */
 
class EmailController extends BaseController {

    public function updateNewInspection($scheduleId, $inspector, $preInspector) {

        try {
            $propertyId = InspectionSchedule::where('id', $scheduleId)->pluck('propertyid');
            $addressId = Property::where('id', $propertyId)->pluck('addressid');
            $street = ContactAddress::where('id', $addressId)->pluck('street');
            $adLine1 = ContactAddress::where('id', $addressId)->pluck('adline1');
            $city = ContactAddress::where('id', $addressId)->pluck('adline2');
            $postalCode = ContactAddress::where('id', $addressId)->pluck('adline2');
            $address = sprintf('%s %s %s %s', $street, $adLine1, $city, $postalCode);
            $dueDate = InspectionSchedule::where('id', $scheduleId)->pluck('scheduledatetime');
            $dueDate = date_create($dueDate);
            $dueDate = date_format($dueDate, "F j, Y, g:i a");

            $sideHeding = $address;
            $topic = Property::where('id', $propertyId)->pluck('reference'). '\'s Inspection - '.str_pad($scheduleId, 5, '0', STR_PAD_LEFT);
            $content = 'You have been assigned a new inspection. <br/><br/>Inspection ID : '.str_pad($scheduleId, 5, '0', STR_PAD_LEFT).'<br/>At: '.$dueDate.'<br/><br/>Click the bellow button to open the page.';

            $email = User::where('id', $inspector)->pluck('email');
            $fName = User::where('id', $inspector)->pluck('fname');

            $link = URL::To('/').'inspection/new/'.$scheduleId;

            Mail::send( 'emails.update', array(
                'sideHeading' => $sideHeding,
                'topic' => $topic,
                'content' => $content,
                'link' => $link,
            ), function ( $message ) use ( $email, $fName) {
                $message->to( $email, $fName )->subject( 'A New Inspection Assigned' );
            });

            if($preInspector != null && $preInspector != '') {

                $topic = Property::where('id', $propertyId)->pluck('reference'). '\'s Inspection Removed - '.str_pad($scheduleId, 5, '0', STR_PAD_LEFT);
                $content = 'The inspection you have been assigned under id '.str_pad($scheduleId, 5, '0', STR_PAD_LEFT).' is <strong>removed</strong> from your inspection list. <br/><br/>Inspection ID : '.str_pad($scheduleId, 5, '0', STR_PAD_LEFT).'<br/>At: '.$dueDate.'<br/><br/>Click the bellow button to open the page.';

                $email = User::where('id', $preInspector)->pluck('email');
                $fName = User::where('id', $preInspector)->pluck('fname');

                $link = null;

                Mail::send( 'emails.update', array(
                    'sideHeading' => $sideHeding,
                    'topic' => $topic,
                    'content' => $content,
                    'link' => $link,
                ), function ( $message ) use ( $email, $fName) {
                    $message->to( $email, $fName )->subject( 'A New Inspection Assigned' );
                });
            }

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('EmailController', 'updateNewInspection', $ex);
            return $ex;
        }
    }
}