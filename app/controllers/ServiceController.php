<?php

class ServiceController extends BaseController
{

    /************************| ScheduleController |***********************
     *
     * CONFIDENTIAL
     * __________________
     *
     *  [2014] - [2024]
     *  All Rights Reserved.
     *
     * Developed by - Pavithra Isuru
     * Created on - 30/10/16 15:44.
     */


    public function getServices() {
        return View::make('backend.services');
    }
}