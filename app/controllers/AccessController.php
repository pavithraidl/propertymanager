<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {15/02/17} {10:24}.
 */
 
class AccessController extends BaseController {

    public function getSecureRoute($systemId) {
        if(SystemRoutingAccess::where('systemid', $systemId)->where('userid', Auth::User()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll == 1) {
            return true;
        }
        else {
            return false;
        }
    }
    public function getOperationAccessChecker(){
        $opCode = Input::get('opcode');

        try{
            if(DB::table('operation_access')->where('opcode', $opCode)->where('userid', Auth::user()->id)->where('status', 1)->pluck('id')){
                return 1;
            }
            else{
                return 0;
            }
        }catch (Exception $ex) {
            $errorController = new ErrorController();
            $errorController->CatchError(1, $ex);
            return 0;
        }

    }

    public function operationAccessChecker($opCode){
        try{
            if(Auth::user()->rollid < 4) {
                return 1;
            }
            else if(DB::table('operation_access')->where('opcode', $opCode)->where('userid', Auth::user()->id)->where('allow', 1)->where('status', 1)->pluck('id')){
                return 1;
            }
            else{
                return 0;
            }
        }catch (Exception $ex) {
            $errorController = new ErrorController();
            $errorController->CatchError(2, $ex);
            return 0;
        }
    }

    public function operationLengthChecker($opLeCode) {
        $opLe = DB::table('operation_length_user')->where('userid', Auth::user()->id)->where('oplecode', $opLeCode)->where('allow', 1)->pluck('length');
        if($opLe == null) {
            return -1;
        }
        else {
            if($opLe == 'd') {
                $timeFilter = date("Y/m/d");
            }
            else if($opLe == 'w') {
                $timeFilter = Carbon::now()->subweeks(1);
            }
            else if($opLe == 'm') {
                $timeFilter = Carbon::now()->submonths(1);
            }
            else {
                $timeFilter = Carbon::now()->subyears(1);
            }

            return $timeFilter;
        }
    }

    public function unwantedAccess($pageName, $pageUrl){
        try{
            DB::table('system_unwanted_access')->insert(
                array(
                    'pagename' => $pageName,
                    'pageurl' => $pageUrl,
                    'userid' => Auth::user()->id,
                    'created_at' => (new DateTime("now", new DateTimeZone('Asia/Colombo')))->format('Y-m-d H:i:s')
                )
            );
        }catch (Exception $ex) {
            $errorController = new ErrorController();
            $errorController->CatchError(3, $ex);
            return 0;
        }
    }

    public function viewAccessCheck($pageId){
        $blockIp = new UserVisitController();
        if ($blockIp->getUserBlockIp()) {
            if(DB::table('system_routingaccess')->where('pageid', $pageId)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') &&
                DB::table('systems')->where('id', DB::table('system_pages')->where('id', $pageId)->pluck('systemid'))->where('visibility', 1)->pluck('id')){
                $userVisit = new UserVisitController();
                $userVisit->setUserVisit($pageId);
                return 1;
            } else {
                if (DB::table('systems')->where('id', DB::table('system_pages')->where('id', $pageId)->pluck('systemid'))->where('visibility', 2)->pluck('id')) {
                    return 2;
                } else {
                    return -1;
                }
            }
        } else {
            return -9;
        }
    }

    //Authenticate the url access (function id - 145)
    public function urlAccessAthenticator() {
        try{
            if(DB::table('users')->where('id', Auth::user()->id)->pluck('active') != 1 || DB::table('users')->where('id',
                    Auth::user()->id)->pluck('status') != 1 || DB::table('org')->where('id', Auth::user()->orgid)->pluck('status') == 0) {
                Auth::logout();
            }
        } catch(Exception $ex) {
            Auth::logout();
        }

    }
}