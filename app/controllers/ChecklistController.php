<?php

class ChecklistController extends BaseController
{

    /************************| ChecklistController |***********************
     *
     * CONFIDENTIAL
     * __________________
     *
     *  [2014] - [2024]
     *  All Rights Reserved.
     *
     * Developed by - Pavithra Isuru
     * Created on - 30/10/16 15:47.
     */


    public function getChecklist() {
        return View::make('backend.checklist');
    }
}