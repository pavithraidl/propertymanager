<?php

class DashboardController extends BaseController
{

    /************************| Dashboard Controller |***********************
     *
     * CONFIDENTIAL
     * __________________
     *
     *  [2014] - [2024]
     *  All Rights Reserved.
     *
     * Developed by - Pavithra Isuru
     * Created on - 30/10/16 10:37.
     */

    public function getDashboard() {
        return View::make('backend.default');
    }
}