<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/


	public function getHome() {
        return View::make('frontend.default');
    }

    public function registerClient() {
        $fName = Input::get('fname');
        $lName = Input::get('lname');
        $company = Input::get('company');
        $email = Input::get('email');
        $password = Input::get('password');

        try {
            if(User::where('email', $email)->where('status', '!=', 0)->pluck('id') && $email != '') {
                return -1;
            }
            else {
                $organization = new Org();
                $organization -> name = $company;
                $organization -> url = null;
                $organization -> status = 1;
                $organization -> created_at = \Carbon\Carbon::now();
                $organization -> save();
                $orgId = $organization -> id;

                $roll = 3;
                $code = str_random(60);

                $user = new User();
                $user -> fname = ucfirst($fName);
                $user -> lname = ucfirst($lName);
                $user -> email = $email;
                $user -> code = $code;
                $user -> active = 0;
                $user -> jobtitle = 'Admin';
                $user -> roll = $roll;
                $user -> orgid = $orgId;
                $user -> status = 3;
                $user -> created_at = \Carbon\Carbon::now('UTC');
                $user -> save();
                $userId = $user -> id;

                Org::where('id', $orgId)->update(array(
                    'ownerid' => $userId
                ));

                if($user) {
                    Mail::send( 'emails.welcome', array(
                        'link'      => URL::route( 'active', $code ),
                        'orgName' => $organization -> name,
                        'name' => $fName.' '.$lName,
                        'homeurl' => URL::To('/')
                    ), function ( $message ) use ( $user, $email, $fName) {
                        $message->to( $email, $fName )->subject( 'Activate your account' );
                    });
                }

                return 1;
            }
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('HomeController', 'registerClient', $ex);
            return 0;
        }
    }

	public function oneFieldEditor() {
        $table = Input::get('table');
        $field = Input::get('field');
        $id = Input::get('id');
        $value = Input::get('value');

        try {
            DB::table($table)->where('id', $id)->update(array($field => $value));

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('HomeController', 'oneFieldEditor', $ex);
            return 0;
        }
    }

    public function oneFieldWithWhereEditor() {
        $table = Input::get('table');
        $field = Input::get('field');
        $id = Input::get('id');
        $value = Input::get('value');
        $wherefield = Input::get('wherefield');
        $whereval = Input::get('whereval');

        try {
            DB::table($table)->where('id', $id)->where($wherefield, $whereval)->update(array($field => $value));

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('HomeController', 'oneFieldWithWhereEditor', $ex);
            return 0;
        }
    }

}
