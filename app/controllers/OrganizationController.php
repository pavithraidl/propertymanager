<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {10/02/17} {23:39}.
 */
 
class OrganizationController extends BaseController {

    public function getOrganizations() {
        return View::make('backend.organization');
    }

    public function getOrgList() {
        try {
            $ownOrgId = Auth::user()->orgid;

            $orgList = Org::where('status', '!=', 0)->lists('id');

            $key = array_search($ownOrgId, $orgList);
            unset($orgList[$key]);
            array_unshift($orgList, $ownOrgId);

            foreach ($orgList as $orgId) {
                $name = Org::where('id', $orgId)->pluck('name');
                $status = Org::where('id', $orgId)->pluck('status');

                $profilePic = URL::To('/').'/assets/images/org/logo/'.$orgId.'/logo-40.png';
                try {
                    getimagesize($profilePic);
                }
                catch(Exception $ex) {
                    $profilePic = URL::To('/').'/assets/images/org/logo/default/logo-40.png';
                }

                $arr[] = array(
                    'id' => $orgId,
                    'name' => $name,
                    'status' => $status,
                    'avatar' => $profilePic
                );
            }

            return json_encode($arr);
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('OrganizationController', 'getOrgList', $ex);
        }
    }

    public function getOrgBasicDetails() {
        $orgId = Input::get('orgid');

        try {
            $name = Org::where('id', $orgId)->pluck('name');
            $ownerId = Org::where('id', $orgId)->pluck('ownerid');
            $email = User::where('id', $ownerId)->pluck('email');
            $status = Org::where('id', $orgId)->pluck('status');
            $url = Org::where('id', $orgId)->pluck('url');

            $profilePic = URL::To('/').'/assets/images/org/logo/'.$orgId.'/logo-100.png';
            try {
                getimagesize($profilePic);
            }
            catch(Exception $ex) {
                $profilePic = URL::To('/').'/assets/images/org/logo/default/logo-100.png';
            }

            $arr = array(
                'id' => $orgId,
                'name' => $name,
                'email' => $email,
                'status' => $status,
                'avatar' => $profilePic,
                'url' => $url
            );

            return json_encode($arr);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('OrganizationController', 'getUserBasicDetails', $ex);
            return 0;
        }
    }

    public function getActivityList() {
        $paginate = Input::get('paginate');
        $orgId = Input::get('orgid');
        $arr = [];

        try {
            $activityList = DB::table('activity_log')->where('orgid', $orgId)->orderBy('created_at', 'desc')->forPage($paginate, 50)->lists('id');

            if(sizeof($activityList) > 0){
                //creating objects
                $timeScence = new CalculationController();

                foreach ($activityList as $activityId) {
                    //get values form database
                    $activity = DB::table('activity_log')->where('id', $activityId)->pluck('activity');
                    $user = User::where('id', ActivityLog::where('id', $activityId)->pluck('userid'))->pluck('fname').' '.
                        User::where('id', ActivityLog::where('id', $activityId)->pluck('userid'))->pluck('lname');
                    $createdAt = DB::table('activity_log')->where('id', $activityId)->pluck('created_at');

                    $activity = '<strong>'.$user.'-</strong> '.$activity;

                    //calculations
                    $setCreatedAt = new DateTime($createdAt, new DateTimeZone('UTC'));
                    $setCreatedAt = $setCreatedAt->setTimezone(new DateTimeZone('Pacific/Auckland'));
                    $setCreatedAt = $setCreatedAt->format('Y-m-d H:i:s');
                    $setCreatedAt = $timeScence->timeAgo($setCreatedAt);

                    //return json
                    $arr[] = array(
                        'activity' => $activity,
                        'timeago' => $setCreatedAt,
                        'createdat' => $createdAt
                    );
                }
                return json_encode($arr);
            }
            else{
                return -1;
            }
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('OrganizationController', 'getActivityList', $ex);
            return 0;
        }
    }

    public function getAccessList() {
        $orgId = Input::get('orgid');
        $ownerId = Org::where('id', $orgId)->pluck('ownerid');

        try {
            $systemList = SystemRoutingAccess::where('userid', $ownerId)->where('allow', 1)->lists('systemid');
            $operationList = SystemOperationAccess::where('userid', $ownerId)->where('allow', 1)->lists('operationid');

            $ret = array(
                'sys' => $systemList,
                'ope' => $operationList
            );

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('OrganizationController', 'getAccessList', $ex);
            return 0;
        }
    }

    public function postOrgStatus() {
        $orgId = Input::get('orgid');
        $status = Input::get('status');

        try {
            if($status == 0) $status = 2;
            Org::where('id', $orgId)->update(array(
                'status' => $status
            ));

            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $activity = new UserController();
            $activity -> saveActivity($status.' Organization - '.Org::where('id', $orgId)->pluck('name').'\'s Organization account', null, null);

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('OrganizationController', 'postOrgStatus', $ex);
            return 0;
        }
    }

    public function changeOrgAccess() {
        $orgId = Input::get('selectedorg');
        $status = Input::get('status');
        $systemId = Input::get('systemid');

        try {
            $userId = Org::where('id', $orgId)->pluck('ownerid');
            if(SystemRoutingAccess::where('systemid', $systemId)->where('userid', $userId)->pluck('id')) {
                SystemRoutingAccess::where('systemid', $systemId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemRoutingAccess();
                $systemAccess -> systemid = $systemId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $activity = new UserController();
            $activity -> saveActivity($status.' Organization - '.Org::where('id', $orgId)->pluck('name').'  access to '.System::where('id', $systemId)->pluck('name'), null, null);

            return 1;

        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('OrganizationController', 'changeOrgAccess', $ex);
            return 0;
        }
    }

    public function changeOrgOperationAccess() {
        $orgId = Input::get('selectedorg');
        $status = Input::get('status');
        $operationId = Input::get('operationid');

        try {
            $userId = Org::where('id', $orgId)->pluck('ownerid');
            if(SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->pluck('id')) {
                SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemOperationAccess();
                $systemAccess -> operationid = $operationId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $systemId = SystemOperation::where('id', $operationId)->pluck('systemid');
            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $activity = new UserController();
            $activity -> saveActivity($status.' Organization - '.Org::where('id', $userId)->pluck('name').' access in the '.System::where('id', $systemId)->pluck('name').' Page to '.SystemOperation::where('id', $operationId)->pluck('operation'), null, null);

            return 1;

        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('OrganizationController', 'changeOrgOperationAccess', $ex);
            return 0;
        }
    }

    public function removeOrgAccount() {
        $orgId = Input::get('orgid');

        try {
            Org::where('id', $orgId)->update(array(
                'status' => 0
            ));

            $activity = new UserController();
            $activity -> saveActivity('Deleted - '.Org::where('id', $orgId)->pluck('name').'\'s Organization account User account', null, null);

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException('OrganizationController', 'postRemoveOrgAccount', $ex);
            return 0;
        }
    }
}