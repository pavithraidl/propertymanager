<?php

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicPropertyFunctions()
	{
		$crawler = $this->client->request('GET', '/');

        $result = array(
            'crw' => $crawler
        );

		$this->assertTrue($this->client->getResponse()->isOk());

        $propertyController = new PropertyController();
        $ret = $propertyController -> getUserList();

        if(is_array($ret)) {
            $result += array(
                'getUserList' => 1
            );
        }
        else {
            $result += array(
                'getUserList' => 0
            );
        }

        $ret = $propertyController -> getPropertyList();

        if(is_array($ret) && sizeof($ret) < 0) {
            if(is_array($ret)) {
                $result += array(
                    'getPropertyList' => 1
                );
            }
            else {
                $result += array(
                    'getPropertyList' => 0
                );
            }
        }

        $ret = $propertyController -> getPropertyDetails();

        if(is_array($ret) && sizeof($ret) < 0) {
            if(is_array($ret)) {
                $result += array(
                    'getPropertyDetails' => 1
                );
            }
            else {
                $result += array(
                    'getPropertyDetails' => 0
                );
            }
        }

        return json_encode($result);

	}

}
