<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


//region -----| Frontend |-------
//----------------------------------------------------------------------------------------------------------------------
Route::get('/', array(
    'as' => 'home',
    'uses' => 'HomeController@getHome'
));

Route::post('/register-client', array(
    'as' => 'register',
    'uses' => 'HomeController@registerClient'
));
//endregion



//region -----| backend |--------
//----------------------------------------------------------------------------------------------------------------------
Route::group( array( 'before' => 'auth' ), function () {

    //region ** System URL Routs **
    /*========================================================================================================*/
    /*========================================================================================================*/
    $routeList = DB::table('system_url_routes')->where('status', 1)->lists('id');
    foreach($routeList as $id){
        if(SystemUrlRoutes::where('id', $id)->pluck('type') == 'get'){
            Route::get(SystemUrlRoutes::where('id', $id)->pluck('url'), array(
                'as' => SystemUrlRoutes::where('id', $id)->pluck('routeas'),
                'uses' => SystemUrlRoutes::where('id', $id)->pluck('uses')
            ));
        }
        else{
            Route::post(SystemUrlRoutes::where('id', $id)->pluck('url'), array(
                'as' => SystemUrlRoutes::where('id', $id)->pluck('routeas'),
                'uses' => SystemUrlRoutes::where('id', $id)->pluck('uses')
            ));
        }
    }
    /*-------------------------------------------------------------------------------------------------------*/
    //endregion



    //region Public function routes
    Route::post('/public/update-single-field', array(
        'as' => 'public:update-single-field',
        'uses' => 'PublicController@updateSingleField'
    ));
    //endregion

    Route::get('/system/manage', array(
        'as' => 'system',
        'uses' => 'SystemController@getManage'
    ));

    Route::post('/system/change-system-status', array(
        'as' => 'system:change-status',
        'uses' => 'SystemController@changeStatus'
    ));


    //region Property
    Route::get('/property', array(
        'as' => 'property',
        'uses' => 'PropertyController@getProperty'
    ));

    Route::get('/property/get-property-list', array(
        'as' => 'property:get-property-list',
        'uses' => 'PropertyController@getPropertyList'
    ));

    Route::post('property/add-property', array(
        'as' => 'property:add-property',
        'uses' => 'PropertyController@addProperty'
    ));

    Route::post('property/update-property', array(
        'as' => 'property:update-property',
        'uses' => 'PropertyController@updateProperty'
    ));

    Route::post('property/set-inspector', array(
        'as' => 'property:set-inspector',
        'uses' => 'PropertyController@setInspector'
    ));

    Route::get('property/get-property-details', array(
            'as' => 'property:get-property',
            'uses' => 'PropertyController@getPropertyDetails'
    ));

    Route::post('property/remove-property', array(
        'as' => 'property:remove-property',
        'uses' => 'PropertyController@removeProperty'
    ));

    Route::get('property/get-single-inspection', array(
        'as' => 'property:get-single-inspection',
        'uses' => 'PropertyController@getSingleInspection'
    ));

    Route::post('property/update-inspection-date', array(
        'as' => 'property:update-inspection-date',
        'uses' => 'PropertyController@updateInspectionDate'
    ));

    Route::post('property/update-inspection-time', array(
        'as' => 'property:update-inspection-time',
        'uses' => 'PropertyController@updateInspectionTime'
    ));

    Route::post('property/update-inspection-inspector', array(
        'as' => 'property:update-inspection-inspector',
        'uses' => 'PropertyController@updateInspectionInspector'
    ));

    Route::get('property/get-activity-list', array(
        'as' => 'property:get-activity-list',
        'uses' => 'PropertyController@getActivityList'
    ));

    Route::post('property/remove-property-owner', array(
        'as' => 'property:remove-property-owner',
        'uses' => 'PropertyController@removePropertyOwner'
    ));

    //region Add Property
        Route::get('property/get-user-list', array(
            'as' => 'property:get-user-list',
            'uses' => 'PropertyController@getUserList'
        ));

        Route::post('property/add-new-property', array(
            'as' => 'property:add-new-property',
            'uses' => 'PropertyController@addNewProperty'
        ));
    //endregion
    //endregion

    //region Contact
    Route::post('contact/upload-avatar', array(
        'as' => 'contact:upload-avatar',
        'uses' => 'ContactController@uploadAvatar'
    ));

    Route::post('contact/add-new-contact', array(
        'as' => 'contact:add-new-contact',
        'uses' => 'ContactController@addNewContact'
    ));
    //endregion

    //region Inspection
    Route::get('/inspections', array(
        'as' => 'inspections',
        'uses' => 'InspectionController@getInspection'
    ));

    Route::get('/inspection/get-schedule-list', array(
        'as' => 'inspection-get-schedule-list',
        'uses' => 'InspectionController@getScheduleInspection'
    ));

    Route::get('/inspection/add-inspection', array(
        'as' => 'inspection-add-inspection',
        'uses' => 'InspectionController@addInspection'
    ));

    Route::get('/inspection/new/{scheduleId}', array(
        'as' => 'inspections-new',
        'uses' => 'InspectionController@getNew'
    ));

    Route::post('/inspection/new/save-basic', array(
        'as' => 'inspection-new-save-basic',
        'uses' => 'InspectionController@saveBasic'
    ));

    Route::post('/inspection/new/save-topic', array(
        'as' => 'inspection-new-save-topic',
        'uses' => 'InspectionController@saveTopic'
    ));

    Route::post('/inspection/new/get-sector', array(
        'as' => 'inspection-new-get-sector',
        'uses' => 'InspectionController@getInspectionSector'
    ));

    Route::post('/inspection/new/save-description', array(
        'as' => 'inspection-new-save-description',
        'uses' => 'InspectionController@saveDescription'
    ));

    Route::post('/inspection/new/save-points', array(
        'as' => 'inspection-new-save-points',
        'uses' => 'InspectionController@savePoints'
    ));

    Route::post('/inspection/new/save-status', array(
        'as' => 'inspection-new-save-status',
        'uses' => 'InspectionController@saveStatus'
    ));

    Route::post('/inspection/new/save-limitations', array(
        'as' => 'inspection-new-save-limitations',
        'uses' => 'InspectionController@saveLimitations'
    ));

    Route::post('/inspection/new/image-upload', array(
        'as' => 'upload-image',
        'uses' => 'InspectionController@uploadImage'
    ));

    Route::post('/inspection/new/remove-media', array(
        'as' => 'inspection-new-remove-media',
        'uses' => 'InspectionController@removeMedia'
    ));

    Route::post('/inspection/new/add-cover-image', array(
        'as' => 'inspection-new-add-cover-image',
        'uses' => 'InspectionController@addCoverImage'
    ));

    //endregion

    Route::get('/services', array(
        'as' => 'services',
        'uses' => 'ServiceController@getServices'
    ));

    Route::get('/users', array(
        'as' => 'users',
        'uses' => 'UserController@getUsers'
    ));

    Route::post('/user/add-user', array(
        'as' => 'user-add-user',
        'uses' => 'UserController@addUser'
    ));

    Route::get('/user/get-user-list', array(
        'as' => 'user-get-user-list',
        'uses' => 'UserController@getUserList'
    ));

    Route::get('/user/get-user-basic-details', array(
        'as' => 'user-get-user-basic-details',
        'uses' => 'UserController@getUserBasicDetails'
    ));

    Route::post('/user/change-user-status', array(
        'as' => 'admin-user-change-user-status',
        'uses' => 'UserController@postUserStatus'
    ));

    Route::post('/user/remove-user-account', array(
        'as' => 'admin-user-remove-user-account',
        'uses' => 'UserController@postRemoveUserAccount'
    ));

    Route::post('/user/resend-activation-email', array(
        'as' => 'user-resend-activation-email',
        'uses' => 'UserController@postResendActivationEmail'
    ));

    Route::get('/user/get-activity-list', array(
        'as' => 'admin-user-get-activity-list',
        'uses' => 'UserController@getActivityList'
    ));

    Route::post('/user/change-system-access', array(
        'as' => 'admin-user-change-system-access',
        'uses' => 'UserController@changeUserAccess'
    ));

    Route::post('/user/change-operation-access', array(
        'as' => 'admin-user-change-operation-access',
        'uses' => 'UserController@changeUserOperationAccess'
    ));

    Route::get('/user/get-access-list', array(
        'as' => 'admin-user-get-access-list',
        'uses' => 'UserController@getAccessList'
    ));

    Route::get('/check-user-email-duplicate', array(
        'as' => 'admin-check-user-email-duplicate',
        'uses' => 'UserController@checkDuplicateEmail'
    ));

    Route::post('/users/save-settings', array(
        'as' => 'admin-user-save-settings',
        'uses' => 'UserController@saveSettings'
    ));

    Route::get('/checklist', array(
        'as' => 'checklist',
        'uses' => 'ChecklistController@getChecklist'
    ));

    Route::get('/organizations', array(
        'as' => 'organizations',
        'uses' => 'OrganizationController@getOrganizations'
    ));

    Route::get('/org/get-org-list', array(
        'as' => 'org-get-org-list',
        'uses' => 'OrganizationController@getOrgList'
    ));

    Route::get('/org/get-org-basic-details', array(
        'as' => 'org-get-org-basic-details',
        'uses' => 'OrganizationController@getOrgBasicDetails'
    ));

    Route::get('/org/get-activity-list', array(
        'as' => 'org-get-activity-list',
        'uses' => 'OrganizationController@getActivityList'
    ));

    Route::get('/org/get-access-list', array(
        'as' => 'org-get-activity-list',
        'uses' => 'OrganizationController@getAccesslist'
    ));

    Route::post('/org/change-org-status', array(
        'as' => 'org-get-access-list',
        'uses' => 'OrganizationController@postOrgStatus'
    ));

    Route::post('/org/change-system-access', array(
        'as' => 'org-change-system-access',
        'uses' => 'OrganizationController@changeOrgAccess'
    ));

    Route::post('/org/change-operation-access', array(
        'as' => 'org-change-operation-access',
        'uses' => 'OrganizationController@changeOrgOperationAccess'
    ));

    Route::post('/org/remove-org-account', array(
        'as' => 'org-remove-org-account',
        'uses' => 'OrganizationController@removeOrgAccount'
    ));

    Route::get('/settings', array(
        'as' => 'settings',
        'uses' => 'SettingsController@getSettings'
    ));

    Route::post('public/field-update', array(
        'as' => 'public-field-update',
        'uses' => 'HomeController@oneFieldEditor'
    ));

    Route::post('public/field-where-update', array(
        'as' => 'public-field-where-update',
        'uses' => 'HomeController@oneFieldWhereEditor'
    ));

    Route::get( '/logout', array(
        'as'   => 'log-out',
        'uses' => 'UserController@getLogOut'
    ));

    Route::get('/test-function', function () {
        ActivityLog::where('id', '!=', -1)->update(array('orgid' => 1));
        return 1;
    });
});




Route::group( array( 'before' => 'guest' ), function () {
    Route::get('/active/{code}', array(
        'as' => 'active',
        'uses' => 'UserController@getActivate'
    ));

    Route::post('active/active-account', array(
        'as' => 'post-active',
        'uses' => 'UserController@postActive'
    ));

    Route::get('login', array(
        'as' => 'get-login',
        'uses' => 'UserController@getLogin'
    ));

    Route::post('login', array(
        'as' => 'post-login',
        'uses' => 'UserController@postLogin'
    ));


});

//endregion
