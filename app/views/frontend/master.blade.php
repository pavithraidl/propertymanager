<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 14/02/17 17:08.
 */
?>

        <!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Easy Inspect" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css" />
{{HTML::style('assets/frontend/css/bootstrap.css')}}
{{HTML::style('/assets/frontend/style.css')}}

<!-- One Page Module Specific Stylesheet -->
{{HTML::style('assets/frontend/onepage.css')}}
<!-- / -->

    {{HTML::style('assets/frontend/css/dark.css')}}
    {{HTML::style('assets/frontend/css/font-icons.css')}}
    {{HTML::style('assets/frontend/css/et-line.css')}}
    {{HTML::style('assets/frontend/css/animate.css')}}
    {{HTML::style('assets/frontend/css/magnific-popup.css')}}

    {{HTML::style('assets/frontend/css/fonts.css')}}

    {{HTML::style('assets/frontend/css/responsive.css')}}
    <meta name="viewport" content="width=device-width, initial-scale=1" />


    <!-- Document Title
    ============================================= -->
    <title>One Page with Registration Form | Canvas</title>

</head>

<body class="stretched" data-loader="11" data-loader-color="#543456">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="full-header transparent-header border-full-header dark static-sticky" data-sticky-class="not-dark" data-sticky-offset="full" data-sticky-offset-negative="100">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- Logo
                ============================================= -->
                <div id="logo">
                    <a href="index.html" class="standard-logo" data-dark-logo="images/canvasone-dark.png"><img src="images/canvasone.png" alt="Inspect Logo"></a>
                    <a href="index.html" class="retina-logo" data-dark-logo="images/canvasone-dark@2x.png"><img src="images/canvasone@2x.png" alt="Inspect Logo"></a>
                </div><!-- #logo end -->

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu">

                    <ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="65">
                        <li><a href="#" data-href="#wrapper"><div>Home</div></a></li>
                        <li><a href="#" data-href="#section-about"><div>Buy</div></a></li>
                        <li><a href="#" data-href="#section-works"><div>Documentation</div></a></li>
                        <li><a href="#" data-href="#section-blog"><div>Blog</div></a></li>
                        <li><a href="#" data-href="#section-contact"><div>Contact</div></a></li>
                    </ul>

                </nav><!-- #primary-menu end -->

            </div>

        </div>

    </header><!-- #header end -->

    @yield('content');

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark noborder">

        {{--<div class="container center">--}}
        {{--<div class="footer-widgets-wrap">--}}

        {{--<div class="row divcenter clearfix">--}}

        {{--<div class="col-md-4">--}}

        {{--<div class="widget clearfix">--}}
        {{--<h4>Site Links</h4>--}}

        {{--<ul class="list-unstyled footer-site-links nobottommargin">--}}
        {{--<li><a href="#" data-scrollto="#wrapper" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Top</a></li>--}}
        {{--<li><a href="#" data-scrollto="#section-about" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">About</a></li>--}}
        {{--<li><a href="#" data-scrollto="#section-works" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Works</a></li>--}}
        {{--<li><a href="#" data-scrollto="#section-services" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Services</a></li>--}}
        {{--<li><a href="#" data-scrollto="#section-blog" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Blog</a></li>--}}
        {{--<li><a href="#" data-scrollto="#section-contact" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Contact</a></li>--}}
        {{--</ul>--}}
        {{--</div>--}}

        {{--</div>--}}

        {{--<div class="col-md-4">--}}

        {{--<div class="widget subscribe-widget clearfix" data-loader="button">--}}
        {{--<h4>Subscribe</h4>--}}

        {{--<div class="widget-subscribe-form-result"></div>--}}
        {{--<form id="widget-subscribe-form" action="../include/subscribe.php" role="form" method="post" class="nobottommargin">--}}
        {{--<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control input-lg not-dark required email" placeholder="Your Email Address">--}}
        {{--<button class="button button-border button-circle button-light topmargin-sm" type="submit">Subscribe Now</button>--}}
        {{--</form>--}}
        {{--</div>--}}

        {{--</div>--}}

        {{--<div class="col-md-4">--}}

        {{--<div class="widget clearfix">--}}
        {{--<h4>Contact</h4>--}}

        {{--<p class="lead">795 Folsom Ave, Suite 600<br>San Francisco, CA 94107</p>--}}

        {{--<div class="center topmargin-sm">--}}
        {{--<a href="#" class="social-icon inline-block noborder si-small si-facebook" title="Facebook">--}}
        {{--<i class="icon-facebook"></i>--}}
        {{--<i class="icon-facebook"></i>--}}
        {{--</a>--}}
        {{--<a href="#" class="social-icon inline-block noborder si-small si-twitter" title="Twitter">--}}
        {{--<i class="icon-twitter"></i>--}}
        {{--<i class="icon-twitter"></i>--}}
        {{--</a>--}}
        {{--<a href="#" class="social-icon inline-block noborder si-small si-github" title="Github">--}}
        {{--<i class="icon-github"></i>--}}
        {{--<i class="icon-github"></i>--}}
        {{--</a>--}}
        {{--<a href="#" class="social-icon inline-block noborder si-small si-pinterest" title="Pinterest">--}}
        {{--<i class="icon-pinterest"></i>--}}
        {{--<i class="icon-pinterest"></i>--}}
        {{--</a>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--</div>--}}

        {{--</div>--}}

        {{--</div>--}}
        {{--</div>--}}

        <div id="copyrights">
            <div class="container center clearfix">
                Copyright IDL Creations {{date('Y')}} | All Rights Reserved
            </div>
        </div>

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
{{HTML::script('assets/frontend/js/jquery.js')}}
{{HTML::script('assets/frontend/js/plugins.js')}}

<!-- Google Map JavaScripts
============================================= -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=YOUR_API_KEY"></script>
{{HTML::script('assets/frontend/js/jquery.gmap.js')}}

<!-- Footer Scripts
============================================= -->
{{HTML::script('assets/frontend/js/functions.js')}}

{{HTML::script('assets/js/custom/constant.js')}}
{{HTML::script('assets/js/custom/validator.js')}}
{{HTML::script('assets/js/custom/public.functions.js')}}

@yield('script')
</body>
</html>

