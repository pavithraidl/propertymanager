<?php
/************************| default.blade.php |***********************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/10/16 10:43.
 */
?>

@extends('backend.master')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
            <!-- top tiles -->
            <div class="row tile_count">
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-building"></i> Total Assets</span>
                    <div id="de-count-assets" class="count">2500</div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-photo"></i> Photos</span>
                    <div id="de-count-photos" class="count">2,500</div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-clipboard"></i> Completed Inspections</span>
                    <div id="de-count-inspections" class="count green">123.50</div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-video-camera"></i> Videos</span>
                    <div id="de-count-videos" class="count">4,567</div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-file-word-o"></i> Word Reports</span>
                    <div id="de-count-word" class="count">2,315</div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-file-pdf-o"></i> PDF Reports</span>
                    <div id="de-count-pdf" class="count">7,325</div>
                </div>
            </div>
            <!-- /top tiles -->
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Search...">
                            <span class="fa fa-search form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="df-schedule-time-filter" class="form-control" required>
                                <option value="0">Any Schedules</option>
                                <option value="1">Within 1 Day</option>
                                <option value="2">Within 3 Day</option>
                                <option value="3">Within 1 Week</option>
                                <option value="4">Within 3 Week</option>
                                <option value="5">Within 1 Month</option>
                            </select>
                        </div>
                    </div>
                    <div class="x_panel">
                        <div class="x_title bg-info">
                            <h2>Scheduled Inspection <small>Upcoming</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a><i class="fa fa-refresh"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Property Name</th>
                                    <th>Due Date</th>
                                    <th>Manage By</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>5 Hedley</td>
                                    <td>12 Nov</td>
                                    <td>Isuru Liyanage</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>23 Avendole</td>
                                    <td>14 Nov</td>
                                    <td>Jhon Doe</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>4B Larry</td>
                                    <td>14 Nov</td>
                                    <td>Rory</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Search...">
                            <span class="fa fa-search form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="df-schedule-time-filter" class="form-control" required>
                                <option value="0">Any Due Date</option>
                                <option value="1">Within 1 Day</option>
                                <option value="2">Within 3 Day</option>
                                <option value="3">Within 1 Week</option>
                                <option value="4">Within 3 Week</option>
                                <option value="5">Within 1 Month</option>
                            </select>
                        </div>

                    </div>

                    <div class="x_panel">
                        <div class="x_title bg-blue">
                            <h2>Due Inspection <small style="color: #d4d4d4;">Ongoing</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a><i class="fa fa-refresh"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Property Name</th>
                                    <th>Due Date</th>
                                    <th>Manage By</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>5 Hedley</td>
                                    <td>12 Nov</td>
                                    <td>Isuru Liyanage</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>23 Avendole</td>
                                    <td>14 Nov</td>
                                    <td>Jhon Doe</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>4B Larry</td>
                                    <td>14 Nov</td>
                                    <td>Rory</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

@endsection