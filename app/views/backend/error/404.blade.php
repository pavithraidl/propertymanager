<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 15/02/17 10:40.
 */
?>

@extends('backend.master')

@section('content')
    <div class="col-md-6 col-md-offset-4">
        <img src="{{URL::To('/')}}/assets/frontend/images/cliparts/404.png" style="position: absolute; right: -11%;"/>
        <h3 style="text-align: center; margin-top: 100px;">Oops...!</h3>
        <h1 style="text-align: center; font-size: 150px;">404</h1>
        <h4 style="text-align: center;">The page your are looking for cannot be found in the system.</h4>
    </div>
@endsection
