<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 23/03/17 22:53.
 */
?>

<div class="modal fade new-contact-modal" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 10000 !important;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="global-contact-modal-title">Add New Contact</h4>
            </div>
            <div id="contact-body" class="modal-body" style="height: 500px; !important; overflow-y: auto;">
                <p style="text-align: center;">
                    <img id="contact-avatar-img" src="{{URL::To('/')}}/assets/images/icons/vector.jpg" class="img-circle contact-image" style="width: 100px; height: 100px;"/>
                </p>
                <div id="contact-avatar-overlay" class="contact-image-overlay">
                    <img id="contact-avatar-overlay-img" src="{{URL::To('/')}}/assets/images/icons/camera.png" />
                    <span id="contact-img-overlay-loading-rate" style="display: none;">20%</span>
                </div>
                <div style="display: none;">
                    <form id="upload-contact-avatar" action="{{URL::Route('contact:upload-avatar')}}" method="post" enctype="multipart/form-data" class="contact-avatar-upload">
                        <input type="file" name="file" id="contact-avatar-choose" onchange="submitContactAvatarForm();" accept="image/jpeg;capture=camera" style="display: none;">
                    </form>
                </div>
                <form>
                    <div class="form-group">
                        <label class="control-label col-xs-12" for="as-reference">Name:</label>
                        <div class="col-xs-12">
                            <input type="text" id="contact-fname" maxlength="14" placeholder="First Name..." onblur="requiredValidator(this, 'Please enter the name')" class="form-control col-md-7 col-xs-12" tabindex="0">
                        </div>
                        <div class="col-xs-12" style="margin-top: 10px;">
                            <input type="text" id="contact-lname" maxlength="14" placeholder="Last Name..." class="form-control col-md-7 col-xs-12" tabindex="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-12 margin-top-sm" for="as-reference">Phone Number:</label>
                        <div class="col-xs-9">
                            <input type="text" id="inc-contact-phone-1" maxlength="14" placeholder="0222647713" class="form-control col-md-7 col-xs-12 phone-number" tabindex="0">
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <select id="inc-contact-phone-type-1" style="height: 33px;" tabindex="0">
                                <option value="Mobile">Mobile</option>
                                <option value="Home">Home</option>
                                <option value="Work">Work</option>
                            </select>
                        </div>
                    </div>
                    <div id="inc-contact-phone-container">
                        {{--Jquery append when click more PHONE--}}
                    </div>
                    <div class="col-md-12" style="margin-top: 3px; margin-bottom: 20px;">
                        <span id="inc-contact-add-phone" style="cursor: pointer;"><i class="fa fa-plus-circle" style="color: #00c433; font-size: 16px;" tabindex="0"></i> Add Phone</span>
                    </div>
                    <div class="form-group margin-top-sm">
                        <label class="control-label col-xs-12" for="inc-contact-email-1">Email:</label>
                        <div class="col-xs-12">
                            <input type="text" id="inc-contact-email-1" maxlength="128" placeholder="someone@mail.com" tabindex="0" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    {{--<div id="inc-contact-email-container">--}}
                    {{--Jquery append when click more EMAIL--}}
                    {{--</div>--}}
                    <div class="col-md-12" style="margin-top: -5px; margin-bottom: 40px;">
                        {{--<span id="inc-contact-add-email" style="cursor: pointer;"><i class="fa fa-plus-circle" style="color: #00c433; font-size: 16px; margin-top: 10px; margin-bottom: 20px;"></i> Add Email</span>--}}
                    </div>
                    <div id="inc-contact-address-container">
                        <div class="form-group margin-top-sm">
                            <label class="control-label col-xs-12" for="inc-contact-street">Address:</label>
                            <div class="col-xs-12">
                                <input type="text" id="inc-contact-street" maxlength="32" placeholder="Street" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group margin-top-sm">
                            <label class="control-label col-xs-12"></label>
                            <div class="col-xs-12">
                                <input type="text" id="inc-contact-suburb" maxlength="64" placeholder="Suburb" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group margin-top-sm">
                            <label class="control-label col-xs-12"></label>
                            <div class="col-xs-12">
                                <input type="text" id="inc-contact-city" maxlength="32" placeholder="City" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group margin-top-sm">
                            <label class="control-label col-xs-12" for="inc-contact-street"></label>
                            <div class="col-xs-12">
                                <input type="text" id="inc-contact-postal-code" maxlength="10" placeholder="Postal Code" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <span id="inc-contact-add-address" style="cursor: pointer;" tabindex="0"><i id="inc-contact-address-icon" class="fa fa-plus-circle" style="color: #00c433; font-size: 16px; margin-bottom: 20px;"></i> <span id="inc-contact-address-text"> Add Address</span></span>
                    </div>
                    <div class="ins-page-end">.</div>
                </form>

            </div>

            <div class="modal-footer">
                <p style="text-align: center;">
                    <button id="contact-close-contact" data-dismiss="modal" type="button" class="btn btn-default" tabindex="-1">Close</button>
                    <button id="contact-add-contact" type="button" class="btn btn-success" tabindex="0">Add Contact</button>

                </p>
            </div>
        </div>
    </div>
    <div style="display: none;">
        <input type="hidden" id="contact-avatar-id" value="" />
        <button id="global-open-contact" type="button" data-toggle="modal" data-target=".new-contact-modal"></button>
    </div>
</div>
{{HTML::script('assets/js/custom/includes/new-contact.js')}}