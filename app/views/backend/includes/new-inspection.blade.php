<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 28/01/17 15:29.
 */
?>

<div class="modal fade bs-new-inspection-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div id="add-inspection-container" class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">New Inspection</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal form">
                    <div class="form-group margin-top-sm">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-frequency">Reference:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="ins-reference" class="form-control col-md-7 col-xs-12">
                                <?php
                                    $assetList = Asset::where('status', '!=', 0)->lists('id');
                                ?>
                                @foreach($assetList as $id)
                                    <option value="{{$id}}">{{Asset::where('id', $id)->pluck('reference');}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group has-feedback margin-top-sm">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-inspect-date">Inspection Date:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="ins-inspect-date" class="date-picker form-control col-md-7 col-xs-12 has-feedback-left" value="" required="required" type="text">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-inspect-time-hours">Inspection Time:</label>
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <select id="ins-inspect-time-hours" class="form-control col-md-7 col-xs-12">
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08" selected>08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <select id="ins-inspect-time-min" class="form-control col-md-7 col-xs-12">
                                <option value="00">00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <select id="ins-inspect-time-ampm" class="form-control col-md-7 col-xs-12">
                                <option value="am">am</option>
                                <option value="pm">pm</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-frequency">Inspection Frequency:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="ins-frequency" class="form-control col-md-7 col-xs-12">
                                <option value="0">No Repeat Inspcetion</option>
                                <option value="1">1 Week</option>
                                <option value="2">2 Weeks</option>
                                <option value="3">3 Weeks</option>
                                <option value="4">1 Month</option>
                                <option value="5">2 Months</option>
                                <option value="6">3 Months</option>
                                <option value="7">4 Months</option>
                                <option value="8">6 Months</option>
                                <option value="9">9 Months</option>
                                <option value="10">12 Months</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-ins-close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="btn-ins-add" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Add </button>
                </div>

            </div>

        </div>
    </div>
</div>
