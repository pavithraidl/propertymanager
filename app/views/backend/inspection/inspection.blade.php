<?php
/************************| inspection.blade.php |***********************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/10/16 15:13.
 */
?>
@extends('backend.master')

@section('content')
    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa {{System::where('id', 3)->pluck('icon')}}"></i> {{System::where('id', 3)->pluck('name')}}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="title_right">
                                    <div class="top_search">
                                        <div class="input-group">
                                            <input id="property-table-search-text" type="text" onkeyup="tableSearchFilter(this);" class="form-control" placeholder="Search for...">
                                            <span class="input-group-btn">
                                                <button id="property-table-search-go" class="btn btn-default" type="button">Go!</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button id="btn-new-property" type="button" class="btn btn-success pull-right" data-toggle="modal" data-target=".bs-new-property-modal-lg"><i class="fa fa-plus"></i> New Property</button>
                        @include('backend.property.include.new-property')
                    </div>
                </div>
                <div id="ins-schedule-list-container" class="col-md-12 margin-top-sm">
                    <table class="table table-striped table-bordered search-filter">
                        <thead>
                        <tr>
                            <th>Address</th>
                            <th>Scheduled At</th>
                            <th>Inspector</th>
                            <th class="center"></th>
                        </tr>
                        </thead>
                        <tbody id="schedule-data">
                        {{--JQuery append--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{HTML::script('assets/js/custom/inspection/inspection.js')}}
    {{HTML::script('/assets/libs/jQuery-Smart-Wizard/js/jquery.smartWizard.js')}}

    <!-- jQuery Smart Wizard -->
    <script>
        $(document).ready(function() {
            $('#wizard').smartWizard();

            $('#wizard_verticle').smartWizard({
                transitionEffect: 'slide'
            });

            $('.buttonNext').addClass('btn btn-success');
            $('.buttonPrevious').addClass('btn btn-primary');
            $('.buttonFinish').addClass('btn btn-default');
        });
    </script>
@endsection