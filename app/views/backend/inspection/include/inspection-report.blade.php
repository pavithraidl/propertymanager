<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 1/03/17 15:56.
 */
?>

<div id="ins-r-container" class="col-xs-12 inspection-report inner-shadow-1" style="padding-top: 30px; margin-top: 40px;">
    <?php
    try {
        $created_at = InspectionSchedule::where('id', Inspection::where('id', $inspectionId)->pluck('scheduleid'))->pluck('started_at');
        $created_at = DateTime::createFromFormat('Y-m-d H:i:s', $created_at);
        $created_at = date_format($created_at, 'd F Y g:i a');
    }catch (Exception $ex) {
        $created_at = 'Null';
    }

        $coverImage = Inspection::where('id', $inspectionId)->pluck('coverimage');
        if($coverImage == null) {
            InspectionMedia::where('inspectionid', $inspectionId)->where('status', 1)->select('id')->get(1);
        }
        $coverImageExt = InspectionMedia::where('id', $coverImage)->pluck('ext');

    if($coverImage == null || $coverImage == '') {
        $coverImage = URL::To('/').'/assets/images/inspection/defaults/system/cover-image.jpg';
    }
    else {
        $coverImage = URL::To('/').'/assets/images/inspection/'.$inspectionId.'/'.$coverImage.'.'.$coverImageExt;
    }

    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="col-md-8">
                    <div class="ins-r-company-logo"><img src="{{URL::To('/')}}/assets/images/org/logo/{{Auth::user()->orgid}}/logo.png" style="width: 100%; height: auto;"/> </div>
                </div>
            </div>
            <div class="col-md-6 ins-r-company-details" style="text-align: right;">
                <h4 style="margin-bottom: -1px;font-size: 13px;">The Property Inspectors 2010 Limited</h4>
                <h5 class="ins-r-company-address">PO Box: 82400, Highland Park, 2143</h5>
                <h5 class="ins-r-company-email">email: pavithra@thepropertyinspectors.co.nz</h5>
                <h5 class="ins-r-company-mobile">mobile: 027 293 9808</h5>
            </div>
        </div>
        <div class="col-md-12 ins-r-report-titles" style="text-align: center;">
            <h2 class="ins-r-report-sub-title" style="margin-top: 80px;">Presenting</h2>
            <h1 class="ins-r-company-name" style="color: #4478ae;text-transform: uppercase;font-size: 34px;margin-top: 20px;">The property inspectors</h1>
            <h2 class="ins-r-report-type" style="margin-bottom: 40px;">Pre-purchase Property Report</h2>
            <div class="col-md-10 col-md-offset-1">
                <img id="ins-r-cover-img" src="{{$coverImage}}" style="width: 100%; height: auto;"/>
            </div>
        </div>
        <div class="row ins-r-report-titles">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="property-address" style="text-align: center; margin-top: 50px;">
                    1 Sample Street Auckland
                </h2>
            </div>
            <div class="col-md-12">
                <h2 style="text-align: right; margin-top: 120px;">Provided for Mr.Tana Turei</h2>
            </div>
            <div class="col-md-12">
                <div class="col-md-4" style="text-align: left">
                    12/03/2017
                </div>
            </div>
        </div>
    </div>
    <div class="ins-page-end">.</div>
    {{--Page End--}}
    <h2>Job Booking</h2>
    <hr/>

    <span id="ins-r-created-date">Inspection Started At: {{$created_at}}</span>
    <div class="col-xs-12">
        <?php
        $fieldList = InspectionFirstPageFields::where('status', 1)->lists('id');
        ?>
        @foreach($fieldList as $fieldId)
        <div class="ins-group col-xs-12">
            <div class="col-xs-6 ins-title">
                {{InspectionFirstPageFields::where('id', $fieldId)->pluck('name')}}
            </div>
            <?php
                $field = InspectionFirstPageFields::where('id', $fieldId)->pluck('field');
            ?>
            <div class="col-xs-6 ins-data ins-r-f-p-{{$field}}">
                @if($field == 'inspector')
                    {{User::where('id', Inspection::where('id', $inspectionId)->where('status', '!=', 0)->pluck($field))->pluck('fname').' '.
                    User::where('id', Inspection::where('id', $inspectionId)->where('status', '!=', 0)->pluck($field))->pluck('lname')}}
                @elseif($field == 'address')
                    <?php
                        $propertyId = InspectionSchedule::where('id', Inspection::where('id', $inspectionId)->pluck('scheduleid'))->pluck('propertyid');
                        $addressId = Property::where('id', $propertyId)->pluck('addressid');
                        $street = ContactAddress::where('id', $addressId)->pluck('street');
                        $adLine1 = ContactAddress::where('id', $addressId)->pluck('adline1');
                        $city = ContactAddress::where('id', $addressId)->pluck('adline2');
                        $postalCode = ContactAddress::where('id', $addressId)->pluck('adline2');
                        $address = sprintf('%s %s %s %s', $street, $adLine1, $city, $postalCode);
                    ?>
                    {{$address}}
                @else
                    {{Inspection::where('id', $inspectionId)->where('status', '!=', 0)->pluck($field)}}
                @endif

            </div>
        </div>
        @endforeach
    </div>
    <div class="ins-page-end">.</div>
    {{--Page End--}}
    <h2>EXPERIENCE & QUALIFICATIONS</h2>
    <hr/>
    <div id="ins-r-experience" class="col-xs-12">
        <p>Grant completed an Apprenticeship and obtained Trade Certification in Carpentry before working as a Self-Employed Builder back<span>in the 80’s.</span></p><p>Grant then went on to have a career with the NZ Police, the last seven years spent as a Forensic Crime Scene Examiner. Grant turned his expertise and keen eye for detail back towards the Building Industry, re-training to become a <span>Property</span></p><p>Inspector <span>by working under an Inspector who had been in the Pre-purchase Property Inspection field for over 10 years. Formal Training was undertaken with Grant completing the following Modules set by the New Zealand Institute of Building</span></p><p>Surveyors</p><p><span>Water Tightness Inspection Training Programme 8 Modules completed:</span></p><ul><li><p>&nbsp; &nbsp;<span>Properties of Moisture</span></p></li><li><p>&nbsp; &nbsp;<span>Forensic Techniques</span></p></li><li><p>&nbsp; &nbsp;<span>Recording and Reporting</span></p></li><li><p>&nbsp; &nbsp;<span>Decay, Fungi and Moulds</span></p></li><li><p>&nbsp; &nbsp;<span>Cladding Systems</span></p></li><li><p>&nbsp; &nbsp;<span>Asset Management</span></p></li><li><p>&nbsp; &nbsp;<span>Condition/Compliance Reporting</span></p></li><li><p>&nbsp; &nbsp;<span>The Building Act Regime</span></p><p>Grant is also a member of BOINZ (Building Officials Institute of New Zealand) &amp; New Zealand Standards</p></li></ul>
    </div>
    <div class="ins-page-end">.</div>
    {{--Page End--}}
    <h2>Certificate of property Inspection</h2>
    <hr/>
    <div id="ins-r-inspection-certificate">
        <p>Date:<br>Client:<br>Site Address:<br>Property Inspector:<br>Company:<br>Position:<br>Date of Inspection:<br>The following areas of the Property have been inspected:</p><p><span>a) Site b) Subfloor c) Exterior d) Roof Exterior e) Roof Space f) Interior g) Services</span></p><p>h) Accessory Units, Ancillary Spaces and Buildings</p><p><span>Any limitations to the coverage of the Inspection are detailed in the Written Report.</span></p><p>CERTIFICATE</p><p><span>I hereby certify that I have carried out the PROPERTY INSPECTION of the site at the above address in accordance with NZS 4306:2005 Residential Property Inspection&nbsp;</span>– <span>and I am competent to undertake this Inspection.</span></p><p>Signature:</p><p><span>An inspection carried out in accordance with NZS4306:2005 is not a statement that a property complies with the requirement of any Act, regulation or bylaw, nor is the report a warranty against any problems developing after the date of the property report. Refer NZS4306:2005 for full details.</span></p>
    </div>
    <div class="ins-page-end"></div>
    {{--Page End--}}
    <div class="ins-limitation">
        <h2>LIMITATIONS OF THIS REPORT</h2>
        <hr/>
        <div id="ins-r-limitation-container">
            {{$limitations}}
        </div>
        <div class="ins-page-end"></div>
        {{--Page End--}}
        <h2>Inspection</h2>
        <hr/>
        <div id="ins-r-inspection">
            <?php $mainList = InspectionMainCategory::where('status', 1)->lists('id') ?>
            @foreach($mainList as $mainId)
                <h3>{{InspectionMainCategory::where('id', $mainId)->pluck('name');}}</h3>
                    <?php
                    $subCatList = InspectionSubCategory::where('status', 1)->where('mainid', $mainId)->lists('id');
                ?>
                @foreach($subCatList as $subId)
                    <h4>{{InspectionSubCategory::where('id', $subId)->pluck('name')}}</h4>
                        <?php
                        $sectorList = InspectionSectors::where('status', 1)->where('subid', $subId)->lists('id');
                        ?>
                        @foreach($sectorList as $secId)

                            <h5>{{InspectionSectors::where('id', $secId)->pluck('name')}}</h5>
                            <div class="col-xs-12" style="margin-left: 20px;">
                                <?php
                                    $rowId = InspectionRow::where('mainid', $mainId)->where('subid', $subId)->where('sectorid', $secId)->where('inspectionid', $inspectionId)->where('status', 1)->pluck('id');
                                    $topic = InspectionRow::where('id', $rowId)->pluck('topic');
                                    $description = InspectionRow::where('id', $rowId)->pluck('description');
                                    $general = InspectionRow::where('id', $rowId)->pluck('general');
                                    $mediaList = null;
                                    if($rowId != null) {
                                        $mediaList = InspectionMedia::where('rowid', $rowId)->where('status', 1)->lists('id');
                                    }
                                ?>
                                {{--@if($topic == '' && $description == '' && $general == '' && $mediaList == null)--}}
                                    {{--<span style="font-style: italic; color: #b9b9b9;">No records</span>--}}
                                {{--@endif--}}
                                <?php
                                    $description == '' || null ? $description = '<span style="font-style: italic; color: #b9b9b9;">No description</span>' : '';
                                ?>
                                <strong id="ins-r-topic-{{$mainId}}-{{$subId}}-{{$secId}}">{{$topic}}</strong>
                                <div class="ins-r-description" id="ins-r-description-{{$mainId}}-{{$subId}}-{{$secId}}">
                                    <p>{{$description}}</p>
                                </div>
                                    <div class="row" id="ins-r-media-{{$mainId}}-{{$subId}}-{{$secId}}">
                                        @if($mediaList != null)
                                            @foreach($mediaList as $mediaId)
                                                <?php
                                                    $mediaType = InspectionMedia::where('id', $mediaId)->pluck('type');
                                                    $mediaExt = InspectionMedia::where('id', $mediaId)->pluck('ext');
                                                ?>
                                                @if($mediaType == 1)
                                                    <div id="ins-r-single-media-container-{{$mediaId}}" class="col-md-4 ins-single-media-container" style="margin-top: 10px;">
                                                        <img src="{{URL::To('/')}}/assets/images/inspection/{{$inspectionId}}/{{$mediaId}}.{{$mediaExt}}" style="width: 100%; height: auto; border: 1px solid rgba(122, 192, 246, 0.81);" />
                                                    </div>
                                                @elseif($mediaType == 2)
                                                    <div id="ins-r-single-media-container-{{$mediaId}}" class="col-md-4 ins-single-media-container" style="margin-top: 10px;">
                                                        <video style="width: 75%; height: auto; border: 1px solid rgba(122, 192, 246, 0.81);" controls>
                                                            <source src="{{URL::To('/')}}/assets/images/inspection/{{$inspectionId}}/{{$mediaId}}.{{$mediaExt}}" type="video/mp4">
                                                                Your browser does not support video.
                                                        </video>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                @if($general != '')
                                <div class="ins-r-important">
                                    <strong>Important!</strong>
                                    <div id="ins-r-bullet-points-{{$mainId}}-{{$subId}}-{{$secId}}" class="ins-r-bullet-points">
                                        <p>{{$general}}</p>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="ins-section-end">.</div>
                        @endforeach
                    <div class="ins-page-end"></div>
                @endforeach
            @endforeach

        </div>
        <div class="ins-page-end"></div>
        {{--Page End--}}
    </div>
</div>
