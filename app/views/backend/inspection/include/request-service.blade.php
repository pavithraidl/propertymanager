<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 23/02/17 11:14.
 */
?>

<div class="modal fade inspection-request-service" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-life-saver blue"></i> Request Service</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal form">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-frequency">Service Type:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="as-frequency" class="form-control col-md-7 col-xs-12">
                                <option value="0">Electrical</option>
                                <option value="1">Plumbing</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Error Discription</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <textarea class="resizable_textarea form-control" placeholder="Type and press enter key to create a bullet point"></textarea>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

