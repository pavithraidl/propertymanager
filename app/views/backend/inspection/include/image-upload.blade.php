<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 14/03/17 06:59.
 */
?>

<div style="display: none;">
    <form action="{{URL::Route('upload-image')}}" method="post" id="upload-image-form" enctype="multipart/form-data" class="image-upload">
        <input type="file" name="file" id="image-choose" onchange="submitForm('#upload-image-form');" accept="image/*;capture=camera|video/*;capture=camera" style="display: none;">
        <input type="hidden" id="img-inspection-id" name="inspectionid" value="" />
        <input type="hidden" id="img-inspection-mainid" name="mainid" value="" />
        <input type="hidden" id="img-inspection-catid" name="catid" value="" />
        <input type="hidden" id="img-inspection-secid" name="secid" value="" />
    </form>
</div>
