<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 16/02/17 09:57.
 */
?>
@extends('backend.master')

@section('styles')
    <!-- Include Editor style. -->
    {{HTML::style('assets/libs/text-editor/froala_editor.css')}}
    {{HTML::style('assets/libs/text-editor/froala_style.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/code_view.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/colors.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/emoticons.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/image_manager.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/image.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/line_breaker.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/table.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/char_counter.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/video.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/fullscreen.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/quick_insert.css')}}
    {{HTML::style('assets/libs/text-editor/plugins/file.css')}}

    {{HTML::style('assets/libs/text-editor/themes/gray.css')}}
@endsection

@section('content')
    @include('backend.inspection.include.request-service')
    @include('backend.inspection.include.image-upload')
    {{--genarates required variables--}}
    <?php
        $schedule = InspectionSchedule::where('id', $scheduleId)->firstOrFail();
        $property = Property::where('id', $schedule->propertyid)->firstOrFail();

            $inspectionId = null;

            if(InspectionSchedule::where('id', $scheduleId)->pluck('inspectionid')) {
                $inspectionId = InspectionSchedule::where('id', $scheduleId)->pluck('inspectionid');
                try {
                    $inspection = Inspection::where('id', $inspectionId)->where('status', '>', 0)->firstOrFail();
                    $residential = $inspection -> residential;
                    $age = $inspection -> age;
                    $type = $inspection -> type;
                    $bedrooms = $inspection -> bedrooms;
                    $bathrooms = $inspection -> bathrooms;
                    $whether = $inspection -> whether;
                    $other = $inspection -> other;
                    $present = $inspection -> present_people;
                    $status = $inspection -> status;
                    $status = $status;
                    $residential == 'Yes' ? $residential = 1 : $residential = 0;

                }
                catch (Exception $ex) {
                    $residential = 1;
                    $age = $whether = $other = '';
                    $type = 0;
                    $bedrooms = 1;
                    $bathrooms = 1;
                    $present = '';
                    $status = 1;
                }
            }
            else {
                $residential = 1;
                $age = $whether = $other = '';
                $type = 0;
                $bedrooms = 1;
                $bathrooms = 1;
                $present = '';
                $status = 1;
            }

        $limitations = Inspection::where('id', $inspectionId)->pluck('limitation');
        if($limitations == '' || $limitations == null) {
            $limitations = InspectionLimitation::where('orgid', Auth::user()->orgid)->pluck('limitation');
        }
        if($limitations == '') $limitations = '<span style="font-style: italic; color: #b9b9b9;">No Limitation Records</span>';
    ?>
    <input type="hidden" id="ins-schedule-id" value="{{$scheduleId}}" />
    <input type="hidden" id="ins-inspection-id" value="{{$inspectionId}}" />
    <input type="hidden" id="ins-inspection-status" value="{{$status}}" />
<!--    End of generating variable -->
    <div class="right_col" role="main">
        <div class="">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <h3 style="text-align: center; margin-bottom: 40px;"><strong>{{$property->reference}} </strong>New Inspection - {{str_pad($scheduleId, 5, '0', STR_PAD_LEFT)}}</h3>
                        <div id="wizard" class="form_wizard wizard_horizontal" style="min-height: 100% !important;">
                            <ul class="wizard_steps">
                                <li>
                                    <a href="#step-1" @if($status > 0) isdone="1" class="done" @endif>
                                        <span class="step_no">1</span>
                                        <span class="step_descr">
                                              Step 1<br />
                                              <small>Gather basic details</small>
                                          </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-2" @if($status > 1) isdone="1" class="done" @endif>
                                        <span class="step_no">2</span>
                                        <span class="step_descr">
                                              Step 2<br />
                                              <small>Inspection</small>
                                          </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-3" @if($status > 2) isdone="1" class="done" @endif>
                                        <span class="step_no">3</span>
                                        <span class="step_descr">
                                              Step 3<br />
                                              <small>Finalizing The Document</small>
                                          </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-4" @if($status > 3) isdone="1" class="done" @endif>
                                        <span class="step_no">4</span>
                                        <span class="step_descr">
                                              Step 4<br />
                                              <small>View Report</small>
                                          </span>
                                    </a>
                                </li>
                            </ul>
                            <div id="step-1" style="display: none;">
                                <div class="step-1-container">
                                    <form class="form-horizontal form-label-left">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ins-residential">Residential <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" value="1" class="flat" @if($residential == 1) checked @endif name="residential"> Yes
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" value="0" class="flat" @if($residential == 0) checked @endif name="residential"> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ins-age">Age of the property <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="ins-age" required="required" class="form-control col-md-7 col-xs-12" value="{{$age}}" maxlength="32">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ins-type">Inspection Type <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id="ins-type" class="form-control">
                                                    <option value="0" @if($type == 0)selected @endif>Simple Inspection</option>
                                                    <option value="1" @if($type == 1)selected @endif>Move-In Inspection</option>
                                                    <option value="2" @if($type == 2)selected @endif>Move-Out Inspection</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ins-num-bedrooms">Number of Bedrooms <span class="required">*</span></label>
                                            <div class="col-md-2 col-sm-2 col-xs-6">
                                                <input type="number" id="ins-num-bedrooms" required="required" class="form-control col-md-7 col-xs-12 only-digits" value="{{$bedrooms}}" onkeydown="maxVal(this, 999);" maxlength="3">
                                            </div>
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ins-num-bedrooms">Bathrooms <span class="required">*</span></label>
                                            <div class="col-md-2 col-sm-2 col-xs-6">
                                                <input type="number" id="ins-num-bathrooms" required="required" class="form-control col-md-7 col-xs-12 only-digits" value="{{$bathrooms}}" onkeydown="maxVal(this, 999);" maxlength="3">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ins-whether">Weather Condition <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="ins-whether" required="required" class="form-control col-md-7 col-xs-12" value="{{$whether}}" maxlength="16">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ins-other-present" class="control-label col-md-3 col-sm-3 col-xs-12">Present at time of inspection</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="ins-present" required="required" class="form-control col-md-7 col-xs-12" value="{{$present}}" maxlength="128">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ins-other-relevant-info" class="control-label col-md-3 col-sm-3 col-xs-12">Other Relevant Information</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="ins-other-relevant-info" class="resizable_textarea form-control" placeholder="Type here if you have any other relevant information about the property...">{{$other}}</textarea>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="step-2">
                                <div id="step-2-container" style="display: none;">
                                    <h2 class="StepTitle">Inspection</h2>
                                    <div class="accordion" id="main-accordion" role="tablist" aria-multiselectable="true">
                                        <?php $mainList = InspectionMainCategory::where('status', 1)->lists('id') ?>
                                        @foreach($mainList as $mainId)
                                            <div class="panel">
                                                <a class="panel-heading" role="tab" id="tab-{{$mainId}}" data-toggle="collapse" data-parent="#main-accordion" href="#collapse-{{$mainId}}" aria-controls="collapseOne">
                                                    <h4 class="panel-title"><i class="{{InspectionMainCategory::where('id', $mainId)->pluck('icon')}}"></i>&nbsp; {{InspectionMainCategory::where('id', $mainId)->pluck('name')}}</h4>
                                                </a>
                                                <div id="collapse-{{$mainId}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="tab-{{$mainId}}">
                                                    <div class="panel-body">
                                                    {{--Sub category starts here--}}
                                                    <?php
                                                    $subCatList = InspectionSubCategory::where('status', 1)->where('mainid', $mainId)->lists('id');
                                                    ?>
                                                    <!--Sub category panel starts here-->
                                                        <h3 style="text-align: center;"><i class="{{InspectionMainCategory::where('id', $mainId)->pluck('icon')}}"></i> {{InspectionMainCategory::where('id', $mainId)->pluck('name')}} Inspection</h3>
                                                        <div class="col-xs-12">
                                                            <div class="accordion" id="{{$mainId}}-accordion" role="tablist" aria-multiselectable="true">
                                                                @foreach($subCatList as $catId)
                                                                    <div class="panel">
                                                                        <a class="panel-heading" role="tab" id="tab-{{$mainId}}-{{$catId}}" data-toggle="collapse" data-parent="#{{$mainId}}-accordion" href="#collapse-{{$mainId}}-{{$catId}}" aria-controls="collapseOne">
                                                                            <h4 class="panel-title"> {{InspectionSubCategory::where('id', $catId)->pluck('name')}}</h4>
                                                                        </a>
                                                                        <div id="collapse-{{$mainId}}-{{$catId}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="tab-{{$mainId}}">
                                                                            <div class="panel-body">
                                                                                {{--Sector panels starts here--}}
                                                                                <h4 style="text-align: center;">{{InspectionSubCategory::where('id', $catId)->pluck('name')}}</h4>
                                                                                <?php
                                                                                $sectorList = InspectionSectors::where('status', 1)->where('subid', $catId)->lists('id');
                                                                                ?>

                                                                                <div class="col-xs-12">
                                                                                    <div class="accordion" id="{{$mainId}}-{{$catId}}-accordion" role="tablist" aria-multiselectable="true">
                                                                                        @foreach($sectorList as $secId)
                                                                                            <div class="panel">
                                                                                                <a class="panel-heading" role="tab" id="tab-{{$mainId}}-{{$catId}}-{{$secId}}" data-toggle="collapse" data-parent="#{{$mainId}}-{{$catId}}-accordion" href="#collapse-{{$mainId}}-{{$catId}}-{{$secId}}" aria-controls="collapseOne" onclick="getSectorData({{$mainId}}, {{$catId}}, {{$secId}});">
                                                                                                    <h4 class="panel-title"> {{InspectionSectors::where('id', $secId)->pluck('name')}}</h4>
                                                                                                </a>
                                                                                                <div id="collapse-{{$mainId}}-{{$catId}}-{{$secId}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="tab-{{$mainId}}-{{$catId}}-{{$secId}}">
                                                                                                    <div class="panel-body" id="ins-sector-body-{{$mainId}}-{{$catId}}-{{$secId}}">
                                                                                                        <h4 style="text-align: center; margin-bottom: 30px;">{{InspectionSectors::where('id', $secId)->pluck('name')}}</h4>
                                                                                                        <div>
                                                                                                            <div class="col-xs-12" style="margin-bottom: 40px;">
                                                                                                                <div class="form-group">
                                                                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ins-topic-{{$mainId}}-{{$catId}}-{{$secId}}">Topic</label>
                                                                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                                        <input type="text" id="ins-topic-{{$mainId}}-{{$catId}}-{{$secId}}" onchange="saveTopic({{$mainId}}, {{$catId}}, {{$secId}})" class="form-control col-md-7 col-xs-12" maxlength="64">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <label class="col-xs-12">Description</label>
                                                                                                            <div class="col-xs-12">
                                                                                                                {{--Text area starts here--}}
                                                                                                                <section class="editor">
                                                                                                                    <div id="description-{{$mainId}}-{{$catId}}-{{$secId}}" class='edit' data-mainid="{{$mainId}}" data-catid="{{$catId}}" data-secid="{{$secId}}" style="margin-top: 30px;">
                                                                                                                    </div>
                                                                                                                </section>
                                                                                                                {{--Text area ends here--}}

                                                                                                                <label class="col-xs-12" style="margin-top: 50px;">Images/Videos</label>
                                                                                                                <div class="row">
                                                                                                                    <div style="position: relative; display: inline-block; margin-top: 20px;">
                                                                                                                        <div id="ins-media-container-{{$mainId}}-{{$catId}}-{{$secId}}" >
                                                                                                                            {{--Jquery image append--}}
                                                                                                                        </div>

                                                                                                                        <div class="col-md-4" style="display: inline-block; height: 150px;">
                                                                                                                            {{--Image Upload--}}
                                                                                                                            <button id="btn-sector-img-upload-{{$mainId}}-{{$catId}}-{{$secId}}" class="btn btn-lg btn-default" onclick="chooseImage({{$mainId}}, {{$catId}}, {{$secId}});" style="margin-left: 28%; margin-top: 10%; margin-bottom: 40%;">
                                                                                                                                <img id="img-sector-img-upload-{{$mainId}}-{{$catId}}-{{$secId}}" src="{{URL::To('/')}}/assets/images/icons/photo-video-upload.png" style="width: 100px; height: auto;"/>
                                                                                                                            </button>
                                                                                                                            {{--Image Upload ends here--}}
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="col-md-12" style="margin-top: 40px;">
                                                                                                                    <div class="form-group">
                                                                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Important</label>
                                                                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                                                            <textarea id="ins-points-{{$mainId}}-{{$catId}}-{{$secId}}" onchange="saveKeyPoints({{$mainId}}, {{$catId}}, {{$secId}})" class="resizable_textarea form-control inspection-key-points" placeholder="Type the important points need to be highlighted!"></textarea>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-12" style="margin-top: 40px;">
                                                                                                                    <button class="pull-right btn btn-sm btn-info" data-toggle="modal" data-target=".inspection-request-service"><i class="fa fa-life-saver"></i> Book Tradesman</button>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endforeach
                                                                                    </div>
                                                                                </div>
                                                                                {{--Sector panels ends here--}}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        {{--Sub category panel ends here--}}
                                                        {{--Sub Category Ends here--}}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div id="step-3">
                                <div id="step-3-container" style="display: none;">
                                    <h2 class="StepTitle" style="margin-bottom: 40px;">Finalizing The Document</h2>
                                    <label class="col-xs-12">Select / Upload a Cover Photo</label>
                                    <div class="col-xs-12 inner-shadow-1" style="max-width: 100% !important; overflow-y: auto; white-space: nowrap; padding: 15px;">
                                        <div class="row">
                                            <div id="ins-cover-photo-container">
                                                <?php
                                                    $allImages = InspectionMedia::where('inspectionid', $inspectionId)->where('status', 1)->where('type', 1)->lists('id');
                                                ?>
                                                @foreach($allImages as $imageId)
                                                <?php
                                                    $ext = InspectionMedia::where('id', $imageId)->pluck('ext');
                                                    $coverImage = Inspection::where('id', $inspectionId)->pluck('coverimage');
                                                ?>
                                                <div id="ins-cover-image-container-{{$imageId}}" style="position: relative; display: inline-block;">
                                                    <button onclick="selectCoverImage({{$imageId}});" class="btn btn-lg btn-default" style="padding: 4px 5px !important;">
                                                        <img src="{{URL::To('/')}}/assets/images/inspection/{{$inspectionId}}/{{$imageId}}.{{$ext}}" style="width: 200px; height: auto;"/>
                                                    </button>
                                                    @if($imageId == $coverImage)
                                                        <i id="ins-cover-image-checked" class="fa fa-check-circle" style="color: #1ABB9C;position: absolute;font-size: 25px;z-index: 1;right: 100px;top: 117px;"></i>
                                                    @endif
                                                </div>
                                                @endforeach
                                            </div>
                                            {{--<div style="position: relative; display: inline-block;">--}}
                                                {{--<button class="btn btn-lg btn-default">--}}
                                                    {{--<img src="{{URL::To('/')}}/assets/images/icons/photo-video-upload.png" style="width: 150px; height: auto;"/>--}}
                                                {{--</button>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                    <label class="col-xs-12" style="margin-top: 50px;">Limitations of this report</label>
                                    <div class="col-xs-12" id="ins-limitation-container">
                                        <section class="editor">
                                            <div id="ins-limitations" class='edit' data-mainid="limitations" style="margin-top: 30px;">
                                                {{$limitations}}
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div id="step-4">
                                <div id="step-4-container" style="display: none;">
                                    <h2 class="StepTitle">View Report</h2>
                                    @include('backend.inspection.include.inspection-report')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{HTML::script('/assets/js/custom/inspection/new-inspection.js')}}
    {{HTML::script('/assets/libs/jQuery-Smart-Wizard/js/jquery.smartWizard.js')}}

    <!-- jQuery Smart Wizard -->
    <script>
        $(document).ready(function() {
            $('#wizard').smartWizard();

            $('#wizard_verticle').smartWizard({
                transitionEffect: 'slide'
            });

            $('.buttonNext').addClass('btn btn-success');
            $('.buttonPrevious').addClass('btn btn-primary');
            $('.buttonFinish').addClass('btn btn-default');
        });

        $(function(){
            $('.edit').froalaEditor({
                theme: 'gray'
            })
        });
    </script>

    <!-- Include JS file. -->
    {{HTML::script('/assets/libs/text-editor/froala_editor.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/align.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/code_beautifier.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/code_view.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/colors.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/draggable.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/emoticons.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/font_size.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/font_family.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/image.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/file.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/image_manager.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/line_breaker.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/link.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/lists.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/paragraph_format.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/paragraph_style.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/video.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/table.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/url.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/entities.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/char_counter.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/inline_style.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/quick_insert.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/save.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/fullscreen.min.js')}}
    {{HTML::script('/assets/libs/text-editor/plugins/quote.min.js')}}

@endsection
