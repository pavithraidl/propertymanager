<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 3/02/17 13:38.
 */
?>

        <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Easy Inspect | Activate Account </title>

    <!-- Bootstrap -->
    {{HTML::style('assets/libs/bootstrap/dist/css/bootstrap.min.css')}}
    <!-- Font Awesome -->
    {{HTML::style('assets/libs/font-awesome/css/font-awesome.min.css')}}
    <!-- NProgress -->
    {{HTML::style('assets/libs/nprogress/nprogress.css')}}

    <!-- Custom Theme Style -->
    {{HTML::style('assets/css/custom.min.css')}}
</head>

<body class="nav-md">
<div class="container body">
<div class="main_container">
<!-- page content -->
    @yield('content')
<!-- /page content -->
</div>
</div>

<!-- jQuery -->
{{HTML::script('assets/libs/jquery/dist/jquery.min.js')}}
<!-- Bootstrap -->
{{HTML::script('assets/libs/bootstrap/dist/js/bootstrap.min.js')}}
<!-- FastClick -->
{{HTML::script('assets/libs/fastclick/lib/fastclick.js')}}
<!-- NProgress -->
{{HTML::script('assets/libs/nprogress/nprogress.js')}}

<!-- Custom Theme Scripts -->
{{HTML::script('assets/js/custom.min.js')}}

{{HTML::script('assets/js/custom/constant.js')}}

{{HTML::script('assets/js/custom/validator.js')}}

{{HTML::script('assets/js/custom/public.functions.js')}}

@yield('script')
</body>
</html>
