<?php
/************************| master.blade.php |***********************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/10/16 10:43.
 */
?>

        <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{URL::To('/')}}/assets/images/logo-ico.png">
    <title>Property Manager</title>

    <!-- Bootstrap -->
{{HTML::style('assets/libs/bootstrap/dist/css/bootstrap.min.css')}}
<!-- Font Awesome -->
{{HTML::style('assets/libs/font-awesome/css/font-awesome.min.css')}}
<!-- NProgress -->
{{HTML::style('assets/libs/nprogress/nprogress.css')}}
<!-- iCheck -->
{{HTML::style('assets/libs/iCheck/skins/flat/green.css')}}
<!-- bootstrap-progressbar -->
{{HTML::style('assets/libs/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}
<!-- JQVMap -->
{{HTML::style('assets/libs/jqvmap/dist/jqvmap.min.css')}}
<!-- bootstrap-daterangepicker -->
{{HTML::style('assets/libs/bootstrap-daterangepicker/daterangepicker.css')}}
{{--Notify--}}
{{HTML::style('assets/libs/pnotify/dist/pnotify.css')}}
{{HTML::style('assets/libs/pnotify/dist/pnotify.buttons.css')}}
{{HTML::style('assets/libs/pnotify/dist/pnotify.nonblock.css')}}

    {{--Datepicker--}}
{{HTML::style('assets/libs/datepicker/themes/classic.css')}}
{{HTML::style('assets/libs/datepicker/themes/classic.date.css')}}
{{HTML::style('assets/libs/datepicker/themes/classic.time.css')}}

    {{--Image Uploader--}}
{{HTML::script('/assets/libs/jquery/jquery-1.11.1.min.js')}}
{{ HTML::script('assets/libs/jquery-forms/jquery.form.js')}}
{{HTML::script('assets/js/custom/public/image-uploader.js')}}
{{HTML::script('assets/js/custom/public/constant.js')}}

{{--IOS Switch--}}
{{HTML::style('assets/libs/ios7-switch/ios7-switch.css')}}

<!-- Custom Theme Style -->
    {{HTML::style('assets/css/custom.css')}}

    {{--Custom pagebuild styles--}}
    @yield('styles')
    {{--End custom pagebuild styles--}}
</head>
<?php
        $userId = Auth::user()->id;
    $profilePic = URL::To('/').'/assets/images/users/'.$userId.'/user-35.jpg';
    try {
        getimagesize($profilePic);
    }
    catch(Exception $ex) {
        $profilePic = URL::To('/').'/assets/images/users/default/user-35.jpg';
    }
?>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{URL::Route('dashboard')}}" class="site_title"><i class="fa fa-paste"></i> <span>Property Manager</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="{{$profilePic}}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>{{Auth::user()->fname}} {{Auth::user()->lname}}</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <?php
                            $systemList = System::where('status', 1)->where('visibility', 1)->lists('id');
                            $otherList = System::where('status', 1)->where('visibility', 2)->lists('id');
                            ?>
                            @foreach($systemList as $systemId)
                                @if(SystemRoutingAccess::where('systemid', $systemId)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3 || System::where('id', $systemId)->where('allow_default', 1)->pluck('id'))
                                    <li>
                                        <a href="{{URL::Route(System::where('id', $systemId)->pluck('route'))}}"><i class="fa {{System::where('id', $systemId)->pluck('icon')}}"></i> {{System::where('id', $systemId)->pluck('name')}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="{{URL::To('/')}}/assets/images/user.jpg" alt="">Isuru Liyanage
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> Profile</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a href="{{URL::Route('log-out')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-globe"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="{{URL::To('/')}}/assets/images/user.jpg" alt="Profile Image" /></span>
                                        <span>
                          <span>Isuru Liyanage</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="{{URL::To('/')}}/assets/images/user.jpg" alt="Profile Image" /></span>
                                        <span>
                          <span>Isuru Liyanage</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="{{URL::To('/')}}/assets/images/user.jpg" alt="Profile Image" /></span>
                                        <span>
                          <span>Isuru Liyanage</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="{{URL::To('/')}}/assets/images/user.jpg" alt="Profile Image" /></span>
                                        <span>
                          <span>Isuru Liyanage</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        {{--Content connects to here--}}
            @yield('content')
        {{--Content end from here--}}

    <!-- footer content -->
        <footer>
            <div class="pull-right">
                IDL Creations
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
{{--{{HTML::script('assets/libs/jquery/dist/jquery.min.js')}}--}}
<!-- Bootstrap -->
{{HTML::script('assets/libs/bootstrap/dist/js/bootstrap.min.js')}}
<!-- FastClick -->
{{HTML::script('assets/libs/fastclick/lib/fastclick.js')}}
<!-- NProgress -->
{{HTML::script('assets/libs/nprogress/nprogress.js')}}
<!-- bootstrap-progressbar -->
{{HTML::script('assets/libs/bootstrap-progressbar/bootstrap-progressbar.min.js')}}
<!-- iCheck -->
{{HTML::script('assets/libs/iCheck/icheck.min.js')}}
<!-- Skycons -->
{{HTML::script('assets/libs/skycons/skycons.js')}}
<!-- Flot -->
{{HTML::script('assets/libs/Flot/jquery.flot.js')}}
{{HTML::script('assets/libs/Flot/jquery.flot.pie.js')}}
{{HTML::script('assets/libs/Flot/jquery.flot.time.js')}}
{{HTML::script('assets/libs/Flot/jquery.flot.stack.js')}}
{{HTML::script('assets/libs/Flot/jquery.flot.resize.js')}}
<!-- Flot plugins -->
{{HTML::script('assets/libs/flot.orderbars/js/jquery.flot.orderBars.js')}}
{{HTML::script('assets/libs/flot-spline/js/jquery.flot.spline.min.js')}}
{{HTML::script('assets/libs/flot.curvedlines/curvedLines.js')}}
<!-- DateJS -->
{{HTML::script('assets/libs/DateJS/build/date.js')}}
{{HTML::script('assets/libs/jqvmap/examples/js/jquery.vmap.sampledata.js')}}
<!-- bootstrap-daterangepicker -->
{{HTML::script('assets/libs/moment/min/moment.min.js')}}
{{HTML::script('assets/libs/bootstrap-daterangepicker/daterangepicker.js')}}

{{--Date Picker--}}
{{HTML::script('assets/libs/datepicker/legacy.js')}}
{{HTML::script('assets/libs/datepicker/picker.js')}}
{{HTML::script('assets/libs/datepicker/picker.date.js')}}
{{HTML::script('assets/libs/datepicker/picker.time.js')}}

<!-- Custom Theme Scripts -->
{{HTML::script('assets/js/custom.min.js')}}
{{HTML::script('assets/js/custom/public/public.functions.js')}}
{{HTML::script('assets/js/custom/public/validator.js')}}

{{--Notiify--}}
{{HTML::script('assets/libs/pnotify/dist/pnotify.js')}}
{{HTML::script('assets/libs/pnotify/dist/pnotify.buttons.js')}}
{{HTML::script('assets/libs/pnotify/dist/pnotify.nonblock.js')}}

{{--IOS Switch--}}
{{HTML::script('/assets/libs/ios7-switch/ios7.switch.js')}}

@yield('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        //ICHECK
        $('input:not(.ios-switch)').iCheck({
            checkboxClass: 'icheckbox_square-aero',
            radioClass: 'iradio_square-aero',
            increaseArea: '20%' // optional
        });

// IOS7 SWITCH
        $(".ios-switch").each(function(){
            mySwitch = new Switch(this);
        });
    });

//    Date picker
    $('.datepicker').pickadate();
    $('.timepicker').pickatime({
        min: new Date(2015,3,20,7),
        max: new Date(2015,7,14,18,30)
    });
</script>

</body>
</html>

