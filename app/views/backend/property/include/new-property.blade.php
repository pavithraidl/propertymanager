<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 19/12/16 12:20.
 */
?>
@include('backend.includes.new-contact')
<div id="mm-add-property-container" class="modal fade bs-new-property-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-home"></i> Add New Property</h4>
            </div>
            <div class="modal-body" style="max-height: 600px;">
                <div class="form-horizontal form" style="max-height: 525px; overflow-y: auto;">
                    <div class="form-group" style="margin-bottom: 20px;">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 margin-top-sm" for="mm-pro-owner">Property Owner:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row" style="margin-top: 40px;">
                                <div id="mm-pro-owner-container" class="col-md-12">
                                    <div id="mm-pro-add-owner" class="tenent-container" tabindex="1">
                                        <span style="margin-left: 10px;"><i class="fa fa-user-md"></i> Add</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Address:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="mm-pro-street" placeholder="Street" onblur="requiredValidator(this, 'Please fill the street');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="2">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-pro-suburb"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="mm-pro-suburb" placeholder="Suburb" onblur="requiredValidator(this, 'Please fill the suburb');" maxlength="64" class="form-control col-md-7 col-xs-12" tabindex="3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-pro-city"></span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <input type="text" id="mm-pro-city" placeholder="City" onblur="requiredValidator(this, 'Please fill the city');" maxlength="64" class="form-control col-md-7 col-xs-12" tabindex="4">
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <input type="text" id="mm-pro-postal-code" placeholder="Postal Code" maxlength="10" class="form-control col-md-7 col-xs-12" tabindex="5">
                        </div>
                    </div>
                    <div class="form-group margin-top-sm">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-pro-reference">Reference:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="mm-pro-reference" onblur="requiredValidator(this, 'Please fill the reference');" maxlength="32" placeholder="Your Internal Asset Reference" class="form-control col-md-7 col-xs-12" tabindex="6">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-pro-manager">Property Manager:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12" tabindex="7">
                            <select id="mm-pro-manager" class="form-control col-md-7 col-xs-12">
                                <option value="0" disabled selected>Select Property Manager</option>
                                {{--Jquery append--}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 margin-top-sm" for="mm-pro-tenant">Tenant:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row" style="margin-top: 40px;">
                                <div id="mm-pro-tenant-container" class="col-md-12">
                                    <div id="mm-pro-add-tenant" class="tenent-container" tabindex="8">
                                        <span style="margin-left: 10px;"><i class="fa fa-user"></i> Add</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group has-feedback" style="margin-top: 35px; cursor: pointer;">
                        <div id="pro-troggle-inspection-schedule" class="col-md-push-3  col-sm-push-3 col-md-6 col-sm-6 col-xs-12" tabindex="9">
                            <h5><i class="fa fa-calendar"></i> Schedule Inspections</h5>
                            <hr style="margin-left: 40%; margin-top: -4%;"/>
                            <i id="pro-toggle-inspection-schedule-icon" class="fa fa-chevron-circle-up" style="position: absolute; right: 0px; top: 10px; font-size: 16px;"></i>
                        </div>
                    </div>
                    <div id="as-schedule-inspection-container">
                        <div class="form-group has-feedback margin-top-sm">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-inspect-date">Inspection Date:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="pro-inspection-date" class="datepicker form-control col-md-7 col-xs-12 has-feedback-left" value="" required="required" type="text">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-inspect-date">Inspection Time:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="pro-inspection-time" class="timepicker form-control col-md-7 col-xs-12 has-feedback-left" value="" required="required" type="text">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-frequency">Inspection Frequency:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="pro-frequency" class="form-control col-md-7 col-xs-12">
                                    <option value="0">No Repeat Inspcetion</option>
                                    <option value="1">1 Week</option>
                                    <option value="2">2 Weeks</option>
                                    <option value="3">3 Weeks</option>
                                    <option value="4">1 Month</option>
                                    <option value="5">2 Months</option>
                                    <option value="6">3 Months</option>
                                    <option value="7">4 Months</option>
                                    <option value="8">6 Months</option>
                                    <option value="9">9 Months</option>
                                    <option value="10">12 Months</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-frequency">Inspection Type:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="pro-inspection-type" class="form-control col-md-7 col-xs-12">
                                    <option value="0">Simple Inspection</option>
                                    <option value="1">Move-In Inspection</option>
                                    <option value="2">Move-Out Inspection</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-manager">Assigned Inspector:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="pro-inspector" class="form-control col-md-7 col-xs-12">
                                    <option value="0" disabled selected>Assign Inspector</option>
                                    {{--Jquery append--}}
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group margin-top-sm">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-alarm">Alarm:</label>
                        <div class="col-md-4 col-sm-4 col-xs-8">
                            <input type="text" id="pro-alarm" required="required" placeholder="Alarm Code" maxlength="16" class="form-control col-md-7 col-xs-12" tabindex="10">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="as-key-location">Key:</label>
                        <div class="col-md-4 col-sm-4 col-xs-8">
                            <input type="text" id="pro-key-location" required="required" placeholder="Key Location" maxlength="64" class="form-control col-md-7 col-xs-12" tabindex="11">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-as-close" type="button" class="btn btn-default" data-dismiss="modal" tabindex="13">Close</button>
                    <button id="pro-btn-add" type="button" class="btn btn-success" tabindex="12"><i class="fa fa-save"></i> Add </button>
                </div>
            </div>
        </div>
    </div>
</div>
{{HTML::script('assets/js/custom/property/new-property.js')}}