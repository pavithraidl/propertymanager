<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 23/05/17 11:51.
 */
?>

@extends('backend.master')

@section('content')

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa {{System::where('id', 8)->pluck('icon')}}"></i> {{System::where('id', 8)->pluck('name')}}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-align-left"></i> System Administrator <small>Manage System</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <!-- start accordion -->
                        <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php
                            $systemList = System::where('status', '!=', 0)->lists('id');
                            ?>
                            @foreach($systemList as $systemId)
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="heading{{$systemId}}" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$systemId}}" aria-expanded="false" aria-controls="collapse{{$systemId}}" style="position: relative;">
                                        <h4 class="panel-title"><i class="fa {{System::where('id', $systemId)->pluck('icon')}}"></i>&nbsp;&nbsp; {{System::where('id', $systemId)->pluck('name')}}</h4>
                                        <?php
                                            $status = System::where('id', $systemId)->pluck('status');
                                            $exceptionLed = $Systemled = '';
                                            if($status == 1) $Systemled = '-green';
                                            else if($status == 2) $Systemled = '-yellow';
                                            else if($status == 3) $Systemled = '-red r-blink-n';

                                            $exceptions = SystemException::where('systemid', $systemId)->where('status', 1)->max('id');
                                            if($exceptions != null || $exceptions != '') $exceptionLed = '-red r-blink-n';
                                            else $exceptionLed = '-green';
                                        ?>
                                        <div id="system-led-status-{{$systemId}}" class="led{{$Systemled}}" style="position: absolute; right: 20px; top: 16px;"></div>
                                        <div id="system-led-exceptions-{{$systemId}}" class="led{{$exceptionLed}}" style="position: absolute; right: 40px; top: 16px;"></div>
                                    </a>
                                    <div id="collapse{{$systemId}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$systemId}}">
                                        <div id="panel-body-{{$systemId}}" class="panel-body">
                                            <input data-id= "system-status-{{$systemId}}" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-right" @if($status == 1)checked @endif/>
                                            <div class="" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: 40px;">
                                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#tab_info" id="info-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a></li>
                                                    <li role="presentation" class=""><a href="#tab_class" role="tab" id="class-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-code"></i> Classes </a></li>
                                                    <li role="presentation" class=""><a href="#tab_view" role="tab" id="view-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-list-alt"></i> Views </a></li>
                                                </ul>
                                                <div id="myTabContent" class="tab-content">
                                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_info" aria-labelledby="info-tab" style="padding: 10px;">
                                                        <div class="row top_tiles" style="margin: 10px 0;">
                                                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                <span>Total Visitors</span>
                                                                <h2>231,809</h2>
                                                                <span class="sparkline_one" style="height: 160px;">
                                                        <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                    </span>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                <span>Total Income</span>
                                                                <h2>$ 231,809</h2>
                                                                <span class="sparkline_one" style="height: 160px;">
                                                        <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                    </span>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                <span>Total Users</span>
                                                                <h2>231,809</h2>
                                                                <span class="sparkline_one" style="height: 160px;">
                                                        <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                    </span>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                <span>Total bandwidth</span>
                                                                <h2>231,809</h2>
                                                                <span class="sparkline_two" style="height: 160px;">
                                                        <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                    </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="col-md-12 bs-callout bs-callout-danger">
                                                                <h4>System Exceptions</h4>
                                                                <div class="form-horizontal form">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Exception Ratio:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                                            <progress max="100" value="50"></progress>
                                                                            <span style="  position: absolute; top: 10px; left: 45%; color: #91acff; font-weight: 700;">50%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Running Exception:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 8px; font-size: 16px; color: #fd132f;">
                                                                            0
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Pending Exception:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px; color: #e49a48;">
                                                                            0
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Fixed Bugs:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px; color: #5cd07e;">
                                                                            0
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <p style="text-align: right">
                                                                    <button class="btn btn-danger">Exceptions</button>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="col-md-12 bs-callout bs-callout-info">
                                                                <h4>System Info</h4>
                                                                <div class="form-horizontal form">
                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">ID:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                            10
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Route:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                            #
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">List Order:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                            200
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Classes:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                            4
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Functions:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                            23
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Created By:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                            Pavithra Isuru
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="margin-top: -10px;">
                                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Created At:</label>
                                                                        <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                            23 Mar 2017
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="tab_class" aria-labelledby="class-tab">
                                                        <div class="led-green"></div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="tab_view" aria-labelledby="view-tab">
                                                        <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo
                                                            booth letterpress, commodo enim craft beer mlkshk </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <!-- end of accordion -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('assets/libs/jquery-sparkline/dist/jquery.sparkline.js')}}
    {{HTML::script('assets/js/custom/system/manage.js')}}
@endsection
