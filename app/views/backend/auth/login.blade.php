<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 26/01/17 07:11.
 */
?>

        <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Property Manager | Login</title>

    <!-- Bootstrap -->
    {{HTML::style('assets/libs/bootstrap/dist/css/bootstrap.min.css')}}
    <!-- Font Awesome -->
    {{HTML::style('assets/libs/font-awesome/css/font-awesome.min.css')}}
    <!-- NProgress -->
    {{HTML::style('assets/libs/nprogress/nprogress.css')}}
    <!-- Animate.css -->
    {{HTML::style('assets/libs/animate.css/animate.min.css')}}
    <!-- Custom Theme Style -->
    {{HTML::style('assets/css/custom.min.css')}}
</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form role="form" action="{{ URL::route('post-login') }}" method="post" id="registerForm">
                    <h1>Login</h1>
                    @if( Session::has('global') )
                        <p style="color:@if($errors->has('email') || Session::has('global') && Session::get('global') != 'Activated! you can now sign in' ) red @else #00ca6d @endif">* {{ Session::get('global') }}</p>
                    @endif
                    <div>
                        <input id="email" name="email" type="email" class="form-control text-input" placeholder="Email" required="true" />
                    </div>
                    <div>
                        <input id="password" name="password"  type="password" class="form-control text-input" placeholder="********" required="true"/>
                    </div>
                    <div class="form-group pull-left">
                        <input type="checkbox" name="remember" id="remember"/>
                        <label for="remember">Remember me</label>
                        <a class="reset_pass" href="#">Lost your password?</a>
                    </div>
                    <div class="col-md-12" style="margin-top: 20px;">
                        <button type="submit" id="register" class="btn btn-default submit">Log in</button>
                    </div>
                    <div class="clearfix"></div>

                    <div class="separator">
                        <p class="change_link">New to site?
                            <a href="#signup" class="to_register"> Create Account </a>
                        </p>

                        <div class="clearfix"></div>
                        <br />

                        <div>
                            <h1><i class="fa  fa-paste"></i> Property Manager</h1>
                            <p>©{{date('Y')}} All Rights Reserved. Privacy and Terms</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>

        <div id="register" class="animate form registration_form">
            <section class="login_content">
                <form>
                    <h1>Create Account</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Username" required="" />
                    </div>
                    <div>
                        <input type="email" class="form-control" placeholder="Email" required="" />
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" required="" />
                    </div>
                    <div>
                        <a class="btn btn-default submit" href="index.html">Submit</a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <p class="change_link">Already a member ?
                            <a href="#signin" class="to_register"> Log in </a>
                        </p>

                        <div class="clearfix"></div>
                        <br />

                        <div>
                            <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                            <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
</html>
