<?php
/************************| services.blade.php |***********************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/10/16 15:13.
 */
?>

@extends('backend.master')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-list" style="color: #48b93e;"></i>&nbsp;&nbsp; Asset List</a></li>
                                <li role="presentation" ><a href="#tab_content2" id="asset-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-building" style="color: #48b93e;"></i>&nbsp;&nbsp; Asset - 5 Hedley</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                    <div class="col-md-12">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <select id="df-schedule-time-filter" class="form-control" required>
                                                <option value="0">All Property Managers</option>
                                                <option value="1">Pavithra Isuru</option>
                                                <option value="2">Jone Doe</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <select id="df-schedule-time-filter" class="form-control" required>
                                                <option value="0">All Inspection Type</option>
                                                <option value="1">Move-In Inspection</option>
                                                <option value="2">Move-Out Inspection</option>
                                                <option value="3">Simple Inspection</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> New Schedule</button>
                                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">Add New Asset</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-horizontal form">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Address:</label>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <input type="text" id="as-add-line1" placeholder="Line 1" required="required" class="form-control col-md-7 col-xs-12">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"></span>
                                                                    </label>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <input type="text" id="as-add-line1" placeholder="Line 2" required="required" class="form-control col-md-7 col-xs-12">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group margin-top-sm">
                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Reference:</label>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <input type="text" id="last-name" name="last-name" required="required" placeholder="Your Internal Asset Reference" class="form-control col-md-7 col-xs-12">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group has-feedback margin-top-sm">
                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Inspection Date:</label>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <input id="birthday" class="date-picker form-control col-md-7 col-xs-12 has-feedback-left" value="" required="required" type="text">
                                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Inspection Frequency:</label>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <select id="" class="form-control col-md-7 col-xs-12">
                                                                            <option value="0">No Repeat Inspcetion</option>
                                                                            <option value="1">1 Week</option>
                                                                            <option value="2">2 Weeks</option>
                                                                            <option value="3">3 Weeks</option>
                                                                            <option value="4">1 Month</option>
                                                                            <option value="5">2 Months</option>
                                                                            <option value="6">3 Months</option>
                                                                            <option value="7">4 Months</option>
                                                                            <option value="8">6 Months</option>
                                                                            <option value="9">9 Months</option>
                                                                            <option value="10">12 Months</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group margin-top-sm">
                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alarm:</label>
                                                                    <div class="col-md-4 col-sm-4 col-xs-8">
                                                                        <input type="text" id="last-name" name="last-name" required="required" placeholder="Alarm Code" class="form-control col-md-7 col-xs-12">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Key:</label>
                                                                    <div class="col-md-4 col-sm-4 col-xs-8">
                                                                        <input type="text" id="last-name" name="last-name" required="required" placeholder="Key Location" class="form-control col-md-7 col-xs-12">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary"><i class="fa fa-save"></i> Add </button>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 margin-top-sm">
                                        <table id="datatable-buttons" class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Inspection Type</th>
                                                <th>Scheduled Date</th>
                                                <th>Inspector</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>5 Hedley Road, Mt Roskill<br/><span class="blue text-sm">Near the ground</span> </td>
                                                <td>Sample Inspection</td>
                                                <td>Nov 08, 2016 16:55 - 17:30</td>
                                                <td>Isuru Liyanage</td>
                                                <td align="center">
                                                    <div class="input-group-btn">
                                                        <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-download"></i> Action <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                            <li><a href="#">Edit Inspection</a></li>
                                                            <li class="divider"></li>
                                                            <li><a href="#">MS Word Report</a></li>
                                                            <li><a href="#">PDF Report</a></li>
                                                            <li><a href="#">Send PDF Report</a></li>
                                                            <li><a href="#">Send Word Report</a></li>
                                                            <li><a href="#">Download Photos</a></li>
                                                            <li><a href="#">Share Web Report</a></li>
                                                            <li class="divider"></li>
                                                            <li><a href="#">Delete This Inspection</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="tab_content2" aria-labelledby="asset-tab">
                                    <div class="col-md-12">
                                        <div class="form-horizontal form col-md-7 margin-top-md">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Address:</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input type="text" id="as-add-line1" placeholder="Line 1" required="required" class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name"></span>
                                                </label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input type="text" id="as-add-line1" placeholder="Line 2" required="required" class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="form-group margin-top-sm">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="last-name">Reference:</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input type="text" id="last-name" name="last-name" required="required" placeholder="Your Internal Asset Reference" class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="form-group has-feedback margin-top-sm">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Scheduled DateTime:</label>
                                                <div class="col-md-4 col-sm-8 col-xs-12">
                                                    <input id="scheduled-inspection-date" class="date-picker form-control col-md-7 col-xs-12 has-feedback-left" value="" required="required" type="text">
                                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <select class="col-md-4 col-sm-8 col-xs-12" style="height: 35px;">
                                                    <option>08:00 am</option>
                                                    <option>08:30 am</option>
                                                    <option>09:00 am</option>
                                                    <option>09:30 am</option>
                                                    <option>10:00 am</option>
                                                    <option>10:30 am</option>
                                                    <option>11:00 am</option>
                                                    <option>11:30 am</option>
                                                    <option>12:00 pm</option>
                                                    <option>12:30 pm</option>
                                                    <option>01:00 pm</option>
                                                    <option>01:30 pm</option>
                                                    <option>02:00 pm</option>
                                                    <option>02:30 pm</option>
                                                    <option>03:00 pm</option>
                                                    <option>03:30 pm</option>
                                                    <option>04:00 pm</option>
                                                    <option>04:30 pm</option>
                                                    <option>05:00 pm</option>
                                                    <option>05:30 pm</option>
                                                    <option>06:00 pm</option>
                                                    <option>06:30 pm</option>
                                                    <option>07:00 pm</option>
                                                    <option>07:30 pm</option>
                                                    <option>08:00 pm</option>
                                                    <option>08:30 pm</option>
                                                    <option>09:00 pm</option>
                                                    <option>09:30 pm</option>
                                                    <option>10:00 pm</option>
                                                    <option>10:30 pm</option>
                                                    <option>11:00 pm</option>
                                                    <option>11:30 pm</option>
                                                    <option>12:00 pm</option>
                                                    <option>12:30 pm</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Inspection Frequency:</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select id="" class="form-control col-md-7 col-xs-12">
                                                        <option value="0">No Repeat Inspcetion</option>
                                                        <option value="1">1 Week</option>
                                                        <option value="2">2 Weeks</option>
                                                        <option value="3">3 Weeks</option>
                                                        <option value="4">1 Month</option>
                                                        <option value="5">2 Months</option>
                                                        <option value="6">3 Months</option>
                                                        <option value="7">4 Months</option>
                                                        <option value="8">6 Months</option>
                                                        <option value="9">9 Months</option>
                                                        <option value="10">12 Months</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group margin-top-sm">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="last-name">Alarm:</label>
                                                <div class="col-md-4 col-sm-4 col-xs-8">
                                                    <input type="text" id="last-name" name="last-name" required="required" placeholder="Alarm Code" class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="last-name">Key:</label>
                                                <div class="col-md-4 col-sm-4 col-xs-8">
                                                    <input type="text" id="last-name" name="last-name" required="required" placeholder="Key Location" class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="margin-top-sm align-center">
                                                <button class="btn btn-lg btn-info"><i class="fa fa-save"></i> Update </button>
                                                <button class="btn btn-lg btn-danger"><i class="fa fa-trash"></i> Delete </button>
                                            </div>
                                        </div>
                                        <div class="col-md-5 margin-top-md">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d408595.91688613454!2d174.58528535403363!3d-36.86269418948528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d0d47fb5a9ce6fb%3A0x500ef6143a29917!2sAuckland!5e0!3m2!1sen!2snz!4v1479014234872" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#scheduled-inspection-date').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_1"
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#in-filter-end').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_1"
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });

        $(document).ready(function() {
            var handleDataTableButtons = function() {
                if ($("#datatable-buttons").length) {
                    $("#datatable-buttons").DataTable({
                        dom: "Bfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true
                    });
                }
            };

            TableManageButtons = function() {
                "use strict";
                return {
                    init: function() {
                        handleDataTableButtons();
                    }
                };
            }();

            $('#datatable').dataTable();

            $('#datatable-keytable').DataTable({
                keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
                ajax: "js/datatables/json/scroller-demo.json",
                deferRender: true,
                scrollY: 380,
                scrollCollapse: true,
                scroller: true
            });

            $('#datatable-fixed-header').DataTable({
                fixedHeader: true
            });

            var $datatable = $('#datatable-checkbox');

            $datatable.dataTable({
                'order': [[ 1, 'asc' ]],
                'columnDefs': [
                    { orderable: false, targets: [0] }
                ]
            });
            $datatable.on('draw.dt', function() {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_flat-green'
                });
            });

            TableManageButtons.init();
        });
    </script>

    {{--Data tables--}}
    {{HTML::script('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}
    {{HTML::script('assets/libs/datatables.net-bs/js/dataTables.bootstrap.min.js')}}
    {{HTML::script('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}
    {{HTML::script('assets/libs/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}
    {{HTML::script('assets/libs/datatables.net-buttons/js/buttons.flash.min.js')}}
    {{HTML::script('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}
    {{HTML::script('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}
    {{HTML::script('assets/libs/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}
    {{HTML::script('assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js')}}
    {{HTML::script('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}
    {{HTML::script('assets/libs/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}
    {{HTML::script('assets/libs/datatables.net-scroller/js/datatables.scroller.min.js')}}
    {{HTML::script('assets/libs/jszip/dist/jszip.min.js')}}
    {{HTML::script('assets/libs/pdfmake/build/pdfmake.min.js')}}
    {{HTML::script('assets/libs/pdfmake/build/vfs_fonts.js')}}
@endsection
