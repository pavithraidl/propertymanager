-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 03, 2017 at 05:27 AM
-- Server version: 5.6.33
-- PHP Version: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `inspectdb`
--
CREATE DATABASE IF NOT EXISTS `inspectdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `inspectdb`;

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `activity` varchar(128) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `userid`, `orgid`, `activity`, `created_at`) VALUES
(1, 10, 1, 'Login to the system', '2017-02-10 21:18:47'),
(2, 10, 1, 'Login to the system', '2017-02-10 21:18:50'),
(3, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(4, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(5, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(6, 11, 1, 'Login to the system', '2017-02-10 21:21:15'),
(7, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(8, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(9, 10, 1, 'Enabled user - Yahoo Yahoo(pavithraidl@yahoo.com) access to Dashboard', '2017-02-10 21:21:15'),
(10, 10, 1, 'Enabled user - Yahoo Yahoo(pavithraidl@yahoo.com) access to Dashboard', '2017-02-10 21:21:15'),
(11, 10, 1, 'Enabled user - Yahoo Yahoo(pavithraidl@yahoo.com) access to Dashboard', '2017-02-10 21:21:15'),
(12, 10, 1, 'Disabled user - Yahoo Yahoo(pavithraidl@yahoo.com) access to Dashboard', '2017-02-10 21:21:15'),
(13, 10, 1, 'Enabled user - Yahoo Yahoo(pavithraidl@yahoo.com) access to Dashboard', '2017-02-10 21:21:15'),
(14, 10, 1, 'Enabled user - Yahoo Isuru(pavithraidl@yahoo.com) access to Assets', '2017-02-10 21:21:15'),
(15, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(16, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(17, 10, 1, 'Disabled user - Yahoo Isuru(pavithraidl@yahoo.com) User account', '2017-02-10 21:21:15'),
(18, 10, 1, 'Enabled user - Yahoo Isuru(pavithraidl@yahoo.com) User account', '2017-02-10 21:21:15'),
(19, 10, 1, 'Resent the activation email to Test (test@test.com)', '2017-02-10 21:21:15'),
(20, 10, 1, 'Resent the activation email to Test (test@test.com)', '2017-02-10 21:21:15'),
(21, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(22, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(23, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(24, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(25, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(26, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(27, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(28, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(29, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(30, 10, 1, 'Enabled user - Yahoo Isuru(pavithraidl@yahoo.com) access to Inspections', '2017-02-10 21:21:15'),
(31, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(32, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(33, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(34, 10, 1, 'Enabled user - Yahoo Isuru() User account', '2017-02-10 21:21:15'),
(35, 10, 1, 'Disabled user - Yahoo Isuru() User account', '2017-02-10 21:21:15'),
(36, 10, 1, 'Enabled user - Yahoo Isuru() User account', '2017-02-10 21:21:15'),
(37, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(38, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(39, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(40, 11, 1, 'Login to the system', '2017-02-10 21:21:15'),
(41, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(42, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(43, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(44, 10, 1, 'Enabled user - Yahoo Isuru(pavithraidl@yahoo.com) access to Inspections', '2017-02-10 21:21:15'),
(45, 10, 1, 'Disabled user - Yahoo Isuru() User account', '2017-02-10 21:21:15'),
(46, 10, 1, 'Enabled user - Yahoo Isuru() User account', '2017-02-10 21:21:15'),
(47, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(48, 10, 1, 'Deleted - Yahoo Isuru User account', '2017-02-10 21:21:15'),
(49, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(50, 11, 1, 'Login to the system', '2017-02-10 21:21:15'),
(51, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(52, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(53, 10, 1, 'Login to the system', '2017-02-10 21:21:15'),
(54, 10, 1, 'Enabled user - Yahoo Isuru(pavithraidl@yahoo.com) access to Inspections', '2017-02-10 21:21:15'),
(55, 10, 1, 'Login to the system', '2017-02-10 14:00:03'),
(56, 10, 1, 'Enabled Organization - Test Org\'s Organization account', '2017-02-10 14:11:48'),
(57, 10, 1, 'Enabled Organization -   access to Assets', '2017-02-10 14:21:16'),
(58, 10, 1, 'Enabled Organization - Test Org  access to Assets', '2017-02-10 14:22:08'),
(59, 10, 1, 'Deleted - Test Org\'s Organization account User account', '2017-02-10 14:41:25'),
(60, 10, 1, 'Login to the system', '2017-02-11 11:13:58'),
(61, 10, 1, 'Login to the system', '2017-02-11 11:19:32'),
(62, 10, 1, 'Login to the system', '2017-02-12 05:11:05'),
(63, 10, 1, 'Deleted - Test Org\'s Organization account User account', '2017-02-12 05:11:27'),
(64, 13, 3, 'Login to the system', '2017-02-14 08:13:01'),
(65, 10, 1, 'Login to the system', '2017-02-14 08:17:18'),
(66, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Dashboard', '2017-02-14 08:20:45'),
(67, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Assets', '2017-02-14 08:20:48'),
(68, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Inspections', '2017-02-14 08:20:50'),
(69, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Services', '2017-02-14 08:20:54'),
(70, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Users', '2017-02-14 08:20:58'),
(71, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Settings', '2017-02-14 08:21:00'),
(72, 10, 1, 'Disabled Organization - Gamma (Pvt) Ltd  access to Assets', '2017-02-14 08:21:28'),
(73, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Users', '2017-02-14 08:39:27'),
(74, 10, 1, 'Disabled Organization - Gamma (Pvt) Ltd  access to Users', '2017-02-14 08:39:35'),
(75, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Users', '2017-02-14 08:39:51'),
(76, 10, 1, 'Disabled Organization - Gamma (Pvt) Ltd  access to Users', '2017-02-14 08:42:33'),
(77, 10, 1, 'Login to the system', '2017-02-14 10:35:09'),
(78, 10, 1, 'Login to the system', '2017-02-14 12:23:14'),
(79, 10, 1, 'Login to the system', '2017-02-14 14:40:05'),
(80, 10, 1, 'Login to the system', '2017-02-15 06:54:44'),
(81, 10, 1, 'Login to the system', '2017-02-15 08:12:26'),
(82, 13, 3, 'Login to the system', '2017-02-15 10:01:03'),
(83, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd  access to Inspectors', '2017-02-15 10:02:16'),
(84, 10, 1, 'Disabled Organization - Gamma (Pvt) Ltd\'s Organization account', '2017-02-15 10:07:25'),
(85, 10, 1, 'Enabled Organization - Gamma (Pvt) Ltd\'s Organization account', '2017-02-15 10:07:29'),
(86, 10, 1, 'Login to the system', '2017-02-15 10:47:07'),
(87, 10, 1, 'Login to the system', '2017-02-19 22:35:36'),
(88, 10, 1, 'Login to the system', '2017-02-20 21:59:49'),
(89, 10, 1, 'Login to the system', '2017-02-20 22:29:54'),
(90, 10, 1, 'Login to the system', '2017-02-22 08:33:10'),
(91, 10, 1, 'Login to the system', '2017-02-22 12:35:42'),
(92, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 13:47:55'),
(93, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 14:34:16'),
(94, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 15:10:08'),
(95, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 15:10:29'),
(96, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 15:10:38'),
(97, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 15:10:42'),
(98, 10, 1, 'Login to the system', '2017-02-22 18:34:40'),
(99, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 18:34:51'),
(100, 10, 1, 'Started a new inspection in property - 110000', '2017-02-22 18:39:15'),
(101, 10, 1, 'Login to the system', '2017-02-22 21:48:30'),
(102, 10, 1, 'Login to the system', '2017-02-22 21:49:15'),
(103, 10, 1, 'Login to the system', '2017-02-24 07:19:07'),
(104, 10, 1, 'Login to the system', '2017-02-24 11:02:47'),
(105, 10, 1, 'Login to the system', '2017-02-24 11:04:39'),
(106, 10, 1, 'Login to the system', '2017-02-25 07:29:45'),
(107, 10, 1, 'Login to the system', '2017-02-25 07:41:13'),
(108, 10, 1, 'Login to the system', '2017-02-25 10:30:34'),
(109, 10, 1, 'Login to the system', '2017-02-26 07:24:50'),
(110, 10, 1, 'Login to the system', '2017-02-26 14:08:13'),
(111, 10, 1, 'Login to the system', '2017-02-26 21:09:29'),
(112, 10, 1, 'Login to the system', '2017-02-26 21:12:50'),
(113, 10, 1, 'Login to the system', '2017-02-27 22:00:02'),
(114, 10, 1, 'Login to the system', '2017-02-28 08:05:25'),
(115, 10, 1, 'Login to the system', '2017-02-28 19:06:32'),
(116, 10, 1, 'Login to the system', '2017-03-01 04:51:06'),
(117, 10, 1, 'Login to the system', '2017-03-01 08:33:59'),
(118, 10, 1, 'Login to the system', '2017-03-01 19:38:28'),
(119, 10, 1, 'Login to the system', '2017-03-02 03:54:58'),
(120, 10, 1, 'Login to the system', '2017-03-02 09:51:05'),
(121, 10, 1, 'Login to the system', '2017-03-02 14:38:31');

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `adline1` varchar(32) DEFAULT NULL,
  `adline2` varchar(32) DEFAULT NULL,
  `frequency` varchar(2) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `insdate` datetime DEFAULT NULL,
  `reference` varchar(16) DEFAULT NULL,
  `alarm` varchar(16) DEFAULT NULL,
  `keylocation` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `adline1`, `adline2`, `frequency`, `type`, `insdate`, `reference`, `alarm`, `keylocation`, `status`, `created_at`) VALUES
(10000, '5 Hedley Road', 'Mt Roskill, Auckland', '4', NULL, '2016-10-12 10:00:00', '5 Hedley', '3322', 'Under the tree', 1, '2017-02-14 22:06:16'),
(110000, '101 Golf Road', 'New Lynn, Auckland 0600', '6', NULL, '2016-12-02 16:00:00', 'Golf ground', '6677', 'No key', 1, '2017-02-23 02:37:49'),
(210000, '111 Newton Road', 'Eden Terrace, Auckland', '8', NULL, '2017-01-30 08:30:00', 'The Core', '3632', 'With Shahriar', 1, '2017-01-24 17:49:13'),
(310000, '111 Newton Road', 'Eden Terrace, Auckland', '4', NULL, '2017-01-30 09:30:00', 'The Core', '3632', 'With Shahriar', 0, '2017-01-22 21:00:37'),
(410000, '12/1 Ellen Street', 'Manurawa', '5', 1, '2017-01-31 16:00:00', 'Ellen St House', '8485', 'With Tana', 1, '2017-01-31 03:52:34'),
(510000, '12/1 Ellen Street', 'Manurawa', '5', 1, '2017-01-31 16:00:00', 'Ellen St House', '8485', 'With Tana', 1, '2017-01-31 03:52:42');

-- --------------------------------------------------------

--
-- Table structure for table `inspections`
--

DROP TABLE IF EXISTS `inspections`;
CREATE TABLE `inspections` (
  `id` int(11) NOT NULL,
  `scheduleid` int(11) DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `inspector` varchar(32) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `residential` varchar(3) DEFAULT NULL,
  `age` varchar(32) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `bedrooms` int(11) DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `whether` varchar(16) DEFAULT NULL,
  `present_people` varchar(128) DEFAULT NULL,
  `other` text,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspections`
--

INSERT INTO `inspections` (`id`, `scheduleid`, `orgid`, `inspector`, `address`, `residential`, `age`, `type`, `bedrooms`, `bathrooms`, `whether`, `present_people`, `other`, `status`, `created_at`) VALUES
(8, 2, 1, NULL, NULL, 'Yes', 'Nearly 10 years', 0, 5, 3, 'Fine', 'Property Owner', 'Nothing', 4, '2017-03-03 04:25:58');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_first_page_fields`
--

DROP TABLE IF EXISTS `inspection_first_page_fields`;
CREATE TABLE `inspection_first_page_fields` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `field` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_first_page_fields`
--

INSERT INTO `inspection_first_page_fields` (`id`, `name`, `field`, `status`, `created_at`) VALUES
(1, 'Inspection ID', 'id', 1, '2017-03-02 17:02:14'),
(2, 'Ordered by', NULL, 0, '2017-03-02 17:02:45'),
(3, 'Mobile Number', NULL, 0, '2017-03-02 17:02:50'),
(4, 'Real Estate Agent', 'inspector', 1, '2017-03-02 17:09:58'),
(5, 'Address of Property', 'address', 1, '2017-03-02 17:10:30'),
(6, 'Residential ', 'residential', 1, '2017-03-02 17:11:15'),
(7, 'Age of the Property', 'age', 1, '2017-03-02 17:11:21'),
(8, 'Number of bedrooms', 'bedrooms', 1, '2017-03-02 17:11:25'),
(9, 'Number of Bathrooms', 'bathrooms', 1, '2017-03-02 17:11:27'),
(10, 'Other relevant Information', 'other', 1, '2017-03-02 17:11:42'),
(11, 'Date of Property Inspection', 'created_at', 1, '2017-03-02 17:12:11'),
(12, 'Time', NULL, 0, '2017-03-02 17:12:16'),
(13, 'Present at time of Inspection', 'present_people', 1, '2017-03-02 17:13:01'),
(14, 'Weather Conditions', 'whether', 1, '2017-03-02 17:13:05');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_limitations`
--

DROP TABLE IF EXISTS `inspection_limitations`;
CREATE TABLE `inspection_limitations` (
  `id` int(11) NOT NULL,
  `limitation` text,
  `orgid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_main_category`
--

DROP TABLE IF EXISTS `inspection_main_category`;
CREATE TABLE `inspection_main_category` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `icon` varchar(16) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_main_category`
--

INSERT INTO `inspection_main_category` (`id`, `name`, `icon`, `status`, `created_at`) VALUES
(1, 'Exterior', 'fa fa-bank', 1, '2017-02-22 21:44:42'),
(2, 'Interior', 'fa fa-qrcode', 1, '2017-02-22 21:44:56'),
(3, 'Other Findings', 'fa fa-bars', 1, '2017-02-22 21:45:08');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_media`
--

DROP TABLE IF EXISTS `inspection_media`;
CREATE TABLE `inspection_media` (
  `id` int(11) NOT NULL,
  `rowid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `caption` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_records`
--

DROP TABLE IF EXISTS `inspection_records`;
CREATE TABLE `inspection_records` (
  `id` int(11) NOT NULL,
  `orgid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_rows`
--

DROP TABLE IF EXISTS `inspection_rows`;
CREATE TABLE `inspection_rows` (
  `id` int(11) NOT NULL,
  `inspectionid` int(11) DEFAULT NULL,
  `mainid` int(11) DEFAULT NULL,
  `subid` int(11) DEFAULT NULL,
  `sectorid` int(11) DEFAULT NULL,
  `topic` varchar(64) DEFAULT NULL,
  `description` text,
  `general` text,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_rows`
--

INSERT INTO `inspection_rows` (`id`, `inspectionid`, `mainid`, `subid`, `sectorid`, `topic`, `description`, `general`, `status`, `created_at`) VALUES
(1, 8, 1, 1, 1, 'Test row test this one. its fine. but', '<p>add any comment</p>', '•testing one\n•testing two\n•testing three', 1, '2017-03-03 04:25:58'),
(3, 8, 1, 1, 5, 'Soffits', '<p>Grant completed an Apprenticeship and obtained Trade Certification in Carpentry before working as a Self-Employed Builder back<span>in the 80’s.</span></p><p>Grant then went on to have a career with the NZ Police, the last seven years spent as a Forensic Crime Scene Examiner. Grant turned his expertise and keen eye for detail back towards the Building Industry, re-training to become a <span>Property</span></p><p>Inspector <span>by working under an Inspector who had been in the Pre-purchase Property Inspection field for over 10 years. Formal Training was undertaken with Grant completing the following Modules set by the New Zealand Institute of Building</span></p><p>Surveyors</p><p><span>Water Tightness Inspection Training Programme 8 Modules completed:</span></p><ul><li><p>&nbsp; &nbsp;<span>Properties of Moisture</span></p></li><li><p>&nbsp; &nbsp;<span>Forensic Techniques</span></p></li><li><p>&nbsp; &nbsp;<span>Recording and Reporting</span></p></li><li><p>&nbsp; &nbsp;<span>Decay, Fungi and Moulds</span></p></li><li><p>&nbsp; &nbsp;<span>Cladding Systems</span></p></li><li><p>&nbsp; &nbsp;<span>Asset Management</span></p></li><li><p>&nbsp; &nbsp;<span>Condition/Compliance Reporting</span></p></li><li><p>&nbsp; &nbsp;<span>The Building Act Regime</span></p><p>Grant is also a member of BOINZ (Building Officials Institute of New Zealand) &amp; New Zealand Standards</p></li></ul>', NULL, 1, '2017-03-01 04:23:27'),
(4, 8, 1, 1, 2, 'Project Whiti', 'testing spouting description <span style="color: rgb(44, 130, 201);">do not delete this</span>', 'testing live updating bullets points', 1, '2017-03-02 08:53:33'),
(5, 8, 1, 1, 3, 'Down pipes', 'down pipes description', '•downpipes text', 1, '2017-02-28 23:44:49'),
(6, 8, 1, 1, 4, NULL, 'Penetrations description.', NULL, 1, '2017-02-28 23:47:19'),
(7, 8, 2, 12, 50, 'test flooring', 'testing on flooring description', 'flooring description', 1, '2017-02-28 23:48:09'),
(8, 8, NULL, NULL, NULL, NULL, '<p>limitations are here you can manage as you want.&nbsp;</p>', NULL, 1, '2017-03-03 04:12:36'),
(9, 8, 1, 2, 6, 'Added new topic', NULL, NULL, 1, '2017-03-01 05:15:17'),
(10, 8, 2, 8, 31, NULL, '<p><span contenteditable="false" draggable="true" class="fr-video fr-dvb fr-draggable fr-active"><video src="https://i.froala.com/download/f5545437834fd75ba7e36b3bd0bcb20ce8dd7ad6.mp4?1488475903" style="width: 600px;" "="" controls="" class="fr-draggable">Your browser does not support HTML5 video.</video></span></p>', NULL, 1, '2017-03-02 04:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_schedule`
--

DROP TABLE IF EXISTS `inspection_schedule`;
CREATE TABLE `inspection_schedule` (
  `id` int(11) NOT NULL,
  `assetid` int(11) DEFAULT NULL,
  `inspectionid` int(11) DEFAULT NULL,
  `scheduledatetime` datetime DEFAULT NULL,
  `inspector` int(11) DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `end_at` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_schedule`
--

INSERT INTO `inspection_schedule` (`id`, `assetid`, `inspectionid`, `scheduledatetime`, `inspector`, `started_at`, `end_at`, `status`, `created_at`) VALUES
(1, 10000, NULL, '2017-01-12 10:30:00', 11, '2017-01-12 10:39:00', '2017-01-12 10:57:00', 2, '2017-02-15 00:46:11'),
(2, 110000, 8, '2017-02-15 14:30:00', 11, '2017-02-23 07:39:15', NULL, 1, '2017-02-23 07:39:15');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_sections`
--

DROP TABLE IF EXISTS `inspection_sections`;
CREATE TABLE `inspection_sections` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `listorder` int(11) DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_sections`
--

INSERT INTO `inspection_sections` (`id`, `name`, `listorder`, `orgid`, `status`, `created_at`) VALUES
(1, 'Exterior', 1, 0, 1, '2017-02-15 03:11:13'),
(2, 'Garage', 2, 0, 1, '2017-02-15 03:11:13'),
(3, 'Dining Room', 3, 0, 1, '2017-02-15 03:15:06'),
(4, 'Kitchen', 4, 0, 1, '2017-02-15 03:15:06'),
(5, 'Master Bedroom', 5, 0, 1, '2017-02-15 03:15:06'),
(6, 'Bedroom', 6, 0, 1, '2017-02-15 03:15:06'),
(7, 'Bonus Room', 7, 0, 1, '2017-02-15 03:15:06'),
(8, 'Bathroom', 8, 0, 1, '2017-02-15 03:15:06'),
(9, 'Living Room', 9, 0, 1, '2017-02-15 03:15:06'),
(10, 'Laundry', 10, 0, 1, '2017-02-15 03:15:06');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_sectors`
--

DROP TABLE IF EXISTS `inspection_sectors`;
CREATE TABLE `inspection_sectors` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `subid` int(11) DEFAULT NULL,
  `multiple` int(11) DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_sectors`
--

INSERT INTO `inspection_sectors` (`id`, `name`, `subid`, `multiple`, `status`, `created_at`) VALUES
(1, 'Roof', 1, 0, 1, '2017-02-21 11:39:55'),
(2, 'Spouting', 1, 0, 1, '2017-02-21 11:40:02'),
(3, 'Downpipes', 1, 0, 1, '2017-02-21 11:40:11'),
(4, 'Penetrations', 1, 0, 1, '2017-02-21 11:40:20'),
(5, 'Soffits', 1, 0, 1, '2017-02-21 11:40:33'),
(6, 'Construction', 2, 0, 1, '2017-02-21 11:40:46'),
(7, 'Fascia, Barges', 2, 0, 1, '2017-02-21 11:40:59'),
(8, 'Flashings', 2, 0, 1, '2017-02-21 11:41:08'),
(9, 'Exterior Walls (inc Cladding, Roof Gables, Door & Window Heads etc)', 2, 0, 1, '2017-02-21 11:41:41'),
(10, 'Risk Junctions: Flashings', 2, 0, 1, '2017-02-21 11:42:08'),
(11, 'Risk Junctions: Cladding', 2, 0, 1, '2017-02-21 11:42:23'),
(12, 'High Risk Junctions', 2, 0, 1, '2017-02-21 11:42:34'),
(13, 'Base Cladding', 3, 0, 1, '2017-02-21 11:42:49'),
(14, 'Block Work/Concrete', 3, 0, 1, '2017-02-21 11:43:03'),
(15, 'Footing/Slabs', 3, 0, 1, '2017-02-21 11:43:13'),
(16, 'Joists', 3, 0, 1, '2017-02-21 11:43:21'),
(17, 'Sub Floor Construction', 3, 0, 1, '2017-02-21 11:43:35'),
(18, 'Doors/Hardware', 4, 0, 1, '2017-02-21 11:43:50'),
(19, 'Garage Within Roof Line: Door', 4, 0, 1, '2017-02-21 11:44:14'),
(20, 'Windows', 4, 0, 1, '2017-02-21 11:44:29'),
(21, 'Deck Barriers/Handrails/Steps/Balcony', 4, 0, 1, '2017-02-21 11:44:51'),
(22, 'Paintwork', 4, 0, 1, '2017-02-21 11:44:59'),
(23, 'Hot Water Cylinder', 5, 0, 1, '2017-02-21 11:45:14'),
(24, 'Pipers, Wastes', 5, 0, 1, '2017-02-21 11:45:26'),
(25, 'Internal Taps, Mixers, Toilets', 5, 0, 1, '2017-02-21 11:45:48'),
(26, 'Exterior Plumbing & Drainage', 5, 0, 1, '2017-02-21 11:46:04'),
(27, 'Driveway/Paths', 6, 0, 1, '2017-02-21 11:46:16'),
(28, 'Decks/Patio/Pergolas etc', 6, 0, 1, '2017-02-21 11:46:35'),
(29, 'Fencing/Retaining walls', 6, 0, 1, '2017-02-21 11:46:47'),
(30, 'Sleep-out/Shed/Carport', 7, 0, 1, '2017-02-21 11:47:07'),
(31, 'Flooring', 8, 0, 1, '2017-02-22 22:28:17'),
(32, 'Ceiling', 8, 0, 1, '2017-02-22 22:28:33'),
(33, 'Walls', 8, 0, 1, '2017-02-22 22:28:44'),
(34, 'Stairwells', 8, 0, 1, '2017-02-22 22:29:00'),
(35, 'Doors/Door Hardware', 8, 0, 1, '2017-02-22 22:29:16'),
(36, 'Windows/ Window Hardware', 8, 0, 1, '2017-02-22 22:29:35'),
(37, 'Security Hardware', 8, 0, 1, '2017-02-22 22:29:48'),
(38, 'Rooms', 9, 0, 1, '2017-02-22 22:43:10'),
(39, 'Interior Decoration', 9, 0, 1, '2017-02-22 22:43:12'),
(40, 'Floor Coverings', 9, 0, 1, '2017-02-22 22:43:23'),
(41, 'Heating', 10, 0, 1, '2017-02-22 22:43:38'),
(42, 'Gas', 10, 0, 1, '2017-02-22 22:43:49'),
(43, 'Ceiling Insulation', 10, 0, 1, '2017-02-22 22:44:00'),
(44, 'Subfloor Insulation', 10, 0, 1, '2017-02-22 22:44:10'),
(45, 'Wall Insulation', 10, 0, 1, '2017-02-22 22:44:21'),
(46, 'Internal Distribution Board', 11, 0, 1, '2017-02-22 22:44:58'),
(47, 'Fittings', 11, 0, 1, '2017-02-22 22:45:12'),
(48, 'Lights', 11, 0, 1, '2017-02-22 22:45:25'),
(49, 'Wiring', 11, 0, 1, '2017-02-22 22:45:37'),
(50, 'Flooring', 12, 0, 1, '2017-02-22 22:46:05'),
(51, 'Bench Tops/Cabinets', 12, 0, 1, '2017-02-22 22:46:19'),
(52, 'Appliances/Accessories', 12, 0, 1, '2017-02-22 22:46:31'),
(53, 'First Level', 13, 0, 1, '2017-02-22 22:47:03'),
(54, 'Ensuite', 13, 0, 1, '2017-02-22 22:47:14'),
(55, 'Ground Floor', 13, 0, 1, '2017-02-22 22:47:39'),
(56, 'Laundry in General', 14, 0, 1, '2017-02-22 22:48:08'),
(57, 'Evidence of Moisture & Mould', 15, 0, 1, '2017-02-22 22:48:56'),
(58, 'Alterations', 15, 0, 1, '2017-02-22 22:49:12'),
(59, 'Pest Evidence', 15, 0, 1, '2017-02-22 22:49:22'),
(60, 'Contamination to Site or Building', 15, 0, 1, '2017-02-22 22:49:37');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_services`
--

DROP TABLE IF EXISTS `inspection_services`;
CREATE TABLE `inspection_services` (
  `id` int(11) NOT NULL,
  `detailid` int(11) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `contactid` int(11) DEFAULT NULL,
  `action` varchar(16) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_sub_category`
--

DROP TABLE IF EXISTS `inspection_sub_category`;
CREATE TABLE `inspection_sub_category` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `mainid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_sub_category`
--

INSERT INTO `inspection_sub_category` (`id`, `name`, `mainid`, `status`, `created_at`) VALUES
(1, 'Roofline', 1, 1, '2017-02-21 11:20:34'),
(2, 'Exterior Construction', 1, 1, '2017-02-21 11:20:47'),
(3, 'Foundation', 1, 1, '2017-02-21 11:21:00'),
(4, 'Other Exterior Features', 1, 1, '2017-02-21 11:21:29'),
(5, 'Plumbing & Drainage ', 1, 1, '2017-02-21 11:21:43'),
(6, 'Outdoor Extras', 1, 1, '2017-02-21 11:22:02'),
(7, 'Out Buildings', 1, 1, '2017-02-21 11:22:14'),
(8, 'Interior Linings & Hardware', 2, 1, '2017-02-22 22:27:35'),
(9, 'Interior Rooms & Decoration', 2, 1, '2017-02-22 22:30:29'),
(10, 'Heating & Insulation', 2, 1, '2017-02-22 22:42:18'),
(11, 'Electrical', 2, 1, '2017-02-22 22:44:33'),
(12, 'Kitchen', 2, 1, '2017-02-22 22:45:54'),
(13, 'Bathrooms', 2, 1, '2017-02-22 22:46:48'),
(14, 'Laundry', 2, 1, '2017-02-22 22:47:53'),
(15, 'General', 3, 1, '2017-02-22 22:48:26');

-- --------------------------------------------------------

--
-- Table structure for table `org`
--

DROP TABLE IF EXISTS `org`;
CREATE TABLE `org` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `ownerid` int(11) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `org`
--

INSERT INTO `org` (`id`, `name`, `ownerid`, `url`, `status`, `created_at`) VALUES
(1, 'IDL Creations', 10, NULL, 1, '2017-02-10 11:06:06'),
(2, 'Test Org', 12, 'www.testorg.org', 1, '2017-02-12 18:11:53'),
(3, 'Gamma (Pvt) Ltd', 13, NULL, 1, '2017-02-15 23:07:29');

-- --------------------------------------------------------

--
-- Table structure for table `service_categories`
--

DROP TABLE IF EXISTS `service_categories`;
CREATE TABLE `service_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_contact`
--

DROP TABLE IF EXISTS `service_contact`;
CREATE TABLE `service_contact` (
  `id` int(11) NOT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `companyname` varchar(64) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `telephone` varchar(16) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `systems`
--

DROP TABLE IF EXISTS `systems`;
CREATE TABLE `systems` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `route` varchar(32) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `listorder` int(11) DEFAULT NULL,
  `visibility` int(11) DEFAULT NULL,
  `allow_default` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `systems`
--

INSERT INTO `systems` (`id`, `name`, `route`, `icon`, `listorder`, `visibility`, `allow_default`, `status`, `created_at`) VALUES
(1, 'Dashboard', 'dashboard', 'fa-dashboard', 10, 1, 1, 1, '2017-02-03 21:33:07'),
(2, 'Properties', 'assets', 'fa-list', 100, 1, 0, 1, '2017-02-14 22:13:45'),
(3, 'Inspections', 'inspections', 'fa-edit', 101, 1, 0, 1, '2017-02-03 21:41:15'),
(4, 'Services', 'services', 'fa-table', 102, 1, 0, 1, '2017-02-03 21:41:15'),
(5, 'Inspectors', 'users', 'fa-user', 103, 1, 0, 1, '2017-02-14 22:08:05'),
(6, 'Organizations', 'organizations', ' fa-building-o', 99, 1, 0, 1, '2017-02-03 21:41:15'),
(7, 'Settings', 'settings', 'fa-cog', 200, 1, 0, 1, '2017-02-03 21:41:15');

-- --------------------------------------------------------

--
-- Table structure for table `system_exceptions`
--

DROP TABLE IF EXISTS `system_exceptions`;
CREATE TABLE `system_exceptions` (
  `id` int(11) NOT NULL,
  `controller` varchar(16) DEFAULT NULL,
  `func` varchar(16) DEFAULT NULL,
  `exception` varchar(1024) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_exceptions`
--

INSERT INTO `system_exceptions` (`id`, `controller`, `func`, `exception`, `user`, `status`, `created_at`) VALUES
(1, 'UserController', 'addUser', 'InvalidArgumentException: Route [active] not defined. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php:231\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(211): Illuminate\\Routing\\UrlGenerator->route(\'active\', Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(55): Illuminate\\Support\\Facades\\Facade::__callStatic(\'route\', Array)\n#2 [internal function]: UserController->addUser()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'addUser\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserContro', NULL, 2, '2017-02-01 19:49:56'),
(2, 'UserController', 'addUser', 'InvalidArgumentException: View [admin.email.welcome-user] not found. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php:146\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php(83): Illuminate\\View\\FileViewFinder->findInPaths(\'admin.email.wel...\', Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Factory.php(124): Illuminate\\View\\FileViewFinder->find(\'admin.email.wel...\')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(402): Illuminate\\View\\Factory->make(\'admin.email.wel...\', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(272): Illuminate\\Mail\\Mailer->getView(\'admin.email.wel...\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(147): Illuminate\\Mail\\Mailer->addContent(Object(Illuminate\\Mail\\Message), \'admin.email.w', NULL, 2, '2017-02-01 20:04:44'),
(3, 'UserController', 'addUser', 'InvalidArgumentException: View [email.active] not found. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php:146\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php(83): Illuminate\\View\\FileViewFinder->findInPaths(\'email.active\', Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Factory.php(124): Illuminate\\View\\FileViewFinder->find(\'email.active\')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(402): Illuminate\\View\\Factory->make(\'email.active\', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(272): Illuminate\\Mail\\Mailer->getView(\'email.active\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(147): Illuminate\\Mail\\Mailer->addContent(Object(Illuminate\\Mail\\Message), \'email.active\', NULL, Array)\n#5 /Users/isuru/Proje', NULL, 2, '2017-02-01 23:47:43'),
(4, 'UserController', 'addUser', 'ErrorException: Undefined variable: company in /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705:204\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705(204): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/isuru/Pr...\', 204, Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/PhpEngine.php(37): include(\'/Users/isuru/Pr...\')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(56): Illuminate\\View\\Engines\\PhpEngine->evaluatePath(\'/Users/isuru/Pr...\', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(140): Illuminate\\View\\Engines\\CompilerEngine->get(\'/Users/isuru/Pr...\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(109): Illuminate\\View\\View->getContents()\n#5 /Users/isuru/Projects/easyinspect/vendor/lar', NULL, 2, '2017-02-01 23:51:17'),
(5, 'UserController', 'addUser', 'ErrorException: Undefined variable: company in /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705:204\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705(204): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/isuru/Pr...\', 204, Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/PhpEngine.php(37): include(\'/Users/isuru/Pr...\')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(56): Illuminate\\View\\Engines\\PhpEngine->evaluatePath(\'/Users/isuru/Pr...\', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(140): Illuminate\\View\\Engines\\CompilerEngine->get(\'/Users/isuru/Pr...\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(109): Illuminate\\View\\View->getContents()\n#5 /Users/isuru/Projects/easyinspect/vendor/lar', NULL, 2, '2017-02-01 23:52:17'),
(6, 'UserController', 'addUser', 'ErrorException: Array to string conversion in /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705:204\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705(204): Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/isuru/Pr...\', 204, Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/PhpEngine.php(37): include(\'/Users/isuru/Pr...\')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(56): Illuminate\\View\\Engines\\PhpEngine->evaluatePath(\'/Users/isuru/Pr...\', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(140): Illuminate\\View\\Engines\\CompilerEngine->get(\'/Users/isuru/Pr...\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(109): Illuminate\\View\\View->getContents()\n#5 /Users/isuru/Projects/easyinspect/vendor/lara', NULL, 2, '2017-02-01 23:59:57'),
(7, 'UserController', 'addUser', 'ErrorException: Undefined variable: company in /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705:204\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/storage/views/4f66652026c3422f15022e4f81be8705(204): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/isuru/Pr...\', 204, Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/PhpEngine.php(37): include(\'/Users/isuru/Pr...\')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(56): Illuminate\\View\\Engines\\PhpEngine->evaluatePath(\'/Users/isuru/Pr...\', Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(140): Illuminate\\View\\Engines\\CompilerEngine->get(\'/Users/isuru/Pr...\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/View.php(109): Illuminate\\View\\View->getContents()\n#5 /Users/isuru/Projects/easyinspect/vendor/lar', NULL, 2, '2017-02-02 00:03:19'),
(8, 'UserController', 'getActivate', 'InvalidArgumentException: View [admin.activate] not found. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php:146\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/FileViewFinder.php(83): Illuminate\\View\\FileViewFinder->findInPaths(\'admin.activate\', Array)\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/View/Factory.php(124): Illuminate\\View\\FileViewFinder->find(\'admin.activate\')\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(211): Illuminate\\View\\Factory->make(\'admin.activate\', Array)\n#3 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(96): Illuminate\\Support\\Facades\\Facade::__callStatic(\'make\', Array)\n#4 [internal function]: UserController->getActivate(\'NsL6sqcBEiD3N3y...\')\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', NULL, 2, '2017-02-03 10:38:52'),
(9, 'UserController', 'postLogin', 'BadMethodCallException: Method [saveActivity] does not exist. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(61): Illuminate\\Routing\\Controller->__call(\'saveActivity\', Array)\n#1 [internal function]: UserController->postLogin()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'postLogin\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\Routing\\Route), \'postLogin\')\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Router.php(967): Ill', NULL, 2, '2017-02-03 11:02:08'),
(10, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:10:26'),
(11, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:10:49'),
(12, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:12:11'),
(13, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:13:27'),
(14, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:13:29'),
(15, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:190\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(190): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 190, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(183): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-03 11:13:38'),
(16, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:42:00'),
(17, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:42:38'),
(18, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:43:07'),
(19, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:44:48'),
(20, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:46:17'),
(21, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:47:21'),
(22, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(185): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-09 11:48:37'),
(23, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(66): UserController->saveActivity(\'Trying to login...\')\n#2 [internal function]: UserController->postLogin()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'postLogin\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\Ro', NULL, 2, '2017-02-10 10:15:42'),
(24, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:192\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(192): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 192, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(66): UserController->saveActivity(\'Trying to login...\')\n#2 [internal function]: UserController->postLogin()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'postLogin\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\Ro', NULL, 2, '2017-02-10 10:15:51'),
(25, 'OrganizationCont', 'getOrgList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'name\' in \'field list\' in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `name` f...\')\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'select `name` f...\', Array)\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(581): Illuminate\\Database\\Connection->runQueryCallback(\'select `name` f...\', Array, Object(Closure))\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(304): Illuminate\\Database\\Connection->run(\'select `name` f...\', Array, Object(Closure))\n#4 /Users/isuru/Projects/easyinspect/v', NULL, 2, '2017-02-10 11:03:12'),
(26, 'OrganizationCont', 'postOrgStatus', 'BadMethodCallException: Method [saveActivity] does not exist. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/OrganizationController.php(170): Illuminate\\Routing\\Controller->__call(\'saveActivity\', Array)\n#1 [internal function]: OrganizationController->postOrgStatus()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'postOrgStatus\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(OrganizationController), Object(Illuminate\\Routing\\Route), \'postOrgStatus\')\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Il', NULL, 2, '2017-02-11 03:10:44'),
(27, 'UserController', 'addUser', 'Swift_TransportException: Connection could not be established with host smtp.gmail.com [Operation timed out #60] in /Users/isuru/Projects/easyinspect/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/StreamBuffer.php:269\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/StreamBuffer.php(62): Swift_Transport_StreamBuffer->_establishSocketConnection()\n#1 /Users/isuru/Projects/easyinspect/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(113): Swift_Transport_StreamBuffer->initialize(Array)\n#2 /Users/isuru/Projects/easyinspect/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mailer.php(79): Swift_Transport_AbstractSmtpTransport->start()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(329): Swift_Mailer->send(Object(Swift_Message), Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(151): Illuminate\\Mail\\Mailer->sendSwif', NULL, 2, '2017-02-15 23:05:22'),
(28, 'InspectionsContr', 'saveBasic', 'PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'inspectdb.inspections\' doesn\'t exist in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `in...\')\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'insert into `in...\', Array)\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(581): Illuminate\\Database\\Connection->runQueryCallback(\'insert into `in...\', Array, Object(Closure))\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(370): Illuminate\\Database\\Connection->run(\'insert into `in...\', Array, Object(Closure))\n#4 /Users/isuru/Proj', NULL, 2, '2017-02-23 02:44:54'),
(29, 'InspectionsContr', 'saveBasic', 'Illuminate\\Database\\Eloquent\\ModelNotFoundException: No query results for model [Inspection]. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php:140\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/InspectionController.php(99): Illuminate\\Database\\Eloquent\\Builder->firstOrFail()\n#1 [internal function]: InspectionController->saveBasic()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'saveBasic\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(InspectionController), Object(Illuminate\\Routing\\Route), \'saveBasic\')\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/frame', NULL, 2, '2017-02-23 07:38:12'),
(30, 'InspectionsContr', 'saveTopic', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'satatus\' in \'field list\' in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `in...\')\n#1 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'insert into `in...\', Array)\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(581): Illuminate\\Database\\Connection->runQueryCallback(\'insert into `in...\', Array, Object(Closure))\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Connection.php(370): Illuminate\\Database\\Connection->run(\'insert into `in...\', Array, Object(Closure))\n#4 /Users/isuru/Projects/easyinspec', NULL, 2, '2017-02-23 07:40:12'),
(31, 'InspectionsContr', 'getSector', 'Illuminate\\Database\\Eloquent\\ModelNotFoundException: No query results for model [InspectionRow]. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php:140\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/InspectionController.php(192): Illuminate\\Database\\Eloquent\\Builder->firstOrFail()\n#1 [internal function]: InspectionController->getFuck()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getFuck\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(InspectionController), Object(Illuminate\\Routing\\Route), \'getFuck\')\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framewo', NULL, 2, '2017-02-23 08:33:28'),
(32, 'InspectionsContr', 'getSector', 'Illuminate\\Database\\Eloquent\\ModelNotFoundException: No query results for model [InspectionRow]. in /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php:140\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/InspectionController.php(192): Illuminate\\Database\\Eloquent\\Builder->firstOrFail()\n#1 [internal function]: InspectionController->getInspectionSector()\n#2 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getInspectionSe...\', Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(InspectionController), Object(Illuminate\\Routing\\Route), \'getInspectionSe...\')\n#5 /Users/isuru/Projects/', NULL, 2, '2017-02-23 08:36:45'),
(33, 'UserController', 'saveActivity', 'ErrorException: Trying to get property of non-object in /Users/isuru/Projects/easyinspect/app/controllers/UserController.php:198\nStack trace:\n#0 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/isuru/Pr...\', 198, Array)\n#1 /Users/isuru/Projects/easyinspect/app/controllers/UserController.php(191): UserController->saveActivity(\'Logout from the...\')\n#2 [internal function]: UserController->getLogOut()\n#3 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#4 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(93): Illuminate\\Routing\\Controller->callAction(\'getLogOut\', Array)\n#5 /Users/isuru/Projects/easyinspect/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(62): Illuminate\\Routing\\ControllerDispatcher->call(Object(UserController), Object(Illuminate\\R', NULL, 2, '2017-02-23 10:48:42');

-- --------------------------------------------------------

--
-- Table structure for table `system_operations`
--

DROP TABLE IF EXISTS `system_operations`;
CREATE TABLE `system_operations` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `operation` varchar(32) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `system_operation_access`
--

DROP TABLE IF EXISTS `system_operation_access`;
CREATE TABLE `system_operation_access` (
  `id` int(11) NOT NULL,
  `operationid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `allow` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `system_routingaccess`
--

DROP TABLE IF EXISTS `system_routingaccess`;
CREATE TABLE `system_routingaccess` (
  `id` int(11) NOT NULL,
  `systemid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `allow` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_routingaccess`
--

INSERT INTO `system_routingaccess` (`id`, `systemid`, `userid`, `allow`, `created_at`) VALUES
(1, 1, 11, 1, '2017-02-04 20:59:18'),
(2, 2, 11, 1, '2017-02-04 08:26:02'),
(3, 3, 11, 1, '2017-02-09 08:41:00'),
(4, 2, NULL, 1, '2017-02-10 14:21:16'),
(5, 2, 12, 1, '2017-02-10 14:22:08'),
(6, 1, 13, 1, '2017-02-14 08:20:45'),
(7, 2, 13, 1, '2017-02-14 21:35:57'),
(8, 3, 13, 1, '2017-02-14 08:20:50'),
(9, 4, 13, 1, '2017-02-14 08:20:54'),
(10, 5, 13, 1, '2017-02-15 23:02:16'),
(11, 7, 13, 1, '2017-02-14 08:21:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(16) DEFAULT NULL,
  `lname` varchar(16) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `password_temp` varchar(60) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `remember_token` varchar(120) DEFAULT NULL,
  `jobtitle` varchar(32) DEFAULT NULL,
  `roll` int(11) DEFAULT NULL,
  `orgid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `gender`, `email`, `password`, `password_temp`, `code`, `active`, `remember_token`, `jobtitle`, `roll`, `orgid`, `status`, `updated_at`, `created_at`) VALUES
(10, 'Pavithra', 'Isuru', NULL, 'pavithraisuru@gmail.com', '$2y$10$iIBVz3lmWdA2cX5pB75jPuk/ebFjdl05zBtuN6/JDOT6jMusQwcb6', NULL, '', 1, 'fcZf5kUf5cHxVjgAxP4LZOhEfV6OcSG7FhwslxeEBzP4NAOp6nc4r05CoDJU', 'System Administrator', 1, 1, 1, '2017-02-23 10:48:42', '2017-02-23 10:48:42'),
(11, 'Yahoo', 'Isuru', NULL, 'pavithra@yahoo.com', '$2y$10$19/2d.MWC799E3g.A8nn0.grQfcojJ8mVPJeSXvMpvdKIWePiHpJO', NULL, '', 1, NULL, 'Test Inspector', 4, 1, 1, '2017-02-09 22:18:23', '2017-02-14 21:06:39'),
(12, 'Test', '', NULL, 'test@test.com', NULL, NULL, 'mkkS3pbtKaNfVxBNFWZQiGojGdcHa6ou8jdlThqCwZkOvBu4bFPqqH7MChgd', 0, NULL, 'User', 4, 2, 3, '2017-02-03 21:22:37', '2017-02-11 03:42:52'),
(13, 'Alpha', 'Romeo', NULL, 'pavithraidl@yahoo.com', '$2y$10$8zpCY7uF3GixEtxm4JC.VOtEpNRZwvACkPxtMo3bvTPYUStjyI0f6', NULL, '', 1, NULL, 'Admin', 3, 3, 1, '2017-02-14 21:12:48', '2017-02-14 21:12:48'),
(14, 'Rupinder', '', NULL, 'rupinder.sekhon1993@gmail.com', NULL, NULL, 'rWM1lVWrzSbYXxMZlaZfgTxFTU4lg6xb3Ba7LwZV9TFiuNfPX8JnExz9qXFU', 0, NULL, 'Inspector', 4, 3, 3, '2017-02-15 23:04:52', '2017-02-15 10:04:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspections`
--
ALTER TABLE `inspections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_first_page_fields`
--
ALTER TABLE `inspection_first_page_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_limitations`
--
ALTER TABLE `inspection_limitations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_main_category`
--
ALTER TABLE `inspection_main_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_media`
--
ALTER TABLE `inspection_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_records`
--
ALTER TABLE `inspection_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_rows`
--
ALTER TABLE `inspection_rows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_schedule`
--
ALTER TABLE `inspection_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_sections`
--
ALTER TABLE `inspection_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_sectors`
--
ALTER TABLE `inspection_sectors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_services`
--
ALTER TABLE `inspection_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_sub_category`
--
ALTER TABLE `inspection_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `org`
--
ALTER TABLE `org`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_categories`
--
ALTER TABLE `service_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_contact`
--
ALTER TABLE `service_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `systems`
--
ALTER TABLE `systems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_exceptions`
--
ALTER TABLE `system_exceptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_operations`
--
ALTER TABLE `system_operations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_operation_access`
--
ALTER TABLE `system_operation_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_routingaccess`
--
ALTER TABLE `system_routingaccess`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=510001;
--
-- AUTO_INCREMENT for table `inspections`
--
ALTER TABLE `inspections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `inspection_first_page_fields`
--
ALTER TABLE `inspection_first_page_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `inspection_limitations`
--
ALTER TABLE `inspection_limitations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inspection_main_category`
--
ALTER TABLE `inspection_main_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `inspection_media`
--
ALTER TABLE `inspection_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inspection_records`
--
ALTER TABLE `inspection_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inspection_rows`
--
ALTER TABLE `inspection_rows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `inspection_schedule`
--
ALTER TABLE `inspection_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `inspection_sections`
--
ALTER TABLE `inspection_sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `inspection_sectors`
--
ALTER TABLE `inspection_sectors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `inspection_services`
--
ALTER TABLE `inspection_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inspection_sub_category`
--
ALTER TABLE `inspection_sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `org`
--
ALTER TABLE `org`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `service_categories`
--
ALTER TABLE `service_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `service_contact`
--
ALTER TABLE `service_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `systems`
--
ALTER TABLE `systems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `system_exceptions`
--
ALTER TABLE `system_exceptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `system_operations`
--
ALTER TABLE `system_operations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_operation_access`
--
ALTER TABLE `system_operation_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_routingaccess`
--
ALTER TABLE `system_routingaccess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;